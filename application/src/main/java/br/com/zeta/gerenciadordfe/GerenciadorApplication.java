package br.com.zeta.gerenciadordfe;

import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.ws.config.annotation.EnableWs;

import br.com.zeta.gerenciadordfe.domain.service.dfe.GerenciadorDfeService;

//Se tiver projetos JPA
//@EnableJpaRepositories(basePackages = {"org.my.multi.module.one.repo. *", "Org.my.multi.module.two.repo. *"}) 
//@EntityScan (basePackages = {"org.my. multi.module.one.model. * "," org.my.multi.module.two.model. * "})
@SpringBootApplication
@ComponentScan(basePackages = {"br.com.zeta.*"})
@EnableJpaAuditing
@EnableScheduling
@EnableWs
public class GerenciadorApplication implements CommandLineRunner{

	@Autowired
	private GerenciadorDfeService gerenciadorDfeService;
	
	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(GerenciadorApplication.class,args);
	}

	
	@Override
	public void run(String... args) throws Exception {
		gerenciadorDfeService.agendarConsultasDFe();
	}
	
}