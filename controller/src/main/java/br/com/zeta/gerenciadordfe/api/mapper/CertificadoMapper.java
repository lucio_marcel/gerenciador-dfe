package br.com.zeta.gerenciadordfe.api.mapper;

import org.modelmapper.ModelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.model.response.CertificadoResponseModel;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;

@Component
public class CertificadoMapper {
	
	private static ModelMapper mapper;
	
	@Autowired
	public void setModelMapper(ModelMapper mapper) {
		CertificadoMapper.mapper = mapper;
	}

	public CertificadoResponseModel toResponseModel(Certificado certificado) {
		CertificadoResponseModel certificadoResponseModel = mapper.map(certificado, CertificadoResponseModel.class);
		certificadoResponseModel.setExpirado(certificado.expirado());
		return certificadoResponseModel;
	}	
	
}
