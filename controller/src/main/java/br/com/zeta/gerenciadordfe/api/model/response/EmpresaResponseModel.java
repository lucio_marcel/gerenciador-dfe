package br.com.zeta.gerenciadordfe.api.model.response;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.mapper.EmpresaMapper;
import br.com.zeta.gerenciadordfe.domain.core.enums.StatusEmpresa;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoAmbiente;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class EmpresaResponseModel {
	
	private static EmpresaMapper empresaMapper;

	private Long codigo;
	private String cnpj;
	private StatusEmpresa statusEmpresa;
	private TipoAmbiente tipoAmbiente;
	private String codigoMuncipio;
	private boolean manifestacaoAutomaticaAtiva;
	private OffsetDateTime dataUltimaConsulta;
	private StatusResposta statusUltimaConsulta;
	private boolean certificadoValido;
	
	@Autowired
	public void setEmpresaMapper(EmpresaMapper empresaMapper) {
		EmpresaResponseModel.empresaMapper = empresaMapper;
	}
	
	public static EmpresaResponseModel toResponseModel(Empresa empresa) {
		return empresaMapper.toResponseModel(empresa);
	}

	public static List<EmpresaResponseModel> toResponseModel(List<Empresa> empresas) {
		return empresaMapper.toCollectionReponseModel(empresas);
	}
	
}
