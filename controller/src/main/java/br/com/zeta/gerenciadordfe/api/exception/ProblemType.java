package br.com.zeta.gerenciadordfe.api.exception;

import lombok.Getter;

@Getter
public enum ProblemType {
	
	DADOS_INVALIDOS("/dados-invalidos","Dados inválidos"),
	ERRO_DE_SISTEMA("/erro-de-sistema","Erro de sistema"),
	PARAMETRO_INVALIDO("/parametro-invalido","Parâmetro inválido"),
	MENSAGEM_IMCOMPREENSIVEL("/mensagem-incompreensivel","Mensagem incompreensível"),
	RECURSO_NAO_ENCONTRADO("/recurso-nap-encontrado","Recurso não encontrado"),
	ENTIDADE_EM_USO("/entidade-em-uso","Entidade em uso"),
	VIOLACAO_DE_INTEGRIDADE("/violacao-de-integridade","Violação de integridade"),
	VIOLACAO_REGRA_NEGOCIO("/erro-negocio","Violação de regra de negócio"),
	FALHA_PROCESSAMENTO_EVENTO_SEFAZ("/falha-processamento-evento-sefaz","Falha no processamento do evento na SEFAZ."),
	FALHA_PROCESSAMENTO_REQUISICAO_SEFAZ("/falha-processamento-requisicao-sefaz","Falha no processamento da requisição na SEFAZ."),
	CERTIFICADO_DIGITAL_EXPIRADO("/certificado-digital-expirado","Certificado digital expirado."),
	CERTIFICADO_DIGITAL_NAO_ENCONTRADO("/certificado-digital-nao-encontrado","Certificado digital não encontrado."),
	FALHA_CERTIFICADO_DIGITAL("/falha-certificado-digital-nao-encontrado","Problemas com o certificado digital."),
	EVENTO_INVALIDO("/evento-invalido","O evento possui dados inválidos."),
	APPLICATION_ERROR("/application-error","Erro na aplicação.");
	
	private String title;
	private String uri;
	
	ProblemType(String path, String title) {
		this.uri = "https://gerenciador-dfe.zeta.com.br" + path;
		this.title = title;
	}

}
