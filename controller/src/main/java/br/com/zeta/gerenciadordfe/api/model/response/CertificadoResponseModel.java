package br.com.zeta.gerenciadordfe.api.model.response;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.mapper.CertificadoMapper;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class CertificadoResponseModel {
	private static CertificadoMapper certificadoMapper;
	
	private TipoCertificado tipoCertificado;
	private String alias;
	private String cnpjCpf;
	private LocalDateTime dataHoraVencimento;
	private Long diasRestantes;
	private Boolean expirado;
	private String motivoCertificadoInvalido;

	@Autowired
	public void setCertificadoMapper(CertificadoMapper certificadoMapper) {
		CertificadoResponseModel.certificadoMapper = certificadoMapper;
	}
	
	public static CertificadoResponseModel toResponseModel(Certificado certificado) {
		return certificadoMapper.toResponseModel(certificado);
	}
}
