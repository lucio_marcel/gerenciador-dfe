package br.com.zeta.gerenciadordfe.api.controller;

import static br.com.zeta.gerenciadordfe.api.model.request.EmpresaRequestModel.mapToDomain;
import static br.com.zeta.gerenciadordfe.api.model.request.EmpresaRequestModel.toDomain;
import static br.com.zeta.gerenciadordfe.api.model.response.EmpresaResponseModel.toResponseModel;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.zeta.gerenciadordfe.api.model.request.EmpresaRequestModel;
import br.com.zeta.gerenciadordfe.api.model.response.EmpresaResponseModel;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;

@RestController
@RequestMapping("/empresas")
public class EmpresaController {
	
	
	@Autowired
	private EmpresaService empresaService;
	
	@GetMapping("/{empresaCodigo}")
	public EmpresaResponseModel buscar(@PathVariable Long empresaCodigo) {
		return toResponseModel(empresaService.buscarPorCodigo(empresaCodigo));
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public List<EmpresaResponseModel> buscarTodas() {
		return toResponseModel(empresaService.buscarTodas());
	}
		
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	private EmpresaResponseModel adicionar(@RequestBody @Valid EmpresaRequestModel empresaRequest) {
		Empresa empresa = toDomain(empresaRequest);
		return toResponseModel(empresaService.adicionar(empresa));
	}
	
	@PutMapping("/{empresaCodigo}")
	@ResponseStatus(HttpStatus.OK)
	private EmpresaResponseModel atualizar(@PathVariable Long empresaCodigo, @RequestBody @Valid EmpresaRequestModel empresaRequest) {
		Empresa empresa = empresaService.buscarPorCodigo(empresaCodigo);
		mapToDomain(empresaRequest, empresa);
		return toResponseModel(empresaService.atualizar(empresa));
	}
	
	@PutMapping("/{empresaCodigo}/ativacao")
	@ResponseStatus(HttpStatus.OK)
	public void ativar(@PathVariable Long empresaCodigo) {
		empresaService.ativar(empresaCodigo);
	}
	
	@DeleteMapping("/{empresaCodigo}/ativacao")
	@ResponseStatus(HttpStatus.OK)
	public void inativar(@PathVariable Long empresaCodigo) {
		empresaService.inativar(empresaCodigo);
	}

}
