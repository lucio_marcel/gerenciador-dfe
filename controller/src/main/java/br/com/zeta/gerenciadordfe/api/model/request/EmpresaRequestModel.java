package br.com.zeta.gerenciadordfe.api.model.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.mapper.EmpresaMapper;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoAmbiente;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class EmpresaRequestModel {
	
	private static EmpresaMapper empresaMapper;
	
	@NotNull
	private Long codigo;
	
	@NotBlank
	private String CNPJ;
	
	@NotNull
	private TipoAmbiente tipoAmbiente;
	
	@NotBlank
	private String codigoMuncipio;
	
	@NotNull
	private boolean manifestacaoAutomaticaAtiva;
	
	@Autowired
	public void setEmpresaMapper(EmpresaMapper empresaMapper) {
		EmpresaRequestModel.empresaMapper = empresaMapper;
	}
	
	public static Empresa toDomain(EmpresaRequestModel empresaRequest) {
		return empresaMapper.toDomain(empresaRequest);
	}
	
	public static void mapToDomain(EmpresaRequestModel empresaRequest, Empresa empresa) {
		empresaMapper.mapToDomain(empresaRequest, empresa);
	}
}
