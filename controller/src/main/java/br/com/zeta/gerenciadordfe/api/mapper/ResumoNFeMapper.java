package br.com.zeta.gerenciadordfe.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.model.response.ResumoNFeResponseModel;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;

@Component
public class ResumoNFeMapper {
	
	private static ModelMapper mapper;
	
	@Autowired
	public void setModelMapper(ModelMapper mapper) {
		ResumoNFeMapper.mapper = mapper;
	}
	
	public ResumoNFeResponseModel toResponseModel(ResumoNFe resumoNFe) {
		ResumoNFeResponseModel resumoNFeResponseModel = mapper.map(resumoNFe, ResumoNFeResponseModel.class);
		resumoNFeResponseModel.setTemXmlDisponivel(resumoNFe.temXmlDisponivel());
		return resumoNFeResponseModel;
	}	
	
	public List<ResumoNFeResponseModel> toCollectionReponseModel(List<ResumoNFe> resumosNFe) {
		return resumosNFe.stream()
				.map(resumoNfe -> toResponseModel(resumoNfe))
				.collect(Collectors.toList());
	}

}
