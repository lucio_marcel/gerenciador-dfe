package br.com.zeta.gerenciadordfe.api.model.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class CertificadoRequestModel {
	
	@NotNull
	private TipoCertificado tipoCertificado;

	@NotBlank
	private String password;
	
	@NotNull
	private MultipartFile arquivo;
}