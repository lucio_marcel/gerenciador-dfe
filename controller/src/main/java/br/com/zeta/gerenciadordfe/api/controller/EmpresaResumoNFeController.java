package br.com.zeta.gerenciadordfe.api.controller;

import static br.com.zeta.gerenciadordfe.api.model.response.ResumoNFeResponseModel.toResponseModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.zeta.gerenciadordfe.api.model.response.ResumoNFeResponseModel;
import br.com.zeta.gerenciadordfe.domain.filter.ResumoNFeFilter;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.com.zeta.gerenciadordfe.domain.service.ResumoNFeService;

@RestController
@RequestMapping("/empresas/{empresaCodigo}/resumos-nfe")
public class EmpresaResumoNFeController {

	@Autowired
	private ResumoNFeService resumoNFeService;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Page<ResumoNFeResponseModel> buscarTodos(@PageableDefault(size = 10) Pageable pageable, ResumoNFeFilter filtro, @PathVariable Long empresaCodigo) {
		filtro.setEmpresaCodigo(empresaCodigo);
		Page<ResumoNFe> resumoPage = resumoNFeService.buscarTodos(filtro, pageable);
		return new PageImpl<>(toResponseModel(resumoPage.getContent()), pageable, resumoPage.getTotalElements());
	}
	
	@GetMapping("/{chaveAcesso}")
	@ResponseStatus(HttpStatus.OK)
	public ResumoNFeResponseModel buscarPorChaveAcesso(@PathVariable Long empresaCodigo, @PathVariable String chaveAcesso) {
		return toResponseModel(resumoNFeService.buscarPorChaveAcesso(empresaCodigo, chaveAcesso));
	}
	
	@GetMapping(value = "/{chaveAcesso}/xml", produces = MediaType.TEXT_XML_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> buscarXml(@PathVariable Long empresaCodigo, @PathVariable String chaveAcesso) {
		String xml = resumoNFeService.buscarXml(empresaCodigo, chaveAcesso);
		return ResponseEntity.ok().body(xml);
	}
	
}