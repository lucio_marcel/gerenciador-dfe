package br.com.zeta.gerenciadordfe.api.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.model.request.EmpresaRequestModel;
import br.com.zeta.gerenciadordfe.api.model.response.EmpresaResponseModel;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;

@Component
public class EmpresaMapper {
	
	private static ModelMapper mapper;
	
	@Autowired
	public void setModelMapper(ModelMapper mapper) {
		EmpresaMapper.mapper = mapper;
	}
	
	public Empresa toDomain(EmpresaRequestModel empresaRequest) {
		return mapper.map(empresaRequest, Empresa.class);
	}
	
	public void mapToDomain(EmpresaRequestModel empresaRequest, Empresa empresa) {
		empresa.setCertificado(new Certificado());
		mapper.map(empresaRequest,empresa);
	}
	
	public EmpresaResponseModel toResponseModel(Empresa empresa) {
		EmpresaResponseModel empresaResponseModel = mapper.map(empresa, EmpresaResponseModel.class);
		empresaResponseModel.setCertificadoValido(empresa.possuiCertificadoValido());
		return mapper.map(empresa, EmpresaResponseModel.class);
	}	
	
	public List<EmpresaResponseModel> toCollectionReponseModel(List<Empresa> empresas) {
		return empresas.stream()
				.map(empresa -> toResponseModel(empresa))
				.collect(Collectors.toList());
	}

}
