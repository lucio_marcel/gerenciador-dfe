package br.com.zeta.gerenciadordfe.api.controller;

import static br.com.zeta.gerenciadordfe.api.model.response.CertificadoResponseModel.toResponseModel;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.zeta.gerenciadordfe.api.model.request.CertificadoRequestModel;
import br.com.zeta.gerenciadordfe.api.model.response.CertificadoResponseModel;
import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;
import br.com.zeta.gerenciadordfe.domain.model.entity.CertificadoFileProperties;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;

@RestController
@RequestMapping("/empresas/{empresaCodigo}/certificado")
public class EmpresaCertificadoController {

	@Autowired
	private EmpresaService empresaService;
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public CertificadoResponseModel buscar(@PathVariable Long empresaCodigo) {
		Certificado certificado = empresaService.buscarCertificado(empresaCodigo);
		return toResponseModel(certificado);
		
	}
	
	@PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CertificadoResponseModel atualizarCertificado(@PathVariable Long empresaCodigo, @Valid CertificadoRequestModel certificadoRequest) throws CertificadoException  {
		
		Certificado certificado = null;
		
		CertificadoFileProperties certificadoFileProperties;
		
		try {
			certificadoFileProperties = CertificadoFileProperties.builder()
					.tipoCertificado(certificadoRequest.getTipoCertificado())
					.originalFilename(certificadoRequest.getArquivo().getOriginalFilename())
					.password(certificadoRequest.getPassword())
					.fileInputStream(certificadoRequest.getArquivo().getInputStream()).build();
		} catch (IOException e) {
			throw new ApplicationException(e.getMessage());
		}

		try {
			certificado = empresaService.atualizarCertificado(empresaCodigo, certificadoFileProperties);
		} catch (CertificadoException e) {
			throw new CertificadoException(e);
		}
		
		return toResponseModel(certificado);
	}
}
