package br.com.zeta.gerenciadordfe.api.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;
import br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe.EventoConsultaNFe;
import br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe.EventoManifestacaoDestinatario;
import br.com.zeta.gerenciadordfe.domain.service.dfe.ManifestacaoNFeService;
import br.com.zeta.gerenciadordfe.domain.service.distdfe.DistribuicaoDfeService;
import br.com.zeta.gerenciadordfe.domain.service.manifestacao.ManifestacaoDestinatarioService;

@RestController
@RequestMapping("/empresas/{empresaCodigo}/nfe")
public class EmpresaNFeController {
	
	@Autowired private ManifestacaoNFeService manifestacaoNFeService;
	@Autowired private DistribuicaoDfeService nfeDistDfeInteresseService;
	@Autowired private ManifestacaoDestinatarioService manifestacaoDestinatarioService;
	
	@PutMapping("/manifestacao-destinatario")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void manifestar(@PathVariable Long empresaCodigo,@RequestBody @Valid EventoManifestacaoDestinatario eventoManifestacaoDestinatario) {
		manifestacaoNFeService.manifestarNFe(empresaCodigo, eventoManifestacaoDestinatario);
	}
	
	// Em testes. Esta implementação passará a usar o Spring-WS
	@PutMapping("/manifestacao-destinatario-nova")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void manifestarNova(@PathVariable Long empresaCodigo,@RequestBody @Valid EventoManifestacaoDestinatario eventoManifestacaoDestinatario) {
		manifestacaoDestinatarioService.manifestarNFe(empresaCodigo, eventoManifestacaoDestinatario);
	}
	
	//Desativada. Passamos a utilizar a impelementaçaõque utiliza o Spring-WS, abaixo.
//	@GetMapping(value = "/consulta", produces = MediaType.TEXT_XML_VALUE)
//	@ResponseStatus(HttpStatus.OK)
//	public ResponseEntity<String> consultar(@PathVariable Long empresaCodigo, @RequestBody @Valid EventoConsultaNFe eventoConsultaNFe) {
//		String xml = distribuicaoDFeService.consultarDFePorChaveAcesso(empresaCodigo, eventoConsultaNFe.getChaveAcesso());
//		return ResponseEntity.ok().body(xml);
//	}
	
	@GetMapping(value = "/consulta", produces = MediaType.TEXT_XML_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<String> consultar(@PathVariable Long empresaCodigo, @RequestBody @Valid EventoConsultaNFe eventoConsultaNFe) {
		String xml = nfeDistDfeInteresseService.consultarPorChaveAcesso(empresaCodigo, eventoConsultaNFe.getChaveAcesso());
		return ResponseEntity.ok().body(xml);
	}
	
	@GetMapping(value = "/danfe", produces = MediaType.APPLICATION_PDF_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Resource> gerarDANFE(@PathVariable Long empresaCodigo, @RequestBody @Valid EventoConsultaNFe eventoConsultaNFe) {

		FileSystemResource xmlFile = new FileSystemResource(getTemporaryXmlFile(empresaCodigo, eventoConsultaNFe.getChaveAcesso()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_PDF));
		
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("file", xmlFile);
		
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
		
		String serverUrl = "http://www.webdanfe.com.br/danfe/GeraDanfe.php";
		
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<Resource> response = restTemplate.exchange(serverUrl, HttpMethod.POST, requestEntity, Resource.class);
		
		xmlFile.getFile().delete();
		
		return response;
		
	}
	
	private File getTemporaryXmlFile(Long empresaCodigo, String chaveAcesso) {
		String xml = nfeDistDfeInteresseService.consultarPorChaveAcesso(empresaCodigo, chaveAcesso);
		
		File temporayFile = null;
		
		try {
			temporayFile = File.createTempFile("NFe", ".xml");
		
			FileWriter writer = new FileWriter(temporayFile);
			writer.write(xml);
			writer.close();
		
		} catch (IOException e) {
			throw new ApplicationException(e.getMessage(), e);
			
		}
		
		return temporayFile;
		
	}

}