package br.com.zeta.gerenciadordfe.api.model.response;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.api.mapper.ResumoNFeMapper;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.SituacaoNFe;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.com.zeta.gerenciadordfe.domain.model.entity.Emitente;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class ResumoNFeResponseModel {
	
	private static ResumoNFeMapper resumoNFeMapper;

	private Long id;
	private Emitente emitente;
	private OffsetDateTime dataEmissao;
	private OffsetDateTime dataAutorizacao;
	private String chaveAcesso;
	private BigDecimal valor;
	private String numeroProtocolo;
	private String digestValue;
	private SituacaoNFe situacaoNfe;
	private Boolean manifestada;
	private OffsetDateTime dataManifestacao;
	private Boolean importada;
	private OffsetDateTime dataImportaqcao;
	private Integer numeroDownloads;
	private Boolean temXmlDisponivel;
	
	@Autowired
	public void setResumoNFeMapper(ResumoNFeMapper resumoNFeMapper) {
		ResumoNFeResponseModel.resumoNFeMapper = resumoNFeMapper;
	}
	
	public static ResumoNFeResponseModel toResponseModel(ResumoNFe resumoNFe) {
		return resumoNFeMapper.toResponseModel(resumoNFe);
	}
	
	public static List<ResumoNFeResponseModel> toResponseModel(List<ResumoNFe> resumosNfe) {
		return resumoNFeMapper.toCollectionReponseModel(resumosNfe);
	}
}
