//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.3.0 
// Consulte <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2021.03.24 às 01:13:11 PM BRT 
//


package br.inf.portalfiscal.nfe.distdfe;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.inf.portalfiscal.nfe.distdfe package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    //private final static QName _Signature_QNAME = new QName("http://www.w3.org/2000/09/xmldsig#", "Signature");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.inf.portalfiscal.nfe.distdfe
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DistDFeInt }
     * 
     */
    public DistDFeInt createDistDFeInt() {
        return new DistDFeInt();
    }

    /**
     * Create an instance of {@link RetDistDFeInt }
     * 
     */
    public RetDistDFeInt createRetDistDFeInt() {
        return new RetDistDFeInt();
    }

    /**
     * Create an instance of {@link RetDistDFeInt.LoteDistDFeInt }
     * 
     */
    public RetDistDFeInt.LoteDistDFeInt createRetDistDFeIntLoteDistDFeInt() {
        return new RetDistDFeInt.LoteDistDFeInt();
    }

    /**
     * Create an instance of {@link DistDFeInt.DistNSU }
     * 
     */
    public DistDFeInt.DistNSU createDistDFeIntDistNSU() {
        return new DistDFeInt.DistNSU();
    }

    /**
     * Create an instance of {@link DistDFeInt.ConsNSU }
     * 
     */
    public DistDFeInt.ConsNSU createDistDFeIntConsNSU() {
        return new DistDFeInt.ConsNSU();
    }

    /**
     * Create an instance of {@link DistDFeInt.ConsChNFe }
     * 
     */
    public DistDFeInt.ConsChNFe createDistDFeIntConsChNFe() {
        return new DistDFeInt.ConsChNFe();
    }

    /**
     * Create an instance of {@link ResEvento }
     * 
     */
    public ResEvento createResEvento() {
        return new ResEvento();
    }

    /**
     * Create an instance of {@link ResNFe }
     * 
     */
    public ResNFe createResNFe() {
        return new ResNFe();
    }

    /**
     * Create an instance of {@link RetDistDFeInt.LoteDistDFeInt.DocZip }
     * 
     */
    public RetDistDFeInt.LoteDistDFeInt.DocZip createRetDistDFeIntLoteDistDFeIntDocZip() {
        return new RetDistDFeInt.LoteDistDFeInt.DocZip();
    }

}
