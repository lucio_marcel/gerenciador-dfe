package br.com.zeta.gerenciadordfe.domain.event;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FinalizacaoConsultaDFeEvent {
	
	private Empresa empresa;
	
}
