package br.com.zeta.gerenciadordfe.domain.filter;

import java.time.OffsetDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.SituacaoNFe;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResumoNFeFilter {
	
	private Long empresaCodigo;
	
	private Boolean nfeManifestada;
	
	private Boolean nfeImportada;
	
	private SituacaoNFe situacaoNfe;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private OffsetDateTime dataEmissaoInicial;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private OffsetDateTime dataEmissaoFinal;

	@DateTimeFormat(iso = ISO.DATE_TIME)
	private OffsetDateTime dataAutorizacaoInicial;
	
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private OffsetDateTime dataAutorizacaoFinal;
}
