package br.com.zeta.gerenciadordfe.domain.event;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ManifestacaoNFeEvent {
	
	private ResumoNFe resuimoNFe;
	
}
