package br.com.zeta.gerenciadordfe.domain.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import br.com.zeta.gerenciadordfe.domain.event.FinalizacaoConsultaDFeEvent;
import br.com.zeta.gerenciadordfe.domain.service.dfe.GerenciadorDfeService;
import br.com.zeta.gerenciadordfe.domain.service.dfe.ManifestacaoNFeService;

@Component
public class FinalizacaoConsultaDFeListener {

	@Autowired
	private GerenciadorDfeService gerenciadorDfeService;
	
	@Autowired
	private ManifestacaoNFeService manifestacaoNFeService;
	
	@TransactionalEventListener
	public void aoFinalizarConsultaDfe(FinalizacaoConsultaDFeEvent event) {
		gerenciadorDfeService.agendarConsultaDFe(event.getEmpresa());
		
		if (event.getEmpresa().isManifestacaoAutomaticaAtiva()) {
			manifestacaoNFeService.manifestarNFeAutorizadas(event.getEmpresa().getCodigo());
		}
	}
}
