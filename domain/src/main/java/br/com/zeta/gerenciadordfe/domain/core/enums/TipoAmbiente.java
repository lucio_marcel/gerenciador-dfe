package br.com.zeta.gerenciadordfe.domain.core.enums;

import lombok.Getter;

@Getter
public enum TipoAmbiente {
	PRODUCAO("1"),
	HOMOLOGACAO("2");
	
	private final String codigo;
	
	TipoAmbiente(String codigo){
		this.codigo = codigo;
	}
	
}