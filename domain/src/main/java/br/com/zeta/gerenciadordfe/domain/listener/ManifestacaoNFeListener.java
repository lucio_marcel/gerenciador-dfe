package br.com.zeta.gerenciadordfe.domain.listener;

import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import br.com.zeta.gerenciadordfe.domain.event.ManifestacaoNFeEvent;

@Component
public class ManifestacaoNFeListener {


	
	@TransactionalEventListener
	public void aoManifestarNFe(ManifestacaoNFeEvent event) {
		Long empresaCodigo = event.getResuimoNFe().getEmpresa().getCodigo();
		String chaveAcesso = event.getResuimoNFe().getChaveAcesso();
				
//		distribuicaoNFeService.consultarDFePorChaveAcesso(empresaCodigo, chaveAcesso);
	}
}
