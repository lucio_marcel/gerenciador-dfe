package br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz;

public class ConsultaNFeException extends SefazException {
	private static final long serialVersionUID = 1L;

	public ConsultaNFeException(String mensagem) {
		super(mensagem);
	}
	
	public ConsultaNFeException(String status, String motivo) {
		this(String.format("A consulta não pôde ser realizada. Código do Erro: %s. Motivo: %s", status, motivo));
	}

}
