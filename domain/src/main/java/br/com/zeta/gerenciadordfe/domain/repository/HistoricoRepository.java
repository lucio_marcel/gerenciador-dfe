package br.com.zeta.gerenciadordfe.domain.repository;

import java.time.OffsetDateTime;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoConsulta;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Historico;

@Repository
public interface HistoricoRepository extends JpaRepository<Historico, Long>{

	@Query( "SELECT COUNT(historico) FROM Historico historico WHERE historico.empresa.id = :empresaId and historico.tipoConsulta= :tipoConsulta and historico.dataConsulta > :dataConsulta" )
	Long contarConsultasUltimaHoraPorTipo(@Param("empresaId") Long empresaId, @Param("tipoConsulta") TipoConsulta tipoConsulta,  @Param("dataConsulta") OffsetDateTime dataConsulta);

	
}
