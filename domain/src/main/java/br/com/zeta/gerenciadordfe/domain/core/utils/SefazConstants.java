package br.com.zeta.gerenciadordfe.domain.core.utils;

public class SefazConstants {
	
//	log
	
//	SERVIÇOS
	public static final String SERVICE_ENVIO_LOTE_ZIP = "#nfeAutorizacaoLoteZip";
	public static final String SERVICE_ENVIO_LOTE = "#nfeAutorizacaoLote";
	public static final String SERVICE_RET_ENVIO_LOTE = "#nfeRetAutorizacaoLote";
	public static final String SERVICE_EVENTO = "#nfeRecepcaoEvento";
	public static final String SERVICE_DOWNLOAD_NFE = "#nfeDownloadNF";
	public static final String SERVICE_INUTILIZACAO = "#nfeInutilizacao";
	public static final String SERVICE_DISTRIBUICAO_NFE = "#nfeDistribuicaoDFe";	
	
//	VERSÃO DOS LAYOUTS
	public static final String NFE_vENVI = "3.10";
	public static final String NFE_vRETENVI = "3.10"; 
	public static final String NFE_vEVE_CANC = "2.00";
	public static final String NFE_vEVE_CART = "2.00";
	public static final String NFE_vINUT = "3.10";

	public static final String NFE_vENVI_v4 = "4.00";
	public static final String NFE_vENVI_v4_6 = "4.6.0";

	public static final String NFE_vRETENVI_v4 = "4.00"; 
	public static final String NFE_vEVE_CANC_v4 = "4.00";
	public static final String NFE_vEVE_CART_v4 = "4.00";
	public static final String NFE_vINUT_v4 = "4.00";
	
/**
	PADRÕES DE NOMES PARA OS ARQUIVOS - Manual de Orientação ao Contribuinte 5.0 - Pág. 90 	
	- NF-e: O nome do arquivo será a chave de acesso completa com extensão “-nfe.xml”;
	- Envio de Lote de NF-e: O nome do arquivo será o número do lote com extensão “-env-lot.xml”;
	- Recibo: O nome do arquivo será o número do lote com extensão “-rec.xml”;
	- Pedido do Resultado do Processamento do Lote de NF-e: O nome do arquivo será o número do recibo com extensão “-ped-rec.xml”;
	- Resultado do Processamento do Lote de NF-e: O nome do arquivo será o número do recibo com extensão “-pro-rec.xml”;
	- Denegação de Uso: O nome do arquivo será a chave de acesso completa com extensão “-den.xml”;
	- Pedido de Cancelamento de NF-e: O nome do arquivo será a chave de acesso completa com extensão “-ped-can.xml”;
	- Cancelamento de NF-e: O nome do arquivo será a chave de acesso completa com extensão “-can.xml”;
	- Pedido de Inutilização de Numeração: O nome do arquivo será composto por: UF + Ano de inutilização + CNPJ do emitente + Modelo + Série + Número Inicial + Número Final com extensão “-ped-inu.xml”;
	- Inutilização de Numeração: O nome do arquivo será composto por: Ano de inutilização + CNPJ do emitente + Modelo + Série + Número Inicial + Número Final com extensão “-inu.xml”;
	- Pedido de Consulta Situação Atual da NF-e: O nome do arquivo será a chave de acesso completa com extensão “-ped-sit.xml”;
	- Situação Atual da NF-e: O nome do arquivo será a chave de acesso completa com extensão “-sit.xml”;
	- Pedido de Consulta do Status do Serviço: O nome do arquivo será: “AAAAMMDDTHHMMSS” do momento da consulta com extensão “-ped-sta.xml”;
	- Status do Serviço: O nome do arquivo será: “AAAAMMDDTHHMMSS” do momento da consulta com extensão “-sta.xml”;	 
*/
	
	public static final String EXTENSAO_PEDIDO_ENVIO_LOTE_ZIP = "_unzip-envlot.xml";
	public static final String EXTENSAO_PEDIDO_ENVIO_LOTE_PED = "-envlot.xml";	
	public static final String EXTENSAO_RETORNO_ENVIO_LOTE = "-rec.xml";
	
	public static final String EXTENSAO_PEDIDO_ENVIO_NFE = "-nfe.xml";
	public static final String EXTENSAO_RETOPRNO_ENVIO_NFE = "-rec.xml";
	
	public static final String EXTENSAO_PEDIDOD_CANCELAMENTO_NFE = "-ped-can.xml";
	public static final String EXTENSAO_RETORNO_CANCELAMENTO_NFE = "-ret-can.xml";	

	public static final String EXTENSAO_PEDIDO_MANIFESTACAO = "-ped-man.xml";
	public static final String EXTENSAO_RETORNO_MANIFESTACAO = "-ret-man.xml";
	
	public static final String EXTENSAO_PEDIDO_DOWNLOAD_NFE = "-ped-down.xml";
	public static final String EXTENSAO_RETORNO_DOWNLOAD_NFE = "-ret-down.xml";
	
	public static final String EXTENSAO_PEDIDO_INUTILIZACAO_NFE = "-ped-inu.xml";
	public static final String EXTENSAO_RETORNO_INUTILIZACAO_NFE = "-inu.xml";
	
	public static final String EXTENSAO_PEDIDO_EVENTO = "-ped-eve.xml";
	public static final String EXTENSAO_RETORNO_EVENTO = "-eve.xml";
	
	public static final String EXTENSAO_PROCESAMENTO_NFE = "-procNFe.xml";
	public static final String EXTENSAO_PROCESSAMENTO_EVENTO_NFE = "-procEventoNFe.xml";
	
	public static final String EXTENSAO_CONSULTA_DISTRIBUICAO_DFE = "-con-dist-dfe.xml";
	public static final String EXTENSAO_RETORNO_DISTTRIBUICAO_DFE = "-ret-dist-dfe.xml";
	public static final String EXTENSAO_RESUMO_NFE = "-res_nfe.xml";
	
	//  PREENCHIMENTO
	public static final String NFE = "NFe";
	public static final String INSCRICAO_ESTADUAL_CONTRIBUINTE = "1";
	public static final String INSCRICAO_ESTADUAL_ISENTO = "2";
	public static final String INSCRICAO_ESTADUAL_NAO_CONTRIBUINTE = "9";
	public static final String ISENTO = "ISENTO";
	
	public static final String CODIGO_PAIS_BRASIL = "1058";
	
	public static final String NOME_MUNICIPIO_EXTERIOR = "EXTERIOR";
	public static final String CODIGO_UF_EXTERIOR = "EX";
	
	public static final String PROCESSAMENTO_SINCRONO = "1";
	public static final String PROCESSAMENTO_ASSINCRONO = "0";
	
	public static final String AMBIENTE_PRODUCAO = "1";
	public static final String AMBIENTE_HOMOLOGACAO = "2";
	
	public static final String CODIGO_EVENTO_CANCELAMENTO = "110111";
	public static final String DESCRICAO_EVENTO_CANCELAMENTO = "Cancelamento";
	
	public static final String TIPO_ITEM_PRODUTO = "PRO";
	public static final String TIPO_ITEM_SERVICO = "SER";
	
	public static final String MODELO_NFE = "55";
	public static final String MODELO_NFCE = "65";
	
	public static final Byte FINALIDADE_USO_IMPORTACAO = 2;
	
	public static final String NFE_HOMOLOGACAO = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
	
	public static final String NFCE_ITEM_HOMOLOGACAO = "NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
	
//	CONTEXTPATH	
	public static final String CONTEXTPATH_ENVIO_v3 = new String("br.inf.portalfiscal.nfe");
	public static final String CONTEXTPATH_CONSULTA_v3 = new String("br.inf.portalfiscal.nfe.wsdl.nferetautorizacao");
	public static final String CONTEXTPATH_CCE_v3 = new String("br.inf.portalfiscal.nfe.wsdl.recepcaoeventocartacorrecao");
	public static final String CONTEXTPATH_EVENTO_v3 = new String("br.inf.portalfiscal.nfe.wsdl.recepcaoevento");
	public static final String CONTEXTPATH_INUTILIZACAO_v3 = new String("br.inf.portalfiscal.nfe.wsdl.nfeinutilizacao2");
	
	public static final String CONTEXTPATH_ENVIO_v4 = new String("br.inf.portalfiscal.nfe.v4");
	public static final String CONTEXTPATH_CONSULTA_v4 = new String("br.inf.portalfiscal.nfe.v4.wsdl.nferetautorizacao4");
	public static final String CONTEXTPATH_CCE_v4 = new String("br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4");
	public static final String CONTEXTPATH_EVENTO_v4 = new String("br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4");
	public static final String CONTEXTPATH_INUTILIZACAO_v4 = new String("br.inf.portalfiscal.nfe.v4.wsdl.nfeinutilizacao4");
	
//	TAGS P/ ASSINATURA
	public static final String ID = "Id";
	public static final String TAG_ENVIO = "infNFe";
	public static final String TAG_CANC = "infCanc";
	public static final String TAG_INUT = "infInut";
	public static final String TAG_INFEVE = "infEvento";
	public static final String TAG_EVENTO = "evento";

	public static final String SEM_PAGAMENTO = "90";
	
	public static final String NFE_NORMAL = "1";
	public static final String NFE_COMPLEMENTAR	 = "2";
	public static final String NFE_AJUSTE = "3";
	public static final String NFE_DEVOLUCAO = "4";
	
	public static final String LOCAL_DESTINO_INTERNO = "1";
	public static final String LOCAL_DESTINO_INTERESTADUAL = "2";
	public static final String LOCAL_DESTINO_EXTERIOR = "3";
	
	public static final String RESP_TECN_CNPJ = "72431703000185";
	public static final String RESP_TECN_CONTATO = "Zeta Informatica";
	public static final String RESP_TECN_EMAIL = "zeta@zeta.com.br";
	public static final String RESP_TECN_FONE = "5133307755";
	
	public static String getCodigoUf(String uf) {
		if ("AC".equals(uf)) return "12";
		else if ("AL".equals(uf)) return "27";
		else if ("AM".equals(uf)) return "13";
		else if ("AP".equals(uf)) return "16";
		else if ("BA".equals(uf)) return "29";
		else if ("CE".equals(uf)) return "23";
		else if ("DF".equals(uf)) return "53";
		else if ("ES".equals(uf)) return "32";
		else if ("GO".equals(uf)) return "52";
		else if ("MA".equals(uf)) return "21";
		else if ("MG".equals(uf)) return "31";
		else if ("MT".equals(uf)) return "51";
		else if ("MS".equals(uf)) return "50";
		else if ("PA".equals(uf)) return "15";
		else if ("PB".equals(uf)) return "25";
		else if ("PE".equals(uf)) return "26";
		else if ("PI".equals(uf)) return "22";
		else if ("PR".equals(uf)) return "41";
		else if ("RJ".equals(uf)) return "33";
		else if ("RN".equals(uf)) return "24";
		else if ("RO".equals(uf)) return "11";
		else if ("RR".equals(uf)) return "14";
		else if ("RS".equals(uf)) return "43";
		else if ("SC".equals(uf)) return "42";
		else if ("SE".equals(uf)) return "28";
		else if ("SP".equals(uf)) return "35";
		else if ("TI".equals(uf)) return "17";
		else return "";
	}
	
//	public static String returnNS(TipoProcesso processo, String uf) {
//		if (getCodigoUf("PE").equals(uf))
//			switch (processo) {
//			case ENVIAR: 
//				return " xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/NfeAutorizacao\"";
//			case CONSULTAR: 
//				return " xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/NfeRetAutorizacao\"";
//			case CANCELAR: 
//			case CORRIGIR:
//				return " xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento\"";
//			case INUTILIZAR: 
//				return " xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/NfeInutilizacao2\"";
//			default:
//				return "";
//			}			
//		else
//			return "";
//	}
}
