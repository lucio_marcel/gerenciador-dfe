package br.com.zeta.gerenciadordfe.domain.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("zeta")
public class XmlStorageProperties {

	private String xmlStorageLocalFolder = "C:/GerenciadorDFe/";
	private boolean xmlStorageConsultaDfeActive;
	private boolean xmlStorageManifestacaoDestinatarioActive;
	private boolean xmlStorageDownloadNfeActive;
}
