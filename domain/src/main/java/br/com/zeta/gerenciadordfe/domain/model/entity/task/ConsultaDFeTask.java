package br.com.zeta.gerenciadordfe.domain.model.entity.task;

import org.springframework.scheduling.annotation.Async;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.service.dfe.GerenciadorDfeService;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor
public class ConsultaDFeTask implements Runnable {

	private final GerenciadorDfeService gerenciadorDfeService;

	private final Empresa empresa;

	@Override
	@Async
	public void run() {
		gerenciadorDfeService.consultarDFe(empresa);
	}

}
