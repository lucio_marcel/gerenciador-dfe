package br.com.zeta.gerenciadordfe.domain.core.configuration;

import java.nio.file.Path;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("zeta")
public class CertificadoStorageProperties {

	private Path certificadoStorageLocalFolder;
	
}
