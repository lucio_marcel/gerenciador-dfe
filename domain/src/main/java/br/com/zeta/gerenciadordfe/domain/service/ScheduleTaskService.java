package br.com.zeta.gerenciadordfe.domain.service;

import java.time.OffsetDateTime;

public interface ScheduleTaskService {

	void addTaskToScheduler(Long id, Runnable task);
	
	void addTaskToScheduler(Long taskId, Runnable task, OffsetDateTime startTime);
	
	void removeTaskFromScheduler(Long id);
	
	void contextRefreshedEvent();
}
