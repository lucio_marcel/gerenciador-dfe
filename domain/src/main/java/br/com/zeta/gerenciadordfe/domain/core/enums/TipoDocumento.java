package br.com.zeta.gerenciadordfe.domain.core.enums;

import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.domain.core.configuration.XmlStorageProperties;

public enum TipoDocumento {
	
	ENVIO_CONSULTA_DFE("/CONSULTA_DFE"),
	RETORNO_CONSULTA_DFE("/CONSULTA_DFE"), 
	ENVIO_MANIFESTACAO_DESTINATARIO("/MANIFESTACAO_DESTINATARIO"),
	RETORNO_MANIFESTACAO_DESTINATARIO("/MANIFESTACAO_DESTINATARIO"),
	PEDIDO_DOWNLOAD_NFE("/NFE_4.0/NFE"),
	RETORNO_DOWNLOAD_NFE("/NFE_4.0/NFE"),
	RESUMO_NFE("/NFE_4.0/RESUMO_NFE");
	
	private XmlStorageProperties xmlStorageProperties;

	private String folder;

	TipoDocumento(String xmlFolder) {
		this.folder = xmlFolder;
	}

	private void setXmlStorageProperties(XmlStorageProperties xmlStorageProperties) {
		this.xmlStorageProperties = xmlStorageProperties;
	}

	public String getLocalXmlFolder(TipoDocumento tipoDocumento, String cnpj) {
		if (isActiveStorage(tipoDocumento)) {
			StringBuilder diretorio = new StringBuilder().append(xmlStorageProperties.getXmlStorageLocalFolder())
					.append("/").append(cnpj).append("/").append(tipoDocumento.folder).append("/")
					.append(String.format("%04d", LocalDateTime.now().atZone(ZoneId.of("America/Sao_Paulo")).getYear()))
					.append("/")
					.append(String.format("%02d",
							LocalDateTime.now().atZone(ZoneId.of("America/Sao_Paulo")).getMonthValue()))
					.append("/").append(String.format("%02d",
							LocalDateTime.now().atZone(ZoneId.of("America/Sao_Paulo")).getDayOfMonth()))
					.append("/");

			return diretorio.toString();
		}

		return null;
	}

	public String getXmlFileExtension(TipoDocumento tipoDocumento) {
		switch (tipoDocumento) {
		case ENVIO_CONSULTA_DFE:
			return EXTENSAO_CONSULTA_DISTRIBUICAO_DFE;
			
		case RETORNO_CONSULTA_DFE:
			return EXTENSAO_RETORNO_DISTTRIBUICAO_DFE;
			
		case RESUMO_NFE:
			return EXTENSAO_RESUMO_NFE;

		case ENVIO_MANIFESTACAO_DESTINATARIO:
			return EXTENSAO_PEDIDO_MANIFESTACAO;
			
		case RETORNO_MANIFESTACAO_DESTINATARIO:
			return EXTENSAO_RETORNO_MANIFESTACAO;
			
		case PEDIDO_DOWNLOAD_NFE:
			return EXTENSAO_PEDIDO_DOWNLOAD_NFE;
				
		case RETORNO_DOWNLOAD_NFE:
				return EXTENSAO_RETORNO_DOWNLOAD_NFE;

		default:
			return null;

		}

	}

	private boolean isActiveStorage(TipoDocumento tipoDocumento) {
		switch (tipoDocumento) {
		case ENVIO_CONSULTA_DFE:
		case RETORNO_CONSULTA_DFE:
		case RESUMO_NFE:
			return xmlStorageProperties.isXmlStorageConsultaDfeActive();

		case ENVIO_MANIFESTACAO_DESTINATARIO:
		case RETORNO_MANIFESTACAO_DESTINATARIO:
			return xmlStorageProperties.isXmlStorageManifestacaoDestinatarioActive();

		case PEDIDO_DOWNLOAD_NFE:
		case RETORNO_DOWNLOAD_NFE:
			return xmlStorageProperties.isXmlStorageDownloadNfeActive();

		default:
			return false;
		}

	}

	@Component
	public static class ServiceInjector {
		@Autowired
		private XmlStorageProperties xmlStorageProperties;

		@PostConstruct
		public void postConstruct() {
			for (TipoDocumento item : EnumSet.allOf(TipoDocumento.class)) {
				item.setXmlStorageProperties(xmlStorageProperties);
			}
		}
	}

}