package br.com.zeta.gerenciadordfe.domain.service.distdfe;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;

public interface DistribuicaoDfeService {
	
	String consultarPorChaveAcesso(Long empresaCodigo, String chaveAcesso);

	void consultarPorUltimoNSU(Empresa empresa);
}


