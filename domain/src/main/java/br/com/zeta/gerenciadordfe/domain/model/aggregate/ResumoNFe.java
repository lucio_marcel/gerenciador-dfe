package br.com.zeta.gerenciadordfe.domain.model.aggregate;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.domain.AbstractAggregateRoot;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.SituacaoNFe;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.TipoOperacao;
import br.com.zeta.gerenciadordfe.domain.event.ManifestacaoNFeEvent;
import br.com.zeta.gerenciadordfe.domain.model.entity.Emitente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "resumo_nfe", uniqueConstraints = {@UniqueConstraint(columnNames = "chaveAcesso")}) 
public class ResumoNFe extends AbstractAggregateRoot<ResumoNFe>{

	@Id
	@EqualsAndHashCode.Include
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Embedded
	private Emitente emitente;

	@Column(nullable = false, columnDefinition = "timestamp with time zone")
	private OffsetDateTime dataEmissao;

	@Column(nullable = false, columnDefinition = "timestamp with time zone")
	private OffsetDateTime dataAutorizacao;

	@Column(nullable = false)
	private String chaveAcesso;

	@Column(nullable = false)
	private BigDecimal valor;

	@Column(nullable = false)
	private String numeroProtocolo;

	@Column(nullable = false)
	private String digestValue;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private SituacaoNFe situacaoNfe;
	
	@Column(nullable = true)
	@Enumerated(EnumType.STRING)
	private TipoOperacao tipoOperacao;

	@Column(nullable = true)
	private boolean nfeManifestada;
	
	@Column(nullable = true, columnDefinition = "timestamp with time zone")
	private OffsetDateTime dataManifestacao;
	
	private boolean nfeImportada;
	
	@Column(columnDefinition = "timestamp with time zone")
	private OffsetDateTime dataImportacao;
	
	private Integer numeroDownloads;
	
	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "timestamp with time zone")
	private OffsetDateTime createdAt;

	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "timestamp with time zone")
	private OffsetDateTime updatedAt;
	
	@org.hibernate.annotations.Type(type = "br.com.zeta.gerenciadordfe.domain.core.type.SQLXMLType")
	@Column(columnDefinition = "xml")
	private String xml;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private Empresa empresa;
	
	public void manifestacaoRealizada() {
		registerEvent(new ManifestacaoNFeEvent(this));
	}
	
	public boolean temXmlDisponivel() {
		return ((this.xml != null) ? true : false);
	}
}
