package br.com.zeta.gerenciadordfe.domain.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.zeta.gerenciadordfe.domain.filter.ResumoNFeFilter;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.inf.portalfiscal.nfe.distdfe.ResNFe;
import br.inf.portalfiscal.nfe.nfe.TNfeProc;

public interface ResumoNFeService {
	
	ResumoNFe salvar(ResumoNFe resumoNfe);
	
	void adicionar(ResumoNFe resumoNfe);
	
	void adicionar(Empresa empresa, ResNFe resNFe);
	
	void adicionar(Empresa empresa, TNfeProc tNfeProc);
	
	void salvarXmlNFe(Long empresaCodigo,String chaveAcesso, String xmlNFe);
	
	ResumoNFe buscarPorChaveAcesso(Long empresaCodigo, String chaveAcesso);
	
	String buscarXml(Long empresaCodigo, String chaveAcesso);
	
	Page<ResumoNFe> buscarTodos(ResumoNFeFilter filtro, Pageable pageable);
	
	List<ResumoNFe> buscarTodosComManifestacaoPendente(Long empresaId);

	
}
