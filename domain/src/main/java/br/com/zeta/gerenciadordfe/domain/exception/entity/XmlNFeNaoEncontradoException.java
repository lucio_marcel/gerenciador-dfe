package br.com.zeta.gerenciadordfe.domain.exception.entity;

public class XmlNFeNaoEncontradoException extends EntityNotFoundException {

	private static final long serialVersionUID = 1L;

	public XmlNFeNaoEncontradoException(String message) {
		super(message);
	}

	public XmlNFeNaoEncontradoException(Long empresaCodigo, String chaveAcesso) {
		this(String.format("Não existe XML de nfe com a chave de acesso: %s cadastrada para a empresa de código: %d", chaveAcesso, empresaCodigo));
	}
}
