package br.com.zeta.gerenciadordfe.domain.service.storage;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.UUID;

import lombok.Builder;
import lombok.Getter;

public interface CertificadoStorageService {

	Path armazenar(NovoCertificado novoCertificado);
	
	void remover(String nomeArquivo);
	
	default Path substituir(String nomeArquivoAntigo, NovoCertificado novoCertificado) {
		Path path = this.armazenar(novoCertificado);
		
		if (nomeArquivoAntigo != null) {
			this.remover(nomeArquivoAntigo);
		}
		
		return path;
	}
	
	default String gerarNomeArquivo(String nomeOriginal) {
		return UUID.randomUUID() + "__" + nomeOriginal;
	}
	
	@Builder
	@Getter
	class NovoCertificado {
		private String nomeArquivo;
		private InputStream inputStream;
	}
	
}
