package br.com.zeta.gerenciadordfe.domain.exception.dfe;

import br.com.zeta.gerenciadordfe.domain.exception.DomainException;

public class ValidacaoEventoException extends DomainException{

	private static final long serialVersionUID = 1L;

	public ValidacaoEventoException(String mensagem) {
		super(mensagem);
	}

}
