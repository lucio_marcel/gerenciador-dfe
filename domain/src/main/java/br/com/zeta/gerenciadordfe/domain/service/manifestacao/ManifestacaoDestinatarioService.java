package br.com.zeta.gerenciadordfe.domain.service.manifestacao;

import br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe.EventoManifestacaoDestinatario;

public interface ManifestacaoDestinatarioService {

	void manifestarNFe(Long empresaCodigo, EventoManifestacaoDestinatario eventoManifestacaoDestinatario);

	void manifestarNFeAutorizadas(Long empresaCodigo);
}
