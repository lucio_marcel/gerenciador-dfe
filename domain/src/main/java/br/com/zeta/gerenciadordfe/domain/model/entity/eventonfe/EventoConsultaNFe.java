package br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventoConsultaNFe {
	
	@NotBlank
	private String chaveAcesso;

}
