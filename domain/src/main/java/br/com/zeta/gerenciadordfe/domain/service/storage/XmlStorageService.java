package br.com.zeta.gerenciadordfe.domain.service.storage;

import org.springframework.ws.WebServiceMessage;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento;

public interface XmlStorageService {
	
	void salvar(TipoDocumento tipoDocumento, String nomeArquivo, String xml, String cnpj);
	
	void salvar(TipoDocumento tipoDocumento, String nomeArquivo, WebServiceMessage webServiceMessage, String cnpj);
}
