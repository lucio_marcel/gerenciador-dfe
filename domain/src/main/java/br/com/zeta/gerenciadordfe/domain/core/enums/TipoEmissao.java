package br.com.zeta.gerenciadordfe.domain.core.enums;

public enum TipoEmissao {
	NORMAL, CONTIGENCIA;
}
