package br.com.zeta.gerenciadordfe.domain.exception;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ApplicationException(String mensagem) {
		super(mensagem);
	}
	
	public ApplicationException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
}
