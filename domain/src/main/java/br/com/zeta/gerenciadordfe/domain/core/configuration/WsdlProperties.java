package br.com.zeta.gerenciadordfe.domain.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("sefaz.wsdl")
public class WsdlProperties {
	
	private Servico consultaDocumentos = new Servico();
	private Servico manifestacao = new Servico();
	
	@Getter
	@Setter
	public class Servico {
		private Ambiente homologacao = new Ambiente();
		private Ambiente producao = new Ambiente();
				
		@Getter
		@Setter
		public class Ambiente {
			private String contigencia;
			private String normal;
		}
		
	}
	
}