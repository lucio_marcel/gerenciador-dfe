package br.com.zeta.gerenciadordfe.domain.service;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.List;

import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;

public interface CertificadoService{
	
	void inicializaCertificado(Certificado certificado, InputStream cacertInputStream) throws CertificadoException;
    
    Certificado certificadoPfx(String filePath, String password) throws CertificadoException;
    
    List<Certificado> listaCertificadosWindows() throws CertificadoException;
    
    KeyStore getKeyStore(Certificado certificado) throws CertificadoException;
    
    X509Certificate getCertificate(Certificado certificado, KeyStore keystore) throws CertificadoException;
    
    Certificado getCertificadoByCnpjCpf(String cnpjCpf) throws CertificadoException;

}
