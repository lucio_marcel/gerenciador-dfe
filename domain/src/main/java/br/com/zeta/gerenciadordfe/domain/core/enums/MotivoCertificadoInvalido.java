package br.com.zeta.gerenciadordfe.domain.core.enums;

import lombok.Getter;

@Getter
public enum MotivoCertificadoInvalido {
	
	CERTIFICADO_EXPIRADO("O certificado está expirado"),
	EMPRESA_NAO_POSSUI_CERTIFICADO("A empresa não possui certificado"),
	SENHA_DO_CERTIFICADO_NAO_INFORMADA("A senha do certificado não foi informada"),
	CNPJ_CERTIFICADO_INVALIDO("O CNPJ do certificado não corresponde ao CNPJ da empresa."),
	MOTIVO_DESCONHECIDO("Motivo desconhecido");
	
	private String descricao;
	
	MotivoCertificadoInvalido(String descricao) {
		this.descricao = descricao;
	}

}
