package br.com.zeta.gerenciadordfe.domain.exception.entity;

public class EmpresaNaoEncontradaException extends EntityNotFoundException{

	private static final long serialVersionUID = 1L;

	public EmpresaNaoEncontradaException(String message) {
		super(message);
	}
	
	public EmpresaNaoEncontradaException(String campo, Long valor) {
		this(String.format("Não existe empresa cadastrada com o %s: %d", campo, valor));
	}

}
