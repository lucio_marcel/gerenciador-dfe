package br.com.zeta.gerenciadordfe.domain.core.enums;

import lombok.Getter;

@Getter
public enum TipoConsulta {
	CONSULTA_POR_CHAVE_ACESSO,
	CONSULTA_POR_ULTIMO_NSU;

}
