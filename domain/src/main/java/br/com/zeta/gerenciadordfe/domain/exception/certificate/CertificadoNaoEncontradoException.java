package br.com.zeta.gerenciadordfe.domain.exception.certificate;

public class CertificadoNaoEncontradoException extends CertificadoException {

	private static final long serialVersionUID = 1L;

	public CertificadoNaoEncontradoException(String message) {
		super(message);
	}
}
