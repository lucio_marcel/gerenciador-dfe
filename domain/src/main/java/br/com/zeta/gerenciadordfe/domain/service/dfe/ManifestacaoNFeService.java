package br.com.zeta.gerenciadordfe.domain.service.dfe;

import br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe.EventoManifestacaoDestinatario;

public interface ManifestacaoNFeService{
	
	void manifestarNFe(Long empresaCodigo, EventoManifestacaoDestinatario eventoManifestacaoDestinatario);
	
	void manifestarNFeAutorizadas(Long empresaCodigo);
}
