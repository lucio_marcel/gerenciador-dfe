package br.com.zeta.gerenciadordfe.domain.core.enums;

import java.util.Arrays;

import lombok.Getter;

@Getter
public enum TipoCertificado {
	REPOSITORIO_WINDOWS ("WINDOWS"),
    ARQUIVO_PFX("ARQUIVO_PFX");

    private final String descricao;

    TipoCertificado(String descricao) {
        this.descricao = descricao;
    }

    public static TipoCertificado valueOfDescricao(String descricao) {
        return Arrays.stream(values()).
        		filter(tipoCertificado -> tipoCertificado.getDescricao().equals(descricao)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
