package br.com.zeta.gerenciadordfe.domain.exception.entity;

import br.com.zeta.gerenciadordfe.domain.exception.DomainException;

public class EntityNotFoundException  extends DomainException {

	private static final long serialVersionUID = 1L;
	
	public EntityNotFoundException(String message) {
		super(message);
	}
	
}
