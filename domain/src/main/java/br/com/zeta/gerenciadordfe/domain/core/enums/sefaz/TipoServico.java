package br.com.zeta.gerenciadordfe.domain.core.enums.sefaz;

public enum TipoServico {
	CONSULTA_DOCUMENTOS,
	MANIFESTACAO

}
