package br.com.zeta.gerenciadordfe.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;

@Repository
public interface ResumoNFeRepository extends JpaRepository<ResumoNFe, Long>, JpaSpecificationExecutor<ResumoNFe>, ResumoNFeRepositoryQueries {
	
	Optional<ResumoNFe> findByEmpresaCodigoAndChaveAcesso(Long empresaCodigo, String chaveAcesso); 
	  
	Page<ResumoNFe> findAll(Specification<ResumoNFe> specification, Pageable pageable);
	
	List<ResumoNFe> findAll(Specification<ResumoNFe> specification);
	
	boolean existsByChaveAcesso(String chaveAcesso);

}
