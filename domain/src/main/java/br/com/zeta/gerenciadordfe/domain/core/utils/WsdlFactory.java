package br.com.zeta.gerenciadordfe.domain.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.domain.core.configuration.ConfigProperties;
import br.com.zeta.gerenciadordfe.domain.core.configuration.WsdlProperties;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoAmbiente;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoEmissao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class WsdlFactory {
	
	private static WsdlProperties wsdlProperties;
	private static ConfigProperties configProperties;
	
	@Autowired
	public void setWsdlProperties(WsdlProperties wsdlProperties){
		WsdlFactory.wsdlProperties = wsdlProperties;
	}
	
	@Autowired
	public void setConfigProperties(ConfigProperties configProperties){
		WsdlFactory.configProperties = configProperties;
	}

	public String getWsdlConsultaDocumentos() {
		return getWsdlConsultaDocumentos(TipoAmbiente.PRODUCAO);
	}
	
	public static String getWsdlConsultaDocumentos(TipoAmbiente tipoAmnbiente) {
		switch (tipoAmnbiente) {
		case PRODUCAO:
			return (TipoEmissao.NORMAL.name().equalsIgnoreCase(configProperties.getTipoEmissao().name())
					? wsdlProperties.getConsultaDocumentos().getProducao().getNormal()
					: wsdlProperties.getConsultaDocumentos().getProducao().getContigencia());

		case HOMOLOGACAO:
			return (TipoEmissao.NORMAL.name().equalsIgnoreCase(configProperties.getTipoEmissao().name())
					? wsdlProperties.getConsultaDocumentos().getHomologacao().getNormal()
					: wsdlProperties.getConsultaDocumentos().getHomologacao().getContigencia());

		default:
			return null;
		}
		
	}
	
	public String getWsdlManifestacao() {
		return getWsdlManifestacao(configProperties.getTipoAmbiente());
	}
	
	public static String getWsdlManifestacao(TipoAmbiente tipoAmnbiente) {
		switch (tipoAmnbiente) {
		case PRODUCAO:
			return (TipoEmissao.NORMAL.name().equalsIgnoreCase(configProperties.getTipoEmissao().name())
					? wsdlProperties.getManifestacao().getProducao().getNormal()
					: wsdlProperties.getManifestacao().getProducao().getContigencia());

		case HOMOLOGACAO:
			return (TipoEmissao.NORMAL.name().equalsIgnoreCase(configProperties.getTipoEmissao().name())
					? wsdlProperties.getManifestacao().getHomologacao().getNormal()
					: wsdlProperties.getManifestacao().getHomologacao().getContigencia());

		default:
			return null;
		}
	}
	
}
