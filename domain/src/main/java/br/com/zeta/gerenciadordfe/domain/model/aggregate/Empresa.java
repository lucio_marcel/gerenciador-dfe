package br.com.zeta.gerenciadordfe.domain.model.aggregate;

import static br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido.CERTIFICADO_EXPIRADO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido.CNPJ_CERTIFICADO_INVALIDO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido.EMPRESA_NAO_POSSUI_CERTIFICADO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido.MOTIVO_DESCONHECIDO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido.SENHA_DO_CERTIFICADO_NAO_INFORMADA;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.domain.AbstractAggregateRoot;

import br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido;
import br.com.zeta.gerenciadordfe.domain.core.enums.StatusEmpresa;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoAmbiente;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta;
import br.com.zeta.gerenciadordfe.domain.event.AtivacaoEmpresaEvent;
import br.com.zeta.gerenciadordfe.domain.event.FinalizacaoConsultaDFeEvent;
import br.com.zeta.gerenciadordfe.domain.event.InativacaoEmpresaEvent;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
@Entity
public class Empresa extends AbstractAggregateRoot<Empresa> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;

	@Column(nullable = false, unique = true)
	private Long codigo;

	@Column(nullable = false, unique = true)
	private String cnpj;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private StatusEmpresa statusEmpresa;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoAmbiente tipoAmbiente;

	@Column(nullable = false)
	private String codigoMuncipio;

	private String ultimoNsuConsultado = "000000000000000";

	private String maiorNsuExistente = "000000000000000";

	@Column(columnDefinition = "timestamp with time zone")
	private OffsetDateTime DataUltimaConsulta;

	@Enumerated(EnumType.STRING)
	private StatusResposta statusUltimaConsulta;

	private boolean manifestacaoAutomaticaAtiva;

	@Embedded
	@Column(nullable = false)
	private Certificado certificado;

	@CreationTimestamp
	@Column(nullable = false, columnDefinition = "timestamp with time zone")
	private OffsetDateTime createdAt;

	@UpdateTimestamp
	@Column(nullable = false, columnDefinition = "timestamp with time zone")
	private OffsetDateTime updatedAt;

	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	private Set<ResumoNFe> resumosNFe = new HashSet<>();
	
	
	@OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	private Set<ResumoNFe> historico = new HashSet<>();

	public void ativar() {
		if (!estaAtiva() && possuiCertificadoValido()) {
			setStatusEmpresa(StatusEmpresa.ATIVO);
			registerEvent(new AtivacaoEmpresaEvent(this));
		}
	}

	public void inativar() {
		if (estaAtiva()) {
			setStatusEmpresa(StatusEmpresa.INATIVO);
			registerEvent(new InativacaoEmpresaEvent(this));
		}
	}

	public boolean estaAtiva() {
		return getStatusEmpresa().equals(StatusEmpresa.ATIVO);
	}

	public boolean possuiCertificadoValido() {

		if (this.getCertificado() != null && this.getCnpj().equals(getCertificado().getCnpjCpf())) {
			if (this.getCertificado().getVencimento() != null) {
				return this.getCertificado().expirado() ? false : true;
			}
		}

		return false;
		
	}
	public MotivoCertificadoInvalido motivoCertificadoInvalido() {

		if (!possuiCertificadoValido()) {
			if (this.getCertificado().getFilePath() == null) {
				return EMPRESA_NAO_POSSUI_CERTIFICADO;
			} else if (this.getCertificado().getDiasRestantes() == null || this.getCertificado().getDiasRestantes() < 1) {
				return CERTIFICADO_EXPIRADO;
			} else if (this.getCertificado().getPassword() == null) {
				return SENHA_DO_CERTIFICADO_NAO_INFORMADA;
			} else if (!this.getCertificado().getCnpjCpf().equals(this.getCnpj())) {
				return CNPJ_CERTIFICADO_INVALIDO;
			} else {
				return MOTIVO_DESCONHECIDO;
			}
		} else {
			return null;
		}
	}

	public void finalizarConsuultaDFe() {
		registerEvent(new FinalizacaoConsultaDFeEvent(this));
	}
}
