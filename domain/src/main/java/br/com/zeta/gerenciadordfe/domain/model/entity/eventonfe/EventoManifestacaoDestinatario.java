package br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class EventoManifestacaoDestinatario {
	
	@NotBlank
	private String chaveAcesso;
	
	@NotBlank
	private String tipoEvento;
	
	private String justificativa;

}
