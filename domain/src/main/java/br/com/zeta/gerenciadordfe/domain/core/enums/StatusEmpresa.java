package br.com.zeta.gerenciadordfe.domain.core.enums;

public enum StatusEmpresa {
	ATIVO,
	INATIVO
}
