package br.com.zeta.gerenciadordfe.domain.exception.dfe;

import br.com.zeta.gerenciadordfe.domain.exception.DomainException;

public class NFeCanceladaException extends DomainException {

	private static final long serialVersionUID = 1L;

	public NFeCanceladaException(String chaveAcesso) {
		super(String.format("A NFe com chave de acesso %s está cancelada.", chaveAcesso));
	}

}
