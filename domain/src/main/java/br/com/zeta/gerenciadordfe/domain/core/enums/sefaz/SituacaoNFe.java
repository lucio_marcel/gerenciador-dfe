package br.com.zeta.gerenciadordfe.domain.core.enums.sefaz;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public enum SituacaoNFe {
	AUTORIZADA("1"), 
	DENEGADA("2"), 
	CANCELADA("3");

	private final String codigo;
	
	private static final Map<String, SituacaoNFe> BY_CODIGO = new HashMap<>();
    
    static {
        for (SituacaoNFe situacao: values()) {
        	BY_CODIGO.put(situacao.codigo, situacao);
        }
    }
    
	SituacaoNFe(String codigoSItuacao){
		this.codigo = codigoSItuacao;
	}
	
	public static SituacaoNFe valueOfCodigo(String codigoSituacao) {
        return BY_CODIGO.get(codigoSituacao);
    }
}
