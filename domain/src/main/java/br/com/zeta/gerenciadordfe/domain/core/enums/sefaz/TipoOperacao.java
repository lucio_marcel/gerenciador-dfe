package br.com.zeta.gerenciadordfe.domain.core.enums.sefaz;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public enum TipoOperacao {
	ENTRADA("0"), 
	SAIDA("1");

	private final String codigo;
	
	private static final Map<String, TipoOperacao> BY_CODIGO = new HashMap<>();
    
    static {
        for (TipoOperacao tipoOperacao: values()) {
        	BY_CODIGO.put(tipoOperacao.codigo, tipoOperacao);
        }
    }
    
	TipoOperacao(String codigoTipo){
		this.codigo = codigoTipo;
	}
	
	public static TipoOperacao valueOfCodigo(String codigoTipo) {
        return BY_CODIGO.get(codigoTipo);
    }
}
