package br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz;

import br.com.zeta.gerenciadordfe.domain.exception.DomainException;

public class SefazException extends DomainException {

	private static final long serialVersionUID = 1L;

	public SefazException(String mensagem) {
		super(mensagem);
	}
	
	public  SefazException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
}
