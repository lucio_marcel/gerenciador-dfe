package br.com.zeta.gerenciadordfe.domain.repository.specification;

import java.util.ArrayList;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;

import br.com.zeta.gerenciadordfe.domain.filter.ResumoNFeFilter;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;

public class ResumoNFeSpecifications {
	
	public static Specification<ResumoNFe> usandoFiltro(ResumoNFeFilter filtro){
		return (root, query, builder) -> {
			
			if (ResumoNFe.class.equals(query.getResultType()))
			{
				root.fetch("empresa");
			}
			
			var predicates = new ArrayList<Predicate>();
			
			if (filtro.getEmpresaCodigo() != null) {
				predicates.add(builder.equal(root.get("empresa").get("codigo"), filtro.getEmpresaCodigo()));
			}
			
			if (filtro.getNfeManifestada() != null) {
				predicates.add(builder.equal(root.get("nfeManifestada"), filtro.getNfeManifestada()));
			}
			
			if (filtro.getNfeImportada() != null) {
				predicates.add(builder.equal(root.get("nfeImportada"), filtro.getNfeImportada()));
			}
			
			if (filtro.getSituacaoNfe() != null) {
				predicates.add(builder.equal(root.get("situacaoNfe"), filtro.getSituacaoNfe()));
			}
			
			if (filtro.getDataEmissaoInicial() != null) {
				predicates.add(builder.greaterThanOrEqualTo(root.get("dataEmissao"), filtro.getDataEmissaoInicial()));
			}
			
			if (filtro.getDataEmissaoFinal() != null) {
				predicates.add(builder.lessThanOrEqualTo(root.get("dataEmissao"), filtro.getDataEmissaoFinal()));
			}
			
			if (filtro.getDataAutorizacaoInicial() != null) {
				predicates.add(builder.greaterThanOrEqualTo(root.get("dataAutorizacao"), filtro.getDataAutorizacaoInicial()));
			}
			
			if (filtro.getDataAutorizacaoFinal() != null) {
				predicates.add(builder.lessThanOrEqualTo(root.get("dataAutorizacao"), filtro.getDataAutorizacaoFinal()));
			}
			
			return builder.and(predicates.toArray(new Predicate[0]));
		};
	}

}
