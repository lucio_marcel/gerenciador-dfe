package br.com.zeta.gerenciadordfe.domain.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import br.com.zeta.gerenciadordfe.domain.event.InativacaoEmpresaEvent;
import br.com.zeta.gerenciadordfe.domain.service.dfe.GerenciadorDfeService;

@Component
public class InativacaoEmpresaListener {

	@Autowired
	private GerenciadorDfeService gerenciadorDfeService;
	
	@TransactionalEventListener
	public void aoInativarEmpresa(InativacaoEmpresaEvent event) {
		gerenciadorDfeService.desagendarConsultaDFe(event.getEmpresa());
	}
}
