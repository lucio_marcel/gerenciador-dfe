package br.com.zeta.gerenciadordfe.domain.model.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Emitente {
	
	@Column(name = "emitente_cnpj")
	private String CNPJ;
	
	@Column(name = "emitente_cpf")
	private String CPF;
	
	@Column(name = "emitente_nome")
	private String nome;
	
	@Column(name = "emitente_inscricao_estadual")
	private String inscricaoEstadual;

}
