package br.com.zeta.gerenciadordfe.domain.exception.certificate;

import java.util.Date;

public class CertificadoExpiradoException extends CertificadoException {

	private static final long serialVersionUID = 1L;

	public CertificadoExpiradoException(String message) {
		super(message);
	}

	public CertificadoExpiradoException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CertificadoExpiradoException(String alias, Date dataExpiracao) {
		this(String.format("O certificado digital '%s' expirou em %t.", alias, dataExpiracao));
	}

	public CertificadoExpiradoException(Throwable cause) {
	        super(cause);
	}
}
