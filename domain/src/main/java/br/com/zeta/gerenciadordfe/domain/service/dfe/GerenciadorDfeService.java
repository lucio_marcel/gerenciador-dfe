package br.com.zeta.gerenciadordfe.domain.service.dfe;

import java.util.concurrent.CompletableFuture;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;

public interface GerenciadorDfeService {
	
	CompletableFuture<Void> agendarConsultasDFe();
	
	void agendarConsultaDFe(Empresa empresa);
	
	void desagendarConsultaDFe(Empresa empresa);
	
	void consultarDFe(Empresa empresa);
	
}
