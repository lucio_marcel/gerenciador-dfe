package br.com.zeta.gerenciadordfe.domain.core.enums.sefaz;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public enum StatusResposta {
	ERRO("-1"),
	NAO_EXECUTADA("0"),
	LOTE_DE_EVENTO_PROCESSADO("128"),
	EVENTO_REGISTRADO_E_VINCULADO_NFE("135"),
	EVENTO_REGISTRADO_E_NAO_VINCULADO_NFE("136"),
	DOCUMENTOS_NAO_LOCALIZADOS("137"),
	DOCUMENTOS_LOCALIZADOS("138"),
	CONSUMO_INDEVIDO("656");
	
	private final String codigo;
	
	private static final Map<String, StatusResposta> BY_CODIGO = new HashMap<>();
	
	static {
        for (StatusResposta status: values()) {
        	BY_CODIGO.put(status.codigo, status);
        }
    }
	
	StatusResposta(String codigo){
		this.codigo = codigo;
	}
	
	public static StatusResposta valueOfCodigo(String codigoStatusResposta) {
        if ((codigoStatusResposta == null) || BY_CODIGO.get(codigoStatusResposta) == null) {
        	return StatusResposta.ERRO;
        } else {
        	return BY_CODIGO.get(codigoStatusResposta);
        }
    }
	
}
