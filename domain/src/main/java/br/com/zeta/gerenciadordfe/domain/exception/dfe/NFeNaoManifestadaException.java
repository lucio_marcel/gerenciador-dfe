package br.com.zeta.gerenciadordfe.domain.exception.dfe;

import br.com.zeta.gerenciadordfe.domain.exception.DomainException;

public class NFeNaoManifestadaException extends DomainException {

	private static final long serialVersionUID = 1L;

	public NFeNaoManifestadaException(String chaveAcesso) {
		super(String.format("A NFe com chave de acesso %s ainda não foi manifestada.", chaveAcesso));
	}

}
