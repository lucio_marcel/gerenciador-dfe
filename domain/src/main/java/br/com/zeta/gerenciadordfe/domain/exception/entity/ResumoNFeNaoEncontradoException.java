package br.com.zeta.gerenciadordfe.domain.exception.entity;

public class ResumoNFeNaoEncontradoException extends EntityNotFoundException {

	private static final long serialVersionUID = 1L;

	public ResumoNFeNaoEncontradoException(String message) {
		super(message);
	}

	public ResumoNFeNaoEncontradoException(Long empresaCodigo, String chaveAcesso) {
		this(String.format("Não existe resumo de NFe cadastrado para a empresa %d com a chave de acesso: %s", empresaCodigo, chaveAcesso));
	}
	
	public ResumoNFeNaoEncontradoException(Long resumoNFeId) {
		this(String.format("Não existe resumo de NFe cadastrado com o código: %d", resumoNFeId));
	}
}
