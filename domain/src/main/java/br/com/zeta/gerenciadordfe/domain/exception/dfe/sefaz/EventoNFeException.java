package br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz;

public class EventoNFeException  extends SefazException {

	private static final long serialVersionUID = 1L;
	
	public EventoNFeException(String message) {
		super(message);
	}
	
	public EventoNFeException(String evento, String status, String motivo) {
		this(String.format("Evento: %s na SEFAZ. Código do Erro: %s. Motivo: %s", evento, status, motivo));
	}
	
}
