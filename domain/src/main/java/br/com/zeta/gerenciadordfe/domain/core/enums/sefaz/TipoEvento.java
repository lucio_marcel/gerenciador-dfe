package br.com.zeta.gerenciadordfe.domain.core.enums.sefaz;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public enum TipoEvento {
	CONFIRMACAO_DA_OPERACAO("210200","Confirmacao da Operacao"),
	CIENCIA_DA_EMISSAO("210210","Ciencia da Operacao"),
	DESCONHECIMENTO_DA_OPERACAO("210220","Desconhecimento da Operacao"),
	OPERACAO_NAO_REALIZADA("210240","Operacao nao Realizada");
	
	private final String codigo;
	private final String descricao;
	
	private static final Map<String, TipoEvento> BY_CODIGO = new HashMap<>();
    
    static {
        for (TipoEvento tipoEvento: values()) {
        	BY_CODIGO.put(tipoEvento.codigo, tipoEvento);
        }
    }
	
	TipoEvento(String codigo, String descricao){
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public static TipoEvento valueOfCodigo(String codigoEvento) {
        return BY_CODIGO.get(codigoEvento);
    }

	
}
