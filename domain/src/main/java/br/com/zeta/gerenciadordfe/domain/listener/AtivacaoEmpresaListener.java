package br.com.zeta.gerenciadordfe.domain.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import br.com.zeta.gerenciadordfe.domain.event.AtivacaoEmpresaEvent;
import br.com.zeta.gerenciadordfe.domain.service.dfe.GerenciadorDfeService;

@Component
public class AtivacaoEmpresaListener {

	@Autowired
	private GerenciadorDfeService gerenciadorDfeService;
	
	@TransactionalEventListener
	public void aoAtivarEmpresa(AtivacaoEmpresaEvent event) {
		gerenciadorDfeService.agendarConsultaDFe(event.getEmpresa());
	}
}
