package br.com.zeta.gerenciadordfe.domain.exception.certificate;

public class CertificadoStorageException extends CertificadoException{

	private static final long serialVersionUID = 1L;

	public CertificadoStorageException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertificadoStorageException(String message) {
		super(message);
	}
}
