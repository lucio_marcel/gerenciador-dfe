package br.com.zeta.gerenciadordfe.domain.exception;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;

public class EntityDataIntegrityViolationException extends DomainException {

	private static final long serialVersionUID = 1L;
	
	public EntityDataIntegrityViolationException(DataIntegrityViolationException e) {
		super(getMessage(e));
	}
	
	private static String getMessage(DataIntegrityViolationException ex) {
		if (ex.getCause() instanceof ConstraintViolationException) {
			return "Já existe cadastro com os daods informados.";
		}
		
		return "Violação de intergidade de dados.";
	}
}