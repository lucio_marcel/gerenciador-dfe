package br.com.zeta.gerenciadordfe.domain.service;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoConsulta;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Historico;

public interface HistoricoService {
		
	Historico adicionar(Historico historico);

	Long contarConsultasUltimaHoraPorTipo(TipoConsulta tipoConsulta, Long empresaId);
	
	
}
