package br.com.zeta.gerenciadordfe.domain.exception;

public class DomainException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public DomainException(String mensagem) {
		super(mensagem);
	}
	
	public  DomainException(String mensagem, Throwable causa) {
		super(mensagem, causa);
	}
}
