package br.com.zeta.gerenciadordfe.domain.model.entity;

import java.io.InputStream;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CertificadoFileProperties {

	private TipoCertificado tipoCertificado;
	private String originalFilename;
	private String password;
	private InputStream fileInputStream;
	
}
