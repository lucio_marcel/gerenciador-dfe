package br.com.zeta.gerenciadordfe.domain.service;

import java.util.List;

import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;
import br.com.zeta.gerenciadordfe.domain.model.entity.CertificadoFileProperties;

public interface EmpresaService {
		
	Empresa adicionar(Empresa empresa);
	
	Empresa atualizar(Empresa empresa);
	
	Empresa buscarPorId(Long id);
	
	Empresa buscarPorCodigo(Long codigo);
	
	Empresa refresh(Empresa empresa);
	
	Certificado buscarCertificado(Long empresaCodigo);
	
	Certificado atualizarCertificado(Long empresaCodigo, CertificadoFileProperties certificadoFileProperties) throws CertificadoException ;
	
	void excluirCertificado(Long empresaCodigo);
	
	List<Empresa> buscarTodas();
	
	List<Empresa> buscarTodasAtivas();
	
	void ativar(Long codigo);
	
	void inativar(Long codigo);
	
}
