package br.com.zeta.gerenciadordfe.domain.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoAmbiente;
import br.com.zeta.gerenciadordfe.domain.core.enums.TipoEmissao;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("zeta")
public class ConfigProperties{
	
	private String cacertsFileLocation;
	private String certificadoFileLocation;
	private TipoAmbiente tipoAmbiente = TipoAmbiente.PRODUCAO;
	private TipoEmissao tipoEmissao = TipoEmissao.NORMAL;
	private ConsultaDocumentos consultaDocumentos = new ConsultaDocumentos();
	
	@Getter
	@Setter
	public class ConsultaDocumentos {
		private Long intervaloTempoConsumoIndevido;
		private Long intervaloTempoDocumentosNaoLocalizados;
		private Long intervaloTempoDocumentosLocalizados;
		private Long intervaloTempoErroNaConsulta;
	}
	 
}
