package br.com.zeta.gerenciadordfe.domain.repository;

import java.util.List;

import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;

public interface ResumoNFeRepositoryQueries {
	
	List<ResumoNFe> findAllManifestacaoPendente(Long empresaCodigo);

}
