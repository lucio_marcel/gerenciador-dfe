package br.com.zeta.gerenciadordfe.domain.core.enums.sefaz;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

@Getter
public enum UF {
	AMBIENTE_NACIONAL("91"),
	AC("12"),
	AL("27"),
	AM("13"),
	AP("16"),
	BA("29"),
	CE("23"),
	DF("53"),
	ES("32"),
	GO("52"),
	MA("21"),
	MG("31"),
	MT("51"),
	MS("50"),
	PA("15"),
	PB("25"),
	PE("26"),
	PI("22"),
	PR("41"),
	RJ("33"),
	RN("24"),
	RO("11"),
	RR("14"),
	RS("43"),
	SC("42"),
	SE("28"),
	SP("35"),
	TI("17");
	
	private final String codigo;
	
	private static final Map<String, UF> BY_CODIGO = new HashMap<>();
    
    static {
        for (UF uf: values()) {
        	BY_CODIGO.put(uf.codigo, uf);
        }
    }
	
	UF(String codigo){
		this.codigo = codigo;
	}

	public static UF parseByCodigoUF(String codigo) {
        return BY_CODIGO.get(codigo);
    }
	
	public static UF parseByCodigoMunicipio(String codigo) {
        return parseByCodigoUF(codigo.substring(0,2));
    }
	
	
}
