package br.com.zeta.gerenciadordfe.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.zeta.gerenciadordfe.domain.core.enums.StatusEmpresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long>{

	List<Empresa> findByStatusEmpresa(StatusEmpresa statusEmpresa);
	Optional<Empresa> findByCodigo(Long codigo);
	
}
