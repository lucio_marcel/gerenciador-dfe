package br.com.zeta.gerenciadordfe.domain.model.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado;
import lombok.Data;

@Data
@Embeddable
public class Certificado {
	
	private static final String TSLv1_2 = "TLSv1.2";

	@Column(name = "certificado_file_path")
	private String filePath;
	
	@Column(name = "certificado_alias")
	private String alias;
	
	@Column(name = "certificado_passowrd")
	private String password;
	
	@Column(name = "certificado_tipo")
	@Enumerated(EnumType.STRING)
	private TipoCertificado tipoCertificado;

	@Column(name = "certificado_cnpj_cpf")
	private String cnpjCpf;
	
	@Column(name = "certificado_vencimento")
	private LocalDate vencimento;
	
	@Column(name = "certificado_data_hora_vencimento")
	private LocalDateTime dataHoraVencimento;

	@Column(name = "certificado_ssl_protocol")
	private String sslProtocol;
	 
	public Certificado() {
		this.setSslProtocol(TSLv1_2);
	}

	public Long getDiasRestantes(){
		if (this.getVencimento() != null) {
			return LocalDate.now().until(this.getVencimento(), ChronoUnit.DAYS);
		} else {
			return null;
		}
	}
	
	public Boolean expirado() {
		if (this.getVencimento() != null) {
			System.out.println(LocalDate.now());
			System.out.println(this.getVencimento());
			return !LocalDate.now().isBefore(this.getVencimento());
		}
		return true;
	}
	
}
