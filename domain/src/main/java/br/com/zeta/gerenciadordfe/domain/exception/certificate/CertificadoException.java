package br.com.zeta.gerenciadordfe.domain.exception.certificate;

import lombok.Getter;

@Getter
public class CertificadoException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CertificadoException(String message) {
		super(message);
	}

	public CertificadoException(String message, Throwable cause) {
		super(message, cause);
	}

	public CertificadoException(Throwable cause) {
		super(cause);
	}

}
