package br.com.zeta.gerenciadordfe.service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.core.configuration.ConfigProperties;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.entity.task.ConsultaDFeTask;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;
import br.com.zeta.gerenciadordfe.domain.service.ScheduleTaskService;
import br.com.zeta.gerenciadordfe.domain.service.dfe.GerenciadorDfeService;
import br.com.zeta.gerenciadordfe.domain.service.distdfe.DistribuicaoDfeService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GerenciadorDfeServiceImpl implements GerenciadorDfeService {

	@Autowired private DistribuicaoDfeService nfeDistDfeInteresseService;
	@Autowired private EmpresaService empresaService;
	@Autowired private ScheduleTaskService schedule;
	@Autowired private ConfigProperties configProperties;

	//@Async
	@Override
	public CompletableFuture<Void> agendarConsultasDFe() {
		List<Empresa> empresas = empresaService.buscarTodasAtivas();

		log.info("Iniciando o Agendamento");
		
		for (Empresa empresa : empresas) {
			agendarConsultaDFe(empresa);
		}
		
		log.info("Finalizando o Agendamento");
		
		return null;
	}

	@Override
	public void agendarConsultaDFe(Empresa empresa) {
		
		
		if (empresa.estaAtiva()) {
			ConsultaDFeTask task = ConsultaDFeTask.builder()
					.empresa(empresa)
					.gerenciadorDfeService(this)
					.build();

			OffsetDateTime taskStartTime;

			switch (empresa.getStatusUltimaConsulta()) {
			case DOCUMENTOS_LOCALIZADOS:
				taskStartTime = empresa.getDataUltimaConsulta().plusMinutes(
						configProperties.getConsultaDocumentos().getIntervaloTempoDocumentosLocalizados());
				break;
				
			case DOCUMENTOS_NAO_LOCALIZADOS:
				taskStartTime = empresa.getDataUltimaConsulta().plusMinutes(
						configProperties.getConsultaDocumentos().getIntervaloTempoDocumentosNaoLocalizados());
				break;

			case CONSUMO_INDEVIDO:
				taskStartTime = empresa.getDataUltimaConsulta()
						.plusMinutes(configProperties.getConsultaDocumentos().getIntervaloTempoConsumoIndevido());
				break;

			case ERRO:
				taskStartTime = empresa.getDataUltimaConsulta()
					.plusMinutes(configProperties.getConsultaDocumentos().getIntervaloTempoErroNaConsulta());
				break;
				
			case NAO_EXECUTADA:
				taskStartTime = OffsetDateTime.now();
				break;
				
			default:
				taskStartTime = OffsetDateTime.now();
				break;

			}

			log.info("Agendando a empresa: " + empresa.getCodigo() + " - Hora da próxima tarefa: " + taskStartTime);
			schedule.addTaskToScheduler(empresa.getId(), task, taskStartTime);
		}
	}

	@Override
	public void desagendarConsultaDFe(Empresa empresa) {
		schedule.removeTaskFromScheduler(empresa.getId());
	}

	@Override
	public void consultarDFe(Empresa empresa) {
		log.info("Iniciando a consulta da empresa: " + empresa.getCodigo() + " - CNPJ: " + empresa.getCnpj() );
		nfeDistDfeInteresseService.consultarPorUltimoNSU(empresa);
	}

}
