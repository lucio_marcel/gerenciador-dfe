package br.com.zeta.gerenciadordfe.service.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento;
import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;
import br.com.zeta.gerenciadordfe.domain.service.storage.XmlStorageService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class XmlStorageServiceImpl implements XmlStorageService {
	
	@Override
	public void salvar(TipoDocumento tipoDocumento, String nomeArquivo, String xml, String cnpj) {
		try {
		    OutputStream outputStream = getOutputStream(tipoDocumento, nomeArquivo, cnpj);
		    byte[] strToBytes = xml.getBytes();
		    outputStream.write(strToBytes);
		    outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		}

	}
	
	@Override
	public void salvar(TipoDocumento tipoDocumento, String nomeArquivo, WebServiceMessage webServiceMessage, String cnpj) {
		try {
			OutputStream outputStream = getOutputStream(tipoDocumento, nomeArquivo, cnpj);
			webServiceMessage.writeTo(outputStream);
		} catch (IOException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
		}
	}	
	
	private OutputStream getOutputStream(TipoDocumento tipoDocumento, String nomeArquivo, String cnpj) {
		try {
			String localFolder = tipoDocumento.getLocalXmlFolder(tipoDocumento, cnpj);
			
			String fileName = new StringBuilder()
					.append(localFolder)
					.append(nomeArquivo)
					.append(tipoDocumento.getXmlFileExtension(tipoDocumento)).toString();
			
			if (!localFolder.isEmpty() && !fileName.isEmpty()) {
				new File(localFolder).mkdirs();
			}
				
			return new FileOutputStream(fileName);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e);
			
		}
		
	}
	
}
