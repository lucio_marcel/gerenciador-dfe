package br.com.zeta.gerenciadordfe.service.core.http;

import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF.BA;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF.CE;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF.PI;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF.SP;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.isNullOrEmpty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants;
import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;

@Component
public class WebServiceRequester {
	public String request(WsRequestParameters wsRequestParameters) throws Exception {

		try {
			// Carga da chave privada
			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

			FileInputStream fileInputStream = new FileInputStream(new File(wsRequestParameters.getCertificateFilePath()));

			keyStore.load(fileInputStream, wsRequestParameters.getCertificatePassowrd().toCharArray());

			fileInputStream.close();

			URL url = new URL(wsRequestParameters.getWebserviceUrl());

			// Com isso não será cheaco se o certificado do site é válido ou não.
			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			} };

			// Criação do gerenciador de chave que vai ser chamado mais abaixo
			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(keyStore, wsRequestParameters.getCertificatePassowrd().toCharArray());

			// Criaação do contexto da requisição.
			// Onde será ignorado qualquer erro do certificado da URL (CA não reconhecida
			// pelo java por exemplo)
			// e também adicionada a chave privada no contexto, necesária para consumir o ws
			SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(kmf.getKeyManagers(), trustAllCerts, new SecureRandom());

			// define que vc vai usar o contexto em HttpsURLConnection
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
 
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-type", "application/soap+xml; charset=utf-8");

			if (!(wsRequestParameters.getUf() == null)
					&& (wsRequestParameters.getUf() == SP || wsRequestParameters.getUf() == BA
							|| wsRequestParameters.getUf() == PI || (wsRequestParameters.getUf() == CE
									&& wsRequestParameters.getService().contains("nfeRecepcaoEvento")))) {

			} else {
				if (!isNullOrEmpty(wsRequestParameters.getSoapAction())) {
					connection.setRequestProperty("SOAPAction", wsRequestParameters.getSoapAction());

				} else {
					if (!isNullOrEmpty(wsRequestParameters.getService())) {
						if (SefazConstants.SERVICE_DISTRIBUICAO_NFE.equals(wsRequestParameters.getService())) {
							connection.setRequestProperty("Content-Type", "text/xml");
							connection.setRequestProperty("Accept-Encoding", "gzip");

						} else {
							connection.setRequestProperty("SOAPAction", url + "" + wsRequestParameters.getService());

						}
					}
				}
			}

			OutputStream outputStream = connection.getOutputStream();

			outputStream.write(wsRequestParameters.getSoapMessage().getBytes());
			outputStream.flush();
			outputStream.close();

			BufferedReader bufferReader;

			if (connection.getResponseCode() < HttpStatus.BAD_REQUEST.value()) {
				bufferReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			} else {
				bufferReader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
			}

			String inputLine;

			StringBuilder response = new StringBuilder();

			while ((inputLine = bufferReader.readLine()) != null) {
				response.append(inputLine);
			}

			bufferReader.close();

			connection.disconnect();

			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException(e.getMessage(), e.getCause());

		}
	}
}