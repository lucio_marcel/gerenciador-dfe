package br.com.zeta.gerenciadordfe.service.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.datatype.XMLGregorianCalendar;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtils {

	static final String CLARION_DATE = "1800-12-28";
	public static final String DEFAULT_PATTERN = "yyyy-MM-dd";
	static final String HOUR_UTC_FORMAT = "HH:mm:ssXXX";
	static final String UTC_TIMEZONE = "UTC";
	
	static final Long DAY_MILLISECONDS = 86400000L; // 1000 * 60 * 60 * 24
	
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	
	public static final String ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
	public static final String ISO_FORMAT_WITHOUT_GMT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String ISO_FORMAT_GMT3 = "yyyy-MM-dd'T'HH:mm:ss-03:00";
	public static final String ISO_FORMAT_GMT4 = "yyyy-MM-dd'T'HH:mm:ss-04:00";

	public static Date startDate = null;
	public static Date endDate = null;
	
	public static Date getClarionDate() {
		
		SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_PATTERN);
		Date clarionDate = null;
		try {
			clarionDate = sdf.parse(CLARION_DATE);
		} catch(Exception e) {
			log.error(e.getMessage());
			return null;
		}
		return clarionDate;
	}
	
	public static Date longToDate(Long source) {
		
		Date returnValue = null;
		long baseTime = getClarionDate().getTime();
		long sourceTime = source * DAY_MILLISECONDS;
		long finalTime = baseTime + sourceTime;		
		returnValue = new Date(finalTime);		
		return returnValue;
	}
	
	public static Long dateToLong(String source) {
		return dateToLong(source, DEFAULT_PATTERN);
	}

	public static Long dateToLong(String source, String pattern) {	
		
		SimpleDateFormat sdf  = null;		
		try {
			sdf = new SimpleDateFormat(pattern);
			return dateToLong(sdf.parse(source));
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}
	
	@SuppressWarnings("deprecation")
	public static Long dateToLong(Date date) {
		
		Long returnValue = null;
		try {
			Long time = date.getTime() - getClarionDate().getTime();
			Double doubleTime = time / (double) DAY_MILLISECONDS;
			returnValue = new Long(Math.round(doubleTime));
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return returnValue;
	}
	
	public static String longDateFormat(Long source) {
		return dateFormat(longToDate(source), DEFAULT_PATTERN);
	}
	
	public static String longDateFormat(Long source, String pattern) {
		return dateFormat(longToDate(source), pattern);
	}
	
	public static String dateFormat(Date source) {
		return dateFormat(source, DEFAULT_PATTERN);
	}
	
	public static String dateFormat(Date source, String pattern) {
		
		String returnValue = null;
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		returnValue = sdf.format(source);
		return returnValue;
	}

	public static String longToHourTime(Long source) {
		
		Long timeInMillis = source / 100;  
		@SuppressWarnings("unused")
		Long seconds = timeInMillis % 60;  
		timeInMillis = timeInMillis / 60;  
		Long minutes = timeInMillis % 60; 
		Long hours = timeInMillis / 60; 
		
		return String.format("%02d:%02d", hours, minutes);
	}
	

	public static Date toDate(XMLGregorianCalendar calendar){
		if(calendar != null){
			return calendar.toGregorianCalendar().getTime();
		}
		return null;
	}
	
	public static String dateToUTCFormat(Date source) {
		SimpleDateFormat sdf = new SimpleDateFormat(ISO_FORMAT);
		String dataFormatada = sdf.format(source);
		return dataFormatada;
	}

	public static String formatDateUTC(long data) {
		return dateToUTCFormat(longToDate(data));
	}
	
	public static Long dateUTCdeformat(String source){	
		return dateToLong(source.substring(0,10)); 
	}	
	
	@SuppressWarnings("deprecation")
	public static Long hourUTCdeformat(String source){
		
		Long total = null;
        try {
        	SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        	Date date = sdf.parse(source.substring(11,19));           
            total = new Integer(((((date.getHours() * 3600) + (date.getMinutes() * 60) + date.getSeconds())) * 100) + 1).longValue();                      
        } catch (Exception e) {
        	log.error(e.getMessage());
            return null;
        }
        return total;
	}	
	
	public static void setStartDate(Date startDate) {
		DateUtils.startDate = startDate;
	}

	public static void setEndDate(Date endDate) {
		DateUtils.endDate = endDate;
	}

	public static String newDateTimeZone(String parTimeZone, UF uf) {	
		
		String timeZone =  "GMT-3";
		
		switch (uf) {
		case AC:
			timeZone = "GMT-5";
			break;
			
		case RO:
		case AM:
		case RR:
		case MS:
		case MT:
			timeZone = "GMT-4";
			break;
			
		default:
			timeZone = "GMT-3";
			break;
		};
		
		Date currentDate = new Date(); 
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone(parTimeZone));	
		String dataUTC = dateFormat.format(currentDate);
		Date newDateUTC;
		try {
			newDateUTC = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dataUTC);
			SimpleDateFormat format = new SimpleDateFormat(ISO_FORMAT);
			Long date = dateToLong(format.format(newDateUTC), ISO_FORMAT);
			SimpleDateFormat format1 = new SimpleDateFormat(ISO_FORMAT_WITHOUT_GMT);

			SimpleDateFormat sdf = new SimpleDateFormat("XXX");
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			String strSetupGMT = sdf.format(date);
			
			format1.setTimeZone(TimeZone.getTimeZone(timeZone));
			return format1.format(newDateUTC) + strSetupGMT;
			
		} catch (ParseException e) {
        	log.error(e.getMessage());
            return null;
		}
		
	}
}
