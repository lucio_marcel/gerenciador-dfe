package br.com.zeta.gerenciadordfe.service;

import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.ENVIO_MANIFESTACAO_DESTINATARIO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.RETORNO_MANIFESTACAO_DESTINATARIO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta.EVENTO_REGISTRADO_E_NAO_VINCULADO_NFE;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta.EVENTO_REGISTRADO_E_VINCULADO_NFE;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta.LOTE_DE_EVENTO_PROCESSADO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF.AMBIENTE_NACIONAL;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.SERVICE_EVENTO;
import static br.com.zeta.gerenciadordfe.domain.core.utils.WsdlFactory.getWsdlManifestacao;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.objectToXml;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.removeAccents;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.setContextPath;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.xmlToDocument;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.xmlToObjectWithNamespace;

import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.TipoEvento;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF;
import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.ValidacaoEventoException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz.EventoNFeException;
import br.com.zeta.gerenciadordfe.domain.exception.entity.ResumoNFeNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe.EventoManifestacaoDestinatario;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;
import br.com.zeta.gerenciadordfe.domain.service.ResumoNFeService;
import br.com.zeta.gerenciadordfe.domain.service.dfe.ManifestacaoNFeService;
import br.com.zeta.gerenciadordfe.domain.service.storage.XmlStorageService;
import br.com.zeta.gerenciadordfe.service.core.certificate.SefazSigner;
import br.com.zeta.gerenciadordfe.service.core.http.WebServiceRequester;
import br.com.zeta.gerenciadordfe.service.core.http.WsRequestParameters;
import br.com.zeta.gerenciadordfe.service.core.utils.DateUtils;
import br.inf.portalfiscal.nfe.manifestacao.TEnvEvento;
import br.inf.portalfiscal.nfe.manifestacao.TEvento;
import br.inf.portalfiscal.nfe.manifestacao.TEvento.InfEvento;
import br.inf.portalfiscal.nfe.manifestacao.TEvento.InfEvento.DetEvento;
import br.inf.portalfiscal.nfe.manifestacao.TRetEnvEvento;
import br.inf.portalfiscal.nfe.manifestacao.TretEvento;
import br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4.NfeDadosMsg;

@Service
public class ManifestacaoNFeServiceImpl implements ManifestacaoNFeService {
	
	private static final String VERSAO_LEIAUTE= "1.00";
	private static final String VERSAO_LEIAUTE_EVENTO = "1.00";
	private static final String VERSAO_EVENTO = "1.00";
	
	private SefazSigner signer;
	private NfeDadosMsg nfeDadosMsg;
	private Empresa empresa;
	private EventoManifestacaoDestinatario evento = new EventoManifestacaoDestinatario();
	private ResumoNFe resumoNFe;
	
	@Autowired private WebServiceRequester webServiceRequester;
	@Autowired private XmlStorageService xmlStorageService;
	@Autowired private EmpresaService empresaService;
	@Autowired private ResumoNFeService resumoNFeService;
	
	@Override
	public void manifestarNFe(Long empresaCodigo, EventoManifestacaoDestinatario eventoManifestacaoDestinatario)  {
		try {
			this.evento = eventoManifestacaoDestinatario;
			
			validarEvento(eventoManifestacaoDestinatario);
			
			this.empresa = empresaService.buscarPorCodigo(empresaCodigo);
			this.resumoNFe = resumoNFeService.buscarPorChaveAcesso(empresaCodigo, evento.getChaveAcesso());
			this.nfeDadosMsg = new NfeDadosMsg();
			
			this.signer = new SefazSigner(empresa.getCertificado().getFilePath(),
					empresa.getCertificado().getAlias(), empresa.getCertificado().getPassword());
			
			String envelopeSoap = getEnvelopeSoap();
			
			xmlStorageService.salvar(ENVIO_MANIFESTACAO_DESTINATARIO, evento.getChaveAcesso(), envelopeSoap, empresa.getCnpj());
			
			String resultSoap = sendEnvelopeSoap(envelopeSoap);
			
			xmlStorageService.salvar(RETORNO_MANIFESTACAO_DESTINATARIO, evento.getChaveAcesso(), resultSoap, empresa.getCnpj());
			
			processareRetornoRequisicao(resultSoap);
			
		 } catch (ResumoNFeNaoEncontradoException e) {
			throw new ResumoNFeNaoEncontradoException(e.getMessage());

		 } catch (ValidacaoEventoException e) {
			throw new ValidacaoEventoException(e.getMessage());
			
		 } catch (EventoNFeException e) {
			throw new EventoNFeException(e.getMessage());
			
		 } catch(CertificadoException e) {
			 throw new CertificadoException(e.getMessage());
			 
		 } catch(Exception e) {
			 e.printStackTrace();
			 throw new ApplicationException(e.getMessage(), e);

		 }
	}

	@Override
	public void manifestarNFeAutorizadas(Long empresaCodigo) {
		List<ResumoNFe> resumosNfe = resumoNFeService.buscarTodosComManifestacaoPendente(empresaCodigo);
		
		resumosNfe.forEach(resumo -> {

			EventoManifestacaoDestinatario evento = EventoManifestacaoDestinatario.builder()
					.chaveAcesso(resumo.getChaveAcesso()).tipoEvento(TipoEvento.CONFIRMACAO_DA_OPERACAO.name()).build();

			manifestarNFe(empresaCodigo, evento);

		});

	}
	
	private void processareRetornoRequisicao(String resultSoap) {
		TRetEnvEvento tRetEnvEvento = getTRetEnvEvento(resultSoap);

		String statusProcessamento = tRetEnvEvento.getCStat();

		if (statusProcessamento.equals(LOTE_DE_EVENTO_PROCESSADO.getCodigo())) {
			for (TretEvento tRetEvento : tRetEnvEvento.getRetEvento()) {
				String statusEvento = tRetEvento.getInfEvento().getCStat();

				if (statusEvento.equals(EVENTO_REGISTRADO_E_VINCULADO_NFE.getCodigo()) 
						|| statusEvento.equals(EVENTO_REGISTRADO_E_NAO_VINCULADO_NFE.getCodigo())) {

					this.resumoNFe.setDataManifestacao(OffsetDateTime.parse(tRetEvento.getInfEvento().getDhRegEvento()));
					this.resumoNFe.setNfeManifestada(true);

					resumoNFe.manifestacaoRealizada();
					resumoNFeService.salvar(this.resumoNFe);

				} else {
					throw new EventoNFeException(tRetEvento.getInfEvento().getXEvento(), statusEvento, tRetEvento.getInfEvento().getXMotivo());
					
				}
				
			}

		} else {
			throw new EventoNFeException("Manifestação do Destinatário", statusProcessamento, tRetEnvEvento.getXMotivo());
		}

	}

	private TRetEnvEvento getTRetEnvEvento(String resultSoap) {
		try {
			Transformer trans = TransformerFactory.newInstance().newTransformer();  
			StringWriter swRetEnvEvento = new StringWriter();
			
			trans.transform(new DOMSource(xmlToDocument(resultSoap).getElementsByTagName("retEnvEvento").item(0)),
					new StreamResult(swRetEnvEvento));

			TRetEnvEvento tRetEnvEvento = (TRetEnvEvento) xmlToObjectWithNamespace(
					swRetEnvEvento.toString().replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\"\\?>", "")
							.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"\\?>", ""),
					TRetEnvEvento.class, "http://www.portalfiscal.inf.br/nfe");
			return tRetEnvEvento;

		} catch (TransformerException | TransformerFactoryConfigurationError  e) {
			throw new ApplicationException(e.getMessage(), e);

		} catch (Exception e) {
			throw new ApplicationException(e.getMessage(), e);
		}
		
	}

	private String getEnvelopeSoap() throws Exception  {

		createNfeDadosMsg();

		//setContextPath(CONTEXTPATH_EVENTO_v4);
		setContextPath("br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4:br.inf.portalfiscal.nfe.manifestacao");

		String xml = removeAccents(objectToXml(this.nfeDadosMsg))
				.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\"\\?>", "")
				.replaceAll("<ns2:", "<")
				.replaceAll("</ns2:", "</")
				.replaceAll(":ns2", "")
				.replaceAll(":ns3", "")
				.replaceAll(":ns4", "")
				.replaceAll("xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/RecepcaoEvento\"", "")
				.replaceAll("xmlns=\"http://www.w3.org/2000/09/xmldsig#\"", "")
				.replaceAll("xmlns=\"http://www.portalfiscal.inf.br/nfe\"", "")
				//.replaceAll("<nfeDadosMsg   >", "")
				.replace("<nfeDadosMsg xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4\"  >", "")
				.replaceAll("</nfeDadosMsg>", "")
				.replaceAll("<envEvento versao=\"1.00\">",
						"<envEvento xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"1.00\">");

		xml = this.signer.signEvento(xml, this.empresa.getCertificado().getAlias(),
				this.empresa.getCertificado().getPassword());

		xml = xml.replaceAll("<envEvento versao=\"1.00\">",
				"<envEvento xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"1.00\">");
		xml = "<nfeDadosMsg>" + xml + "</nfeDadosMsg>";

		StringBuilder envelopeSOAP = new StringBuilder().append("<soap:Envelope")
				.append(" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\"")
				.append(" xmlns=\"http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4\">")
				.append("<soap:Header/>").append(String.format("<soap:Body>%s</soap:Body>", xml))
				.append("</soap:Envelope>");

		return envelopeSOAP.toString();


	}
	
	private String sendEnvelopeSoap(String envelopeSOAP) throws Exception {
		
		WsRequestParameters wsRequestParameters = WsRequestParameters.builder()
				.service(SERVICE_EVENTO)
				.certificateFilePath(empresa.getCertificado().getFilePath())
				.certificatePassowrd(empresa.getCertificado().getPassword())
				.soapAction(null)
				.soapMessage(envelopeSOAP)
				.uf(UF.parseByCodigoMunicipio(empresa.getCodigoMuncipio()))
				.webserviceUrl(getWsdlManifestacao(empresa.getTipoAmbiente()))
				.build();
		
		return webServiceRequester.request(wsRequestParameters);
		
	}
			
	private void createNfeDadosMsg() {

		TEnvEvento tEnvEvento = new TEnvEvento();
		tEnvEvento.setIdLote("1");
		tEnvEvento.setVersao(VERSAO_LEIAUTE);

		TEvento tEvento = new TEvento();
		tEvento.setInfEvento(createInfEvento());
		tEvento.setVersao(VERSAO_LEIAUTE_EVENTO);
		
		tEnvEvento.getEvento().add(tEvento);

		nfeDadosMsg.getContent().add(tEnvEvento);
		
	}

	private InfEvento createInfEvento() {
		InfEvento infEvento = new InfEvento();
		
		infEvento.setNSeqEvento("1");
		infEvento.setCOrgao(AMBIENTE_NACIONAL.getCodigo());
		infEvento.setVerEvento(VERSAO_EVENTO);
		infEvento.setTpEvento(evento.getTipoEvento());
		infEvento.setTpAmb(this.empresa.getTipoAmbiente().getCodigo());
		infEvento.setCNPJ(this.empresa.getCnpj());
		infEvento.setChNFe(this.evento.getChaveAcesso());
		infEvento.setDhEvento(
				DateUtils.newDateTimeZone("GMT", UF.parseByCodigoMunicipio(this.empresa.getCodigoMuncipio())));
		infEvento.setId(String.format("ID%s%s%02d", infEvento.getTpEvento(), infEvento.getChNFe(),
				Integer.parseInt(infEvento.getNSeqEvento())));

		infEvento.setDetEvento(createDetEvento());

		return infEvento;
	}
	
	private  DetEvento createDetEvento() {
		DetEvento detEvento = new DetEvento();

		TipoEvento tipoevento = TipoEvento.valueOfCodigo(evento.getTipoEvento());

		detEvento.setVersao(VERSAO_EVENTO);
		detEvento.setDescEvento(tipoevento.getDescricao());

		if (tipoevento == TipoEvento.OPERACAO_NAO_REALIZADA) {
			detEvento.setXJust(evento.getJustificativa());
		}

		return detEvento;
	}
	
	private void validarEvento(EventoManifestacaoDestinatario evento) throws ValidacaoEventoException{
		TipoEvento tipoevento = TipoEvento.valueOfCodigo(evento.getTipoEvento());
		
		if (tipoevento == null) {
			throw new ValidacaoEventoException(String.format("O tipo de evento '%s' não corrsponde a um tipo de evento de manifestação váido.", evento.getTipoEvento()));
		}
		
		if (tipoevento == TipoEvento.OPERACAO_NAO_REALIZADA){
			if (evento.getJustificativa() == null || evento.getJustificativa().isEmpty()) {
				throw new ValidacaoEventoException(String.format("O Evento '%s' requer uma justificativa.",tipoevento.name()));	
			}
		}
	}
	
	}
