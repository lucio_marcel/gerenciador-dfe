package br.com.zeta.gerenciadordfe.service.storage;

import java.nio.file.Files;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import br.com.zeta.gerenciadordfe.domain.core.configuration.CertificadoStorageProperties;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoStorageException;
import br.com.zeta.gerenciadordfe.domain.service.storage.CertificadoStorageService;

@Service
public class CertificadoStorageServiceImpl implements CertificadoStorageService{

	@Autowired
	private CertificadoStorageProperties certificadoStorageProperties;
	
	@Override
	public Path armazenar(NovoCertificado novoCertificado) {
		Path arquivoPath = getArquivoPath(gerarNomeArquivo(novoCertificado.getNomeArquivo()));
		
		try {
			FileCopyUtils.copy(novoCertificado.getInputStream(), Files.newOutputStream(arquivoPath));
		} catch (Exception e) {
			throw new CertificadoStorageException(String.format("Não foi possível armazenar o arquivo: %s", arquivoPath.toString()), e);
		}
		
		return arquivoPath;

	}

	@Override
	public void remover(String nomeArquivo) {
		Path arquivoPath = getArquivoPath(nomeArquivo);
		
		try {
			Files.deleteIfExists(arquivoPath);
		} catch (Exception e) {
			throw new CertificadoStorageException(String.format("Não foi possível excluir o arquivo: %s", arquivoPath.toString()), e);
		}
		
	}
	
	private Path getArquivoPath(String nomeArquivo) {
		return certificadoStorageProperties.getCertificadoStorageLocalFolder()
				.resolve(Path.of(nomeArquivo));
	}

}
