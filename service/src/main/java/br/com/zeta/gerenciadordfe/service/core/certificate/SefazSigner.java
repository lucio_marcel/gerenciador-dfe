package br.com.zeta.gerenciadordfe.service.core.certificate;

import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.ID;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.NFE;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.TAG_CANC;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.TAG_ENVIO;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.TAG_EVENTO;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.TAG_INFEVE;
import static br.com.zeta.gerenciadordfe.domain.core.utils.SefazConstants.TAG_INUT;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;

public class SefazSigner extends DefaultSigner {

	private PrivateKey privateKey;
    private KeyInfo keyInfo;
 
    public SefazSigner(String certificateFilePath, String alias, String password) {
		super(certificateFilePath, alias, password);
	} 
    
	public String signCanc(String xml, String alias, String password) throws Exception {
        return this.signSefaz(xml, alias, password, TAG_CANC);
    }

    public String signInut(String xml, String alias, String password) throws Exception {
        return this.signSefaz(xml, alias, password, TAG_INUT);
    }
    
    public String signEnvio(String xml, String alias, String password) throws Exception {
        return this.signSefaz(xml, alias, password, TAG_ENVIO);
    }

    public String signEnviNFe(String xml, String alias, String password) throws Exception {

    	Document document = documentFactory(xml);
		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
        ArrayList<Transform> transformList = signatureFactory(signatureFactory);

        for (int i = 0; i < document.getDocumentElement().getElementsByTagName(NFE).getLength(); i++) {
            NodeList elements = document.getElementsByTagName(TAG_ENVIO);
            org.w3c.dom.Element el = (org.w3c.dom.Element) elements.item(i);
            el.setIdAttribute(ID, true);
            String id = el.getAttribute(ID);
            Reference ref = signatureFactory.newReference("#" + id, signatureFactory.newDigestMethod(DigestMethod.SHA1, null), 
            		transformList, null, null);
            SignedInfo si = signatureFactory.newSignedInfo(signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE, 
            		(C14NMethodParameterSpec) null), signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));
            keyInfo = getKeyInfo();
            XMLSignature signature = signatureFactory.newXMLSignature(si, keyInfo);
            privateKey = getPrivateKey();
            DOMSignContext dsc = new DOMSignContext(privateKey, document.getDocumentElement().getElementsByTagName(NFE).item(i));
            signature.sign(dsc);
        }

        return outputXML(document);
    }
    
    public String signEvento(String xml, String alias, String password) throws Exception {
		try {
			Document document = documentFactory(xml);
			XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
			ArrayList<Transform> transformList = signatureFactory(signatureFactory);

			for (int i = 0; i < document.getDocumentElement().getElementsByTagName(TAG_EVENTO).getLength(); i++) {
				NodeList elements = document.getElementsByTagName(TAG_INFEVE);
				org.w3c.dom.Element el = (org.w3c.dom.Element) elements.item(i);
				el.setIdAttribute(ID, true);
				String id = el.getAttribute(ID);
				Reference ref = signatureFactory.newReference("#" + id,
						signatureFactory.newDigestMethod(DigestMethod.SHA1, null), transformList, null, null);
				SignedInfo si = signatureFactory.newSignedInfo(
						signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,
								(C14NMethodParameterSpec) null),
						signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
						Collections.singletonList(ref));
				keyInfo = getKeyInfo();
				XMLSignature signature = signatureFactory.newXMLSignature(si, keyInfo);
				privateKey = getPrivateKey();
				DOMSignContext dsc = new DOMSignContext(privateKey,
						document.getDocumentElement().getElementsByTagName(TAG_EVENTO).item(i));
				signature.sign(dsc);
			}
			
			return outputXML(document);
			
		} catch (SAXException | IOException | ParserConfigurationException e) {
			throw new ApplicationException(e.getMessage());
			
		}
    }

    private String signSefaz(String xml, String alias, String password, String evento) throws Exception {

    	Document document = documentFactory(xml); 
        XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
        ArrayList<Transform> transformList = signatureFactory(signatureFactory);
        NodeList elements = document.getElementsByTagName(evento);
        StringWriter sw = new StringWriter();
        Transformer serializer = TransformerFactory.newInstance().newTransformer(); 
        serializer.transform(new DOMSource(elements.item(0)), new StreamResult(sw));
        org.w3c.dom.Element el = (org.w3c.dom.Element) elements.item(0);
        String id = el.getAttribute(ID);
        Reference ref = signatureFactory.newReference("#" + id, signatureFactory.newDigestMethod(DigestMethod.SHA1, null),  
        		transformList, null, null);
        SignedInfo si = signatureFactory.newSignedInfo(signatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,  
        		(C14NMethodParameterSpec) null), signatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null), Collections.singletonList(ref));    
        keyInfo = getKeyInfo();
        XMLSignature signature = signatureFactory.newXMLSignature(si, keyInfo);
        privateKey = getPrivateKey();
        DOMSignContext dsc = new DOMSignContext(privateKey, document.getFirstChild());
        signature.sign(dsc);

        return outputXML(document);
    }
    
    private ArrayList<Transform> signatureFactory(XMLSignatureFactory signatureFactory)
            throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {

    	ArrayList<Transform> transformList = new ArrayList<Transform>();
        TransformParameterSpec tps = null;
        Transform envelopedTransform = signatureFactory.newTransform(Transform.ENVELOPED, tps);
        Transform c14NTransform = signatureFactory.newTransform("http://www.w3.org/TR/2001/REC-xml-c14n-20010315", tps);
        transformList.add(envelopedTransform);
        transformList.add(c14NTransform);

        return transformList;
    }

    private String outputXML(Document doc) throws TransformerException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();
        trans.transform(new DOMSource(doc), new StreamResult(os));
        String xml = os.toString();
        if ((xml != null) && (!"".equals(xml))) {
       	    xml = xml.replaceAll("&#13;", "");
            xml = xml.replaceAll("\\r\\n", "");
            xml = xml.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"\\?>", "");
        }
        return xml;
    }
}