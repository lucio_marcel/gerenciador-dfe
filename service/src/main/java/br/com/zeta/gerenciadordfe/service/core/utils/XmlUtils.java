package br.com.zeta.gerenciadordfe.service.core.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;

import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;
import br.com.zeta.gerenciadordfe.service.core.xml.XMLNamespaceFilter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XmlUtils {
	
	private static String contextPath;
	
	public static String getContextPath() {
		return contextPath;
	}

	public static void setContextPath(String contextPath) {
		XmlUtils.contextPath = contextPath;
	}
	

	public static Document xmlToDocument(String xmlString) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();		
		return builder.parse(new InputSource(new ByteArrayInputStream(xmlString.getBytes("utf-8"))));
	}
	
	public static String documentToXml(Document document) throws Exception {
		Transformer trans = TransformerFactory.newInstance().newTransformer();  
		StringWriter writer = new StringWriter();  
		trans.transform(new DOMSource(document.getDocumentElement()), new StreamResult(writer));
		
		return writer.toString(); 
	}

	public static Object xmlToObjectWithNamespace(String xmlString, Class<?> declaredType, String namespace) throws Exception {
		try {
		Unmarshaller unmarshaller = JAXBContext.newInstance(declaredType).createUnmarshaller();
		SAXParserFactory factorySax = SAXParserFactory.newInstance();
		XMLReader reader = factorySax.newSAXParser().getXMLReader();
		XMLFilter xmlFilter = new XMLNamespaceFilter(reader,namespace);
		reader.setContentHandler(unmarshaller.getUnmarshallerHandler());
		Reader stringReader = new StringReader((String)xmlString);
		SAXSource sourceSax = new SAXSource(xmlFilter, new InputSource(stringReader));

		return unmarshaller.unmarshal(sourceSax);
		} catch (SAXParseException e) {
			throw new ApplicationException(e.getMessage(), e);
		}
	}
	
	public static String objectToXml(Object object) throws Exception {
		Document document = createDocument();
		JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
		
		if (getContextPath() != null) {
			jaxbContext = JAXBContext.newInstance(getContextPath());
		}
		
		Marshaller marshaller = jaxbContext.createMarshaller();
		//marshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new JaxbCharacterEscapeHandler());
		//marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
		//marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		marshaller.marshal(object, document);

		return documentToXml(document);
	}
	
	public static Document createDocument() throws Exception {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(Boolean.FALSE);
		DocumentBuilder builder = factory.newDocumentBuilder();		
		
		return builder.newDocument();
	}

	public static String removeAccents(String str) {
		try {
	    	str = Normalizer.normalize(str, Normalizer.Form.NFD)
	    			.replaceAll("[^\\p{ASCII}]", "")
	    			.replaceAll("\r\n", "")
	    			.replaceAll("&#13;", "")
	    			.replaceAll("&lt;", "<")
	    			.replaceAll("&gt;", ">")
	    			.replaceAll("&#xD;", "")
	    			.trim();	 
	    	
			return resetEncoding(str);
			
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage());
		}
		
		return null;
	}

	public static String resetEncoding(String text) throws UnsupportedEncodingException {
		if (text != null){				
			byte btext[] = text.getBytes("ISO-8859-1"); 
			text = new String(btext, "UTF-8");
		}
		
		return text;
	}

	public static boolean isNullOrEmpty(String str) {
        return (str == null || str.trim().isEmpty()) ? true : false;
    }
	
	public static boolean isNullOrZero(Integer value) {
        return (value == null || value == 0) ? true : false;
    }

	public static String gZipToXml(byte[] conteudo) throws IOException {
		GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(conteudo));	    
	    BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));  	   
	    String outStr = "";
	    String line = "";
	    
	    while ((line = bf.readLine()) != null) {  
	        outStr += line;  
	    } 
	    
	    return outStr;    
	}
	
}
