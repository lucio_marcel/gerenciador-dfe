package br.com.zeta.gerenciadordfe.service;

import static br.com.zeta.gerenciadordfe.domain.core.enums.MotivoCertificadoInvalido.EMPRESA_NAO_POSSUI_CERTIFICADO;

import java.nio.file.Path;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.core.enums.StatusEmpresa;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta;
import br.com.zeta.gerenciadordfe.domain.exception.EntityDataIntegrityViolationException;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.exception.entity.EmpresaNaoEncontradaException;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;
import br.com.zeta.gerenciadordfe.domain.model.entity.CertificadoFileProperties;
import br.com.zeta.gerenciadordfe.domain.repository.EmpresaRepository;
import br.com.zeta.gerenciadordfe.domain.service.CertificadoService;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;
import br.com.zeta.gerenciadordfe.domain.service.storage.CertificadoStorageService;
import br.com.zeta.gerenciadordfe.domain.service.storage.CertificadoStorageService.NovoCertificado;

@Service
public class EmpresaServiceImpl implements EmpresaService{

	@Autowired
	private EmpresaRepository empresaRepository;
	
	@Autowired
	private CertificadoStorageService certificadoStorageService;
	
	@Autowired
	CertificadoService certificadoService;
	
	@Override
	@Transactional
	public Empresa adicionar(Empresa empresa) {
		try {
			empresa.setStatusEmpresa(StatusEmpresa.INATIVO);
			empresa.setUltimoNsuConsultado("000000000000000");
			empresa.setMaiorNsuExistente("000000000000000");
			empresa.setStatusUltimaConsulta(StatusResposta.NAO_EXECUTADA);
			
			return empresaRepository.saveAndFlush(empresa);
		
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new EntityDataIntegrityViolationException(e);
		
		}
	}
	
	@Override
	@Transactional
	public Empresa atualizar(Empresa empresa) {
		try {
			if (empresa.getId() == null) {
				return adicionar(empresa);
			}
			
			return empresaRepository.saveAndFlush(empresa);
		
		} catch (DataIntegrityViolationException e) {
			throw new EntityDataIntegrityViolationException(e);

		}
	}

	@Override
	public Empresa buscarPorId(Long id) {
		return empresaRepository.findById(id)
				.orElseThrow(() -> new EmpresaNaoEncontradaException("id",id));
	}
	
	@Override
	public Empresa buscarPorCodigo(Long codigo) {
		return empresaRepository.findByCodigo(codigo)
				.orElseThrow(() -> new EmpresaNaoEncontradaException("código",codigo));
	}
	
	@Override
	public List<Empresa> buscarTodas() {
		return empresaRepository.findAll();
	}

	@Override
	public List<Empresa> buscarTodasAtivas() {
		return empresaRepository.findByStatusEmpresa(StatusEmpresa.ATIVO);
	}

	@Override
	@Transactional
	public void ativar(Long codigo) {
		Empresa empresa = buscarPorCodigo(codigo);
		
		if (empresa.possuiCertificadoValido()) {
			empresa.ativar();
			atualizar(empresa);
			
		} else {
			String descricao = empresa.motivoCertificadoInvalido().getDescricao();
			
			throw new CertificadoException(
					String.format("Certificado digital inválido. %s", descricao));
		}		
	}

	@Override
	@Transactional
	public void inativar(Long codigo) {
		Empresa empresa = buscarPorCodigo(codigo);
		empresa.inativar();
		atualizar(empresa);
	}

	@Override
	public Empresa refresh(Empresa empresa) {
		return buscarPorId(empresa.getId());
		
	}

	@Override
	@Transactional
	public Certificado atualizarCertificado(Long empresaCodigo, CertificadoFileProperties certificadoFileProperties) throws CertificadoException{
		Empresa empresa = buscarPorCodigo(empresaCodigo);
		
		String nomeArquivoAtual = null;
		
		if (empresa.getCertificado() != null) {
			nomeArquivoAtual = empresa.getCertificado().getFilePath();
		}
		
		NovoCertificado novoCertificado = NovoCertificado.builder()
				.nomeArquivo(empresa.getCnpj() + "-" + certificadoFileProperties.getOriginalFilename())
				.inputStream(certificadoFileProperties.getFileInputStream())
				.build();
		
		Path pathArquivo = certificadoStorageService.substituir(nomeArquivoAtual, novoCertificado);
		
		Certificado certificado = null;
		
		try {
			certificado = certificadoService.certificadoPfx(pathArquivo.toString(), certificadoFileProperties.getPassword());
		} catch (CertificadoException e) {
			throw new CertificadoException(e.getMessage());
		}
		
		empresa.setCertificado(certificado);
		
		if (!empresa.possuiCertificadoValido()) {
			String descricao = empresa.motivoCertificadoInvalido().getDescricao();
			
			throw new CertificadoException(
					String.format("Certificado digital inválido. %s", descricao));
		}		
		
		atualizar(empresa);
		
		return certificado;
	}

	@Override
	public Certificado buscarCertificado(Long empresaCodigo) {
		Empresa empresa = buscarPorCodigo(empresaCodigo);
		
		if (!empresa.possuiCertificadoValido()
				&& empresa.motivoCertificadoInvalido().equals(EMPRESA_NAO_POSSUI_CERTIFICADO)) {
			throw new CertificadoNaoEncontradoException(EMPRESA_NAO_POSSUI_CERTIFICADO.getDescricao());
		}
		
		return empresa.getCertificado();
	}
	
	@Override
	@Transactional
	public void excluirCertificado(Long empresaCodigo) {
		// TODO Auto-generated method stub
	}
	
}
