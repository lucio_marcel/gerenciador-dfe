package br.com.zeta.gerenciadordfe.service.core.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class GZip {

	public static String toString(byte[] zipContent) throws IOException {

		if (zipContent == null || zipContent.length == 0) {
			return "";
		}

		GZIPInputStream gzipInputStream = new GZIPInputStream(new ByteArrayInputStream(zipContent));
		BufferedReader byfferReader = new BufferedReader(new InputStreamReader(gzipInputStream, "UTF-8"));

		StringBuilder unzippedString = new StringBuilder();
		String line;

		while ((line = byfferReader.readLine()) != null) {
			unzippedString.append(line);
		}

		return unzippedString.toString();
	}

	public static Reader toReader(byte[] zipContent) throws IOException {

		return new StringReader(toString(zipContent));

	}

	public static Object unZipAndUnMarshal(byte[] zipContent, String contextPath) throws Exception {

		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(contextPath);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			Object object = unmarshaller.unmarshal(toReader(zipContent));

			return object;

		} catch (IOException | JAXBException e) {
			throw new Exception(e.getMessage(), e);

		}
		
	}
	
	public static JAXBElement<?> unZipAndUnMarshal(byte[] zipContent, Class<?> classToBeBound) throws Exception {

		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(classToBeBound);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			JAXBElement<?> element = (JAXBElement<?>) unmarshaller.unmarshal(toReader(zipContent));

			return element;

		} catch (IOException | JAXBException e) {
			throw new Exception(e.getMessage(), e);

		}
		
	}
	
	public static JAXBElement<?> unZipAndUnMarshal(byte[] zipContent, Class<?>... classesToBeBound) throws Exception {

		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			JAXBElement<?> element = (JAXBElement<?>) unmarshaller.unmarshal(toReader(zipContent));

			return element;

		} catch (IOException | JAXBException e) {
			throw new Exception(e.getMessage(), e);

		}
		
	}

}
