package br.com.zeta.gerenciadordfe.service.core.xml;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

@Component
public class DefaultNamespacePrefixMapper extends NamespacePrefixMapper {

	private static Map<String, String> namespaceMap = new HashMap<>();

	static {
		namespaceMap.put("http://www.portalfiscal.inf.br/nfe", "");
		namespaceMap.put("http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4", "");
		namespaceMap.put("http://www.w3.org/2000/09/xmldsig#", "");
	}
	/**
	 * Create mappings.
	 */
	public DefaultNamespacePrefixMapper() {
		
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		return namespaceMap.getOrDefault(namespaceUri, suggestion);
	}

}
