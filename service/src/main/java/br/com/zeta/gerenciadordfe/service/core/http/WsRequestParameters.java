package br.com.zeta.gerenciadordfe.service.core.http;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WsRequestParameters {
	
	private String service;
	private String webserviceUrl;
	private String certificateFilePath;
	private String certificatePassowrd;
	private String soapMessage;
	private String soapAction;
	private UF uf;

}
