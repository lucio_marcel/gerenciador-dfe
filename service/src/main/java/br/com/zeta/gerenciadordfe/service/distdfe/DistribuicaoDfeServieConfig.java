package br.com.zeta.gerenciadordfe.service.distdfe;

import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.ENVIO_CONSULTA_DFE;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.RETORNO_CONSULTA_DFE;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.Marshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.WebServiceMessageFactory;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.client.support.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;

import br.com.zeta.gerenciadordfe.domain.core.utils.WsdlFactory;
import br.com.zeta.gerenciadordfe.domain.service.storage.XmlStorageService;

@Configuration
public class DistribuicaoDfeServieConfig {
	
	@Autowired private WsdlFactory wsdlFactory;
	@Autowired private ResourceLoader resourceLoader;
	@Autowired private XmlStorageService xmlStorageService;

	private Resource requestSchema;

	private Jaxb2Marshaller getMarshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		Map<String, Object> properties = new HashMap<>();
		properties.put(Marshaller.JAXB_ENCODING, "UTF-8");
		properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.setMarshallerProperties(properties);
		marshaller.setContextPaths("br.inf.portalfiscal.nfe.distdfe:"
				+ "br.inf.portalfiscal.nfe.wsdl.nfedistribuicaodfe:");
		return marshaller;
	}
	
	private Wss4jSecurityInterceptor securityInterceptor() throws Exception {
		Wss4jSecurityInterceptor securityInterceptor = new Wss4jSecurityInterceptor();
		securityInterceptor.setValidationActions("NoSecurity");
		return securityInterceptor;
	}

	@SuppressWarnings("unused")
	private PayloadValidatingInterceptor payloadValidatingInterceptor(String xsdSchemaPath) {
		requestSchema = resourceLoader.getResource("file:" + xsdSchemaPath);
		PayloadValidatingInterceptor payloadValidatingInterceptor = new PayloadValidatingInterceptor();
		payloadValidatingInterceptor.setValidateRequest(true);
		payloadValidatingInterceptor.setSchema(requestSchema);
		return payloadValidatingInterceptor;
	}
	
	private WebServiceMessageFactory getMessageFactory() throws SOAPException {
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(MessageFactory.newInstance());
		saajSoapMessageFactory.setMessageFactory(messageFactory);
		return saajSoapMessageFactory;
	}

	@Bean
	public DistribuicaoDfeServiceImpl nfeDistDfeInteresseService() throws Exception {
		DistribuicaoDfeServiceImpl nfeDistDfeInteresseService = new DistribuicaoDfeServiceImpl();
		
		nfeDistDfeInteresseService.setDefaultUri(wsdlFactory.getWsdlConsultaDocumentos());
		nfeDistDfeInteresseService.setMarshaller(getMarshaller());
		nfeDistDfeInteresseService.setUnmarshaller(getMarshaller());
		nfeDistDfeInteresseService.setMessageFactory(getMessageFactory());
		
		ClientInterceptor[] interceptors = new ClientInterceptor[] { securityInterceptor() };
		
		interceptors = (ClientInterceptor[]) ArrayUtils.add(interceptors, new ClientInterceptor() {
			
		    @Override
		    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
		    	xmlStorageService.salvar(ENVIO_CONSULTA_DFE,
		    			nfeDistDfeInteresseService.getDocumentoConsulta(), messageContext.getRequest(), nfeDistDfeInteresseService.getEmpresaConsulta().getCnpj());
		        return true;
		    }

		    @Override
		    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
		    	xmlStorageService.salvar(RETORNO_CONSULTA_DFE,
		    			nfeDistDfeInteresseService.getDocumentoConsulta(), messageContext.getResponse(), nfeDistDfeInteresseService.getEmpresaConsulta().getCnpj());
		        return true;
		    }

		    @Override
		    public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
		        return true;
		    }

		    @Override
		    public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {
				
		    }
		});
		
		nfeDistDfeInteresseService.setInterceptors(interceptors);

		return nfeDistDfeInteresseService;
	}
	
}
