package br.com.zeta.gerenciadordfe.service.core.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

public class XMLNamespaceFilter extends XMLFilterImpl {
	
	private String namemspace;
	
	public XMLNamespaceFilter(XMLReader reader, String namespace) {
		super(reader);
		this.namemspace = namespace;
	}

	@Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
       super.startElement(this.namemspace, localName, qName, attributes);
    }
}
