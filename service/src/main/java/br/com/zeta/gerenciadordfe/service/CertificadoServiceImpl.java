package br.com.zeta.gerenciadordfe.service;

import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado.ARQUIVO_PFX;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado.REPOSITORIO_WINDOWS;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;

import org.apache.commons.httpclient.protocol.Protocol;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERUTF8String;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoCertificado;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;
import br.com.zeta.gerenciadordfe.domain.service.CertificadoService;
import br.com.zeta.gerenciadordfe.service.core.certificate.SocketFactoryDinamico;

@Service
public class CertificadoServiceImpl implements CertificadoService {

	private final DERObjectIdentifier CNPJ = new DERObjectIdentifier("2.16.76.1.3.3");
	private final DERObjectIdentifier CPF = new DERObjectIdentifier("2.16.76.1.3.1");

	@Override
	public void inicializaCertificado(Certificado certificado, InputStream cacertInputStream) throws CertificadoException  {

		Optional.ofNullable(certificado).orElseThrow(() -> new CertificadoException("Certificado não pode ser nulo."));
		Optional.ofNullable(cacertInputStream).orElseThrow(() -> new CertificadoException("Cacert não pode ser nulo."));

		try {
			KeyStore keyStore = getKeyStore(certificado);
			SocketFactoryDinamico socketFactory = new SocketFactoryDinamico(keyStore, certificado.getAlias(),
					certificado.getPassword(), cacertInputStream, certificado.getSslProtocol());
			Protocol protocol = new Protocol("https", socketFactory, 443);
			Protocol.registerProtocol("https", protocol);
			
		} catch (KeyManagementException | KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException	e) {
			throw new CertificadoException(e.getMessage(), e.getCause());
			
		}
		
	}
	
	private void setDadosCertificado(Certificado certificado) throws CertificadoException {
		
		try {
			KeyStore keyStore = getKeyStore(certificado);
			Enumeration<String> aliasEnum = keyStore.aliases();
			String aliasKey = aliasEnum.nextElement();

			certificado.setAlias(aliasKey);
			certificado.setCnpjCpf(getDocumentoFromCertificado(certificado, keyStore));
			certificado.setVencimento(dataValidade(certificado).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			certificado.setDataHoraVencimento(dataValidade(certificado).toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
			
		} catch (CertificadoException | KeyStoreException e) {
			throw new CertificadoException(e.getMessage(), e.getCause());
			
		}

	}

	@Override
	public Certificado certificadoPfx(String filePath, String password) throws CertificadoException{

		Optional.ofNullable(filePath).orElseThrow(() -> new CertificadoException("Caminho do Certificado não pode ser nulo."));
		Optional.ofNullable(password).orElseThrow(() -> new CertificadoException("Senha não pode ser nula."));

		if (!Files.exists(Paths.get(filePath)))
			throw new CertificadoNaoEncontradoException("O arquivo " + filePath + " não existe.");

		try {
			Certificado certificado = new Certificado();
			certificado.setFilePath(filePath);
			certificado.setPassword(password);
			certificado.setTipoCertificado(ARQUIVO_PFX);

			setDadosCertificado(certificado);

			return certificado;
			
		} catch (CertificadoException e) {
			throw new CertificadoException(e.getMessage(), e.getCause());
			
		}
	}

	@Override
	public List<Certificado> listaCertificadosWindows() throws CertificadoException {
		List<Certificado> listaCert = new ArrayList<>();
		Certificado certificado = new Certificado();
		certificado.setTipoCertificado(REPOSITORIO_WINDOWS);
		
		try {
			KeyStore ks = getKeyStore(certificado);
			Enumeration<String> aliasEnum = ks.aliases();

			while (aliasEnum.hasMoreElements()) {
				String aliasKey = aliasEnum.nextElement();

				if (aliasKey != null) {
					setDadosCertificado(listaCert, ks, aliasKey, REPOSITORIO_WINDOWS);
				}

			}

		} catch (KeyStoreException ex) {
			throw new CertificadoException("Erro ao Carregar Certificados:" + ex.getMessage());
			
		}

		return listaCert;

	}

	private void setDadosCertificado(List<Certificado> listaCert, KeyStore keyStore, String aliasKey, TipoCertificado tipoCertificadoEnum) throws CertificadoException {
		Certificado cert = new Certificado();
		cert.setAlias(aliasKey);
		cert.setTipoCertificado(tipoCertificadoEnum);
		cert.setPassword("");
		cert.setCnpjCpf(getDocumentoFromCertificado(cert, keyStore));
		
		Date dataValidade = dataValidade(cert);
		
		if (dataValidade == null) {
			cert.setAlias("(INVALIDO)" + aliasKey);
			cert.setVencimento(LocalDate.of(2000, 1, 1));
			cert.setDataHoraVencimento(LocalDateTime.of(2000, 1, 1, 0, 0, 0));

		} else {
			cert.setVencimento(dataValidade.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			cert.setDataHoraVencimento(dataValidade.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
		
		}

		listaCert.add(cert);
	}

	private Date dataValidade(Certificado certificado) throws CertificadoException {

		KeyStore keyStore = getKeyStore(certificado);
		
		if (keyStore == null) {
			throw new CertificadoException("Erro Ao pegar Keytore, verifique o Certificado.");
		}

		X509Certificate certificate = getCertificate(certificado, keyStore);
		
		return certificate.getNotAfter();
	}
	
	@Override
	public KeyStore getKeyStore(Certificado certificado) throws CertificadoException {
		try {
			KeyStore keyStore;

			switch (certificado.getTipoCertificado()) {
			
			case REPOSITORIO_WINDOWS:
				keyStore = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
				keyStore.load(null, null);
				return keyStore;

			case ARQUIVO_PFX:
				File file = new File(certificado.getFilePath());
				
				if (!file.exists()) {
					throw new CertificadoException("Certificado Digital não Encontrado");
				}
				
				keyStore = KeyStore.getInstance("PKCS12");
				
				try (ByteArrayInputStream bs = new ByteArrayInputStream(Files.readAllBytes(file.toPath()))) {
					keyStore.load(bs, certificado.getPassword().toCharArray());
				}
				
				return keyStore;

			default:
				throw new CertificadoException(
						"Tipo de certificado não Configurado: " + certificado.getTipoCertificado());
			}
			
		} catch (NoSuchAlgorithmException | CertificateException | IOException | KeyStoreException | NoSuchProviderException e) {
			if (Optional.ofNullable(e.getMessage()).orElse("").startsWith("keystore password was incorrect"))
				throw new CertificadoException("Senha do certificado inválida.");

			throw new CertificadoException("Erro ao carregar aKeyStore: " + e.getMessage());
		}

	}

	@Override
	public X509Certificate getCertificate(Certificado certificado, KeyStore keystore) throws CertificadoException {
		
		try {
			return (X509Certificate) keystore.getCertificate(certificado.getAlias());
		} catch (KeyStoreException e) {
			throw new CertificadoException("Erro Ao pegar X509Certificate: " + e.getMessage());
		}
		
	}
	
	@Override
	public Certificado getCertificadoByCnpjCpf(String cnpjCpf) throws CertificadoException {
		return listaCertificadosWindows().stream().filter(cert -> cnpjCpf.equals(cert.getCnpjCpf())).findFirst()
				.orElseThrow(() -> new CertificadoNaoEncontradoException("Certificado não encontrado com CNPJ/CPF : " + cnpjCpf));
	}

	private String getDocumentoFromCertificado(Certificado certificado, KeyStore keyStore) throws CertificadoException{

		final String[] cnpjCpf = { "" };
		try {
			X509Certificate certificate = getCertificate(certificado, keyStore);

			Optional.ofNullable(certificate.getSubjectAlternativeNames())
					.ifPresent(lista -> lista.stream().filter(x -> x.get(0).equals(0)).forEach(a -> {
						byte[] data = (byte[]) a.get(1);
						try (ASN1InputStream is = new ASN1InputStream(data)) {
							DERSequence derSequence = (DERSequence) is.readObject();
							DERObjectIdentifier tipo = DERObjectIdentifier.getInstance(derSequence.getObjectAt(0));
							if (CNPJ.equals(tipo) || CPF.equals(tipo)) {
								Object objeto = ((DERTaggedObject) ((DERTaggedObject) derSequence.getObjectAt(1))
										.getObject()).getObject();
								if (objeto instanceof DEROctetString) {
									cnpjCpf[0] = new String(((DEROctetString) objeto).getOctets());
								} else if (objeto instanceof DERPrintableString) {
									cnpjCpf[0] = ((DERPrintableString) objeto).getString();
								} else if (objeto instanceof DERUTF8String) {
									cnpjCpf[0] = ((DERUTF8String) objeto).getString();
								} else if (objeto instanceof DERIA5String) {
									cnpjCpf[0] = ((DERIA5String) objeto).getString();
								}
							}
							if (CPF.equals(tipo) && cnpjCpf[0].length() > 25) {
								cnpjCpf[0] = cnpjCpf[0].substring(8, 19);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}));

		} catch (Exception e) {
			throw new CertificadoException("Erro ao localizar CNPJ/CPFdo Certificado: " + e.getMessage());
			
		}
		
		return cnpjCpf[0];
	}
}
