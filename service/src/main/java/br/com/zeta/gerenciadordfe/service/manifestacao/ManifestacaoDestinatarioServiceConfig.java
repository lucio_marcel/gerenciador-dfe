package br.com.zeta.gerenciadordfe.service.manifestacao;

import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.bind.Marshaller;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.WebServiceMessageFactory;
import org.springframework.ws.client.support.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import br.com.zeta.gerenciadordfe.domain.core.utils.WsdlFactory;
import br.com.zeta.gerenciadordfe.domain.service.storage.XmlStorageService;
import br.com.zeta.gerenciadordfe.service.core.utils.JaxbCharacterEscapeHandler;

@Configuration
public class ManifestacaoDestinatarioServiceConfig {
	
	@Autowired private WsdlFactory wsdlFactory;
	@Autowired private ResourceLoader resourceLoader;
	@Autowired private XmlStorageService xmlStorageService;

	private Resource requestSchema;

	private Jaxb2Marshaller createMarshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setCheckForXmlRootElement(true);
        marshaller.setSupportDtd(true);
        marshaller.setSupportJaxbElementClass(false);
        
        marshaller.setMarshallerProperties(
                Map.of(
                    Marshaller.JAXB_ENCODING, StandardCharsets.ISO_8859_1.toString(),
                    Marshaller.JAXB_FRAGMENT, false,
                    Marshaller.JAXB_FORMATTED_OUTPUT, false,
                    "com.sun.xml.bind.marshaller.CharacterEscapeHandler", JaxbCharacterEscapeHandler.theInstance,
                    "com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>"
                )
            );
            marshaller.setUnmarshallerProperties(
                Map.of(
                    Marshaller.JAXB_ENCODING, StandardCharsets.ISO_8859_1.toString()
                )
            );
		
//		Map<String, Object> properties = new HashMap<>();
//		properties.put(Marshaller.JAXB_ENCODING, "UTF-8");
//		properties.put(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//		properties.put("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new JaxbCharacterEscapeHandler());
//		marshaller.setMarshallerProperties(properties);
//		marshaller.setUnmarshallerProperties(properties);
//		marshaller.setContextPaths("br.inf.portalfiscal.nfe.v_4_00.evento.manifestacao:"
//				+ "br.inf.portalfiscal.nfe.v_4_00.evento.recepcaoevento.ws"
//				+ ":org.w3._2003._05.xmldsig");
		return marshaller;
	}
	
//	private Wss4jSecurityInterceptor securityInterceptor(Certificado certificado) throws Exception {
//		Wss4jSecurityInterceptor securityInterceptor = new Wss4jSecurityInterceptor();
//		securityInterceptor.setValidationActions("NoSecurity");
//
//		// set security actions on request
//		securityInterceptor.setSecurementActions("Signature");
//		// sign the request
//		securityInterceptor.setSecurementUsername(certificado.getAlias());
//		securityInterceptor.setSecurementPassword(certificado.getPassword());
//		securityInterceptor.setSecurementSignatureCrypto(getCryptoFactoryBean(certificado).getObject());
//		securityInterceptor.setSecurementSignatureKeyIdentifier("DirectReference");
//
//		securityInterceptor.setValidationActions("NoSecurity");
//		return securityInterceptor;
//      
//	}
//
//  
//  public CryptoFactoryBean getCryptoFactoryBean(Certificado certificado) throws IOException {
//	  Resource clientKeyStoreFile = resourceLoader.getResource("file:" + certificado.getFilePath());
//	  
//      CryptoFactoryBean cryptoFactoryBean = new CryptoFactoryBean();
//      cryptoFactoryBean.setKeyStorePassword(certificado.getPassword());
//      cryptoFactoryBean.setKeyStoreLocation(clientKeyStoreFile);
//      return cryptoFactoryBean;
//  }
	
	@SuppressWarnings("unused")
	private PayloadValidatingInterceptor payloadValidatingInterceptor(String xsdSchemaPath) {
		requestSchema = resourceLoader.getResource("file:" + xsdSchemaPath);
		PayloadValidatingInterceptor payloadValidatingInterceptor = new PayloadValidatingInterceptor();
		payloadValidatingInterceptor.setValidateRequest(true);
		payloadValidatingInterceptor.setSchema(requestSchema);
		return payloadValidatingInterceptor;
	}
	
	private WebServiceMessageFactory getMessageFactory() throws SOAPException {
		MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
		SaajSoapMessageFactory saajSoapMessageFactory = new SaajSoapMessageFactory(MessageFactory.newInstance());
		saajSoapMessageFactory.setMessageFactory(messageFactory);
		return saajSoapMessageFactory;
	}

	@Bean
	public ManifestacaoDestinatarioServiceImpl ManifestacaoDestinatario() throws Exception {
		ManifestacaoDestinatarioServiceImpl manifestacaoDestinatarioService = new ManifestacaoDestinatarioServiceImpl();
		
		manifestacaoDestinatarioService.setDefaultUri(wsdlFactory.getWsdlManifestacao());
//		manifestacaoDestinatarioService.setMarshaller(getMarshaller());
//		manifestacaoDestinatarioService.setUnmarshaller(getMarshaller());
		manifestacaoDestinatarioService.setMessageFactory(getMessageFactory());
		
//		ClientInterceptor[] interceptors = new ClientInterceptor[] { securityInterceptor(manifestacaoDestinatarioService.getEmpresa().getCertificado()) };
//		
//		interceptors = (ClientInterceptor[]) ArrayUtils.add(interceptors, new ClientInterceptor() {
//			String nomeArquivo ="teste";// beanManifestacaoDestinatarioService.getEvento().getChaveAcesso();
//			
//		    @Override
//		    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
////		    	SoapMessage soapMessage = (SoapMessage) messageContext.getRequest();
////		        soapMessage.setSoapAction("http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEventoNF");
//		    	
//		    	xmlStorageService.salvar(ENVIO_MANIFESTACAO_DESTINATARIO,
//						nomeArquivo, messageContext.getRequest(), manifestacaoDestinatarioService.getEmpresa().getCnpj());
//		        return true;
//		    }
//
//		    @Override
//		    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
//		    	xmlStorageService.salvar(RETORNO_MANIFESTACAO_DESTINATARIO,
//						nomeArquivo, messageContext.getResponse(), manifestacaoDestinatarioService.getEmpresa().getCnpj());
//		        return true;
//		    }
//
//		    @Override
//		    public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
//		        return true;
//		    }
//
//		    @Override
//		    public void afterCompletion(MessageContext messageContext, Exception ex) throws WebServiceClientException {
//				
//		    }
//		});
//		
//		manifestacaoDestinatarioService.setInterceptors(interceptors);
//
		return manifestacaoDestinatarioService;
	}
	
	@Bean
    public Boolean disableSSLValidation() throws Exception {
        final SSLContext sslContext = SSLContext.getInstance("TLS");

        sslContext.init(null, new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }}, null);

        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        return true;
    }

}
