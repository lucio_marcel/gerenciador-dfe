package br.com.zeta.gerenciadordfe.service;

import java.time.OffsetDateTime;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoConsulta;
import br.com.zeta.gerenciadordfe.domain.exception.EntityDataIntegrityViolationException;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Historico;
import br.com.zeta.gerenciadordfe.domain.repository.HistoricoRepository;
import br.com.zeta.gerenciadordfe.domain.service.HistoricoService;

@Service
public class HistoricoServiceImpl implements HistoricoService{

	
	@Autowired
	private HistoricoRepository historicoRepository;

	
	@Override
	@Transactional
	public Historico adicionar(Historico historico) {
		try {		
			return historicoRepository.saveAndFlush(historico);
		
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new EntityDataIntegrityViolationException(e);
		
		}
	}


	@Override
	public Long contarConsultasUltimaHoraPorTipo(TipoConsulta tipoConsulta, Long empresaId) {
		try {		
			OffsetDateTime menosUmaHora = OffsetDateTime.now().minusHours(1);
			return historicoRepository.contarConsultasUltimaHoraPorTipo(empresaId, tipoConsulta, menosUmaHora);
		
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			throw new EntityDataIntegrityViolationException(e);
		
		}
		
	}
	
}
