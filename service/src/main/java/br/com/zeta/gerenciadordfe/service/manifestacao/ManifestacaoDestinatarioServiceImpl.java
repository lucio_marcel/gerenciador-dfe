package br.com.zeta.gerenciadordfe.service.manifestacao;

import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.ENVIO_MANIFESTACAO_DESTINATARIO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.RETORNO_MANIFESTACAO_DESTINATARIO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.TipoEvento.OPERACAO_NAO_REALIZADA;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF.AMBIENTE_NACIONAL;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.callback.CallbackHandler;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.wss4j.common.crypto.CertificateStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.security.wss4j2.Wss4jSecurityInterceptor;
import org.springframework.ws.soap.security.wss4j2.support.CryptoFactoryBean;

import br.com.zeta.gerenciadordfe.domain.core.configuration.ConfigProperties;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.TipoEvento;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.ValidacaoEventoException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz.ConsultaNFeException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz.EventoNFeException;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.com.zeta.gerenciadordfe.domain.model.entity.eventonfe.EventoManifestacaoDestinatario;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;
import br.com.zeta.gerenciadordfe.domain.service.ResumoNFeService;
import br.com.zeta.gerenciadordfe.domain.service.manifestacao.ManifestacaoDestinatarioService;
import br.com.zeta.gerenciadordfe.domain.service.storage.XmlStorageService;
import br.com.zeta.gerenciadordfe.service.core.certificate.SefazSigner;
import br.com.zeta.gerenciadordfe.service.core.utils.DateUtils;
import br.com.zeta.gerenciadordfe.service.core.utils.JaxbCharacterEscapeHandler;
import br.com.zeta.gerenciadordfe.service.core.utils.WebServiceMessageFacory;
import br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils;
import br.inf.portalfiscal.nfe.manifestacao.TEnvEvento;
import br.inf.portalfiscal.nfe.manifestacao.TEvento;
import br.inf.portalfiscal.nfe.manifestacao.TRetEnvEvento;
import br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4.NfeDadosMsg;
import br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4.NfeRecepcaoEventoNFResult;
import lombok.Getter;

public class ManifestacaoDestinatarioServiceImpl extends WebServiceGatewaySupport
		implements ManifestacaoDestinatarioService {

	@Autowired private WebServiceMessageFacory webServiceMessageFacory;
	@Autowired private ResumoNFeService resumoNFeService;
	@Autowired private EmpresaService empresaService;
	@Autowired private XmlStorageService xmlStorageService;
	@Autowired private ResourceLoader resourceLoader;
	@Autowired private ConfigProperties configProperties;
	
	private static final String VERSAO_LEIAUTE = "1.00";
	private static final String VERSAO_LEIAUTE_EVENTO = "1.00";
	private static final String VERSAO_EVENTO = "1.00";

	@Getter	private EventoManifestacaoDestinatario evento;
	
	@Getter	private Empresa empresa;

	private ResumoNFe resumoNFe;

	ManifestacaoDestinatarioServiceImpl() {
		setMarshaller(createMarshaller());
		setUnmarshaller(createMarshaller());
	}
	
	@Override
	public void manifestarNFe(Long empresaCodigo, EventoManifestacaoDestinatario eventoManifestacaoDestinatario) {
		this.evento = eventoManifestacaoDestinatario;
		
		validarEvento(eventoManifestacaoDestinatario);
		
		this.empresa = empresaService.buscarPorCodigo(empresaCodigo);
		this.resumoNFe = resumoNFeService.buscarPorChaveAcesso(empresaCodigo, evento.getChaveAcesso());
		
		getWebServiceResponse();
	}

	@Override
	public void manifestarNFeAutorizadas(Long empresaCodigo) {
		List<ResumoNFe> resumosNfe = resumoNFeService.buscarTodosComManifestacaoPendente(empresaCodigo);

		resumosNfe.forEach(resumo -> {
			EventoManifestacaoDestinatario evento = EventoManifestacaoDestinatario.builder()
					.chaveAcesso(resumo.getChaveAcesso()).tipoEvento(TipoEvento.CONFIRMACAO_DA_OPERACAO.name()).build();

			manifestarNFe(empresaCodigo, evento);
		});

	}

	private void getWebServiceResponse() {
		
		ClientInterceptor[] interceptors;
		try {
			interceptors = new ClientInterceptor[] { securityInterceptor() };

			interceptors = (ClientInterceptor[]) ArrayUtils.add(interceptors, new ClientInterceptor() {
				String nomeArquivo = "teste";// beanManifestacaoDestinatarioService.getEvento().getChaveAcesso();

				@Override
				public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
//		    	SoapMessage soapMessage = (SoapMessage) messageContext.getRequest();
//		        soapMessage.setSoapAction("http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEventoNF");

					
					
					xmlStorageService.salvar(ENVIO_MANIFESTACAO_DESTINATARIO, nomeArquivo, messageContext.getRequest(),
							getEmpresa().getCnpj());
					return true;
				}

				@Override
				public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
					xmlStorageService.salvar(RETORNO_MANIFESTACAO_DESTINATARIO, nomeArquivo,
							messageContext.getResponse(), getEmpresa().getCnpj());
					return true;
				}

				@Override
				public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
					xmlStorageService.salvar(ENVIO_MANIFESTACAO_DESTINATARIO, nomeArquivo, messageContext.getRequest(),
							getEmpresa().getCnpj());
					
					xmlStorageService.salvar(RETORNO_MANIFESTACAO_DESTINATARIO, nomeArquivo,
							messageContext.getResponse(), getEmpresa().getCnpj());
					return true;
				}

				@Override
				public void afterCompletion(MessageContext messageContext, Exception ex)
						throws WebServiceClientException {
					
					xmlStorageService.salvar(ENVIO_MANIFESTACAO_DESTINATARIO, nomeArquivo, messageContext.getRequest(),
							getEmpresa().getCnpj());
					
					xmlStorageService.salvar(RETORNO_MANIFESTACAO_DESTINATARIO, nomeArquivo,
							messageContext.getResponse(), getEmpresa().getCnpj());

				}
			});

			setInterceptors(interceptors);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		
		
		try {
			setMessageSender(webServiceMessageFacory.getWebServiceMessageSender(empresa.getCertificado()));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ConsultaNFeException("Erro ao iniciar o processamento da consulta.");
		}

		NfeRecepcaoEventoNFResult result = new NfeRecepcaoEventoNFResult();

		NfeDadosMsg nfeDadosMensagem = getNfeDadosMsg();

//		String message =  getNfeDadosMsg();
//		
//		StreamSource source = new StreamSource(new StringReader(message));
//        StreamResult result1 = new StreamResult(System.out);
//        
		try {
//			 source = new StreamSource(new StringReader(message));
//	        result1 = new StreamResult(System.out);
//	        getWebServiceTemplate().sendSourceAndReceiveToResult(source, new SoapActionCallback("http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEventoNF"), result1);
//	        
			
			
		result = (NfeRecepcaoEventoNFResult) getWebServiceTemplate().marshalSendAndReceive(nfeDadosMensagem,
				new SoapActionCallback("http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEventoNF")); 
		

//		    public void doWithMessage(WebServiceMessage message) {
//		        ((SoapMessage)message).setSoapAction("http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4/nfeRecepcaoEventoNF");
//		    }
//		});
		} catch(Exception e) {
			e.printStackTrace();
		}
		
			TRetEnvEvento tRetEnvEvento = (TRetEnvEvento) result.getContent().get(0);

		StatusResposta statusResposta = StatusResposta.valueOfCodigo(tRetEnvEvento.getCStat());

		switch (statusResposta) {
		case LOTE_DE_EVENTO_PROCESSADO:
			tRetEnvEvento.getRetEvento().forEach(tRetEvento -> {
				StatusResposta statusEvento = StatusResposta.valueOfCodigo(tRetEvento.getInfEvento().getCStat());

				switch (statusEvento) {
				case EVENTO_REGISTRADO_E_VINCULADO_NFE:
				case EVENTO_REGISTRADO_E_NAO_VINCULADO_NFE:
					this.resumoNFe
							.setDataManifestacao(OffsetDateTime.parse(tRetEvento.getInfEvento().getDhRegEvento()));
					this.resumoNFe.setNfeManifestada(true);

					resumoNFe.manifestacaoRealizada();
					resumoNFeService.salvar(this.resumoNFe);
					break;

				default:
					throw new EventoNFeException(tRetEvento.getInfEvento().getXEvento(), statusEvento.getCodigo(),
							tRetEvento.getInfEvento().getXMotivo());
				}
			});

		default:
			throw new EventoNFeException("Manifestação do Destinatário", statusResposta.getCodigo(),
					tRetEnvEvento.getXMotivo());

		}

	}

	private NfeDadosMsg getNfeDadosMsg() {

		TipoEvento tipoevento = TipoEvento.valueOfCodigo(evento.getTipoEvento());

		TEvento.InfEvento.DetEvento detEvento = new TEvento.InfEvento.DetEvento();
		detEvento.setVersao(VERSAO_EVENTO);
		detEvento.setDescEvento(tipoevento.getDescricao());
		if (tipoevento == TipoEvento.OPERACAO_NAO_REALIZADA) {
			detEvento.setXJust(evento.getJustificativa());
		}

		TEvento.InfEvento infEvento = new TEvento.InfEvento();
		infEvento.setNSeqEvento("1");
		infEvento.setCOrgao(AMBIENTE_NACIONAL.getCodigo());
		infEvento.setVerEvento(VERSAO_EVENTO);
		infEvento.setTpEvento(evento.getTipoEvento());
		infEvento.setTpAmb(this.empresa.getTipoAmbiente().getCodigo());
		infEvento.setCNPJ(this.empresa.getCnpj());
		infEvento.setChNFe(this.evento.getChaveAcesso());
		infEvento.setDhEvento(
				DateUtils.newDateTimeZone("GMT", UF.parseByCodigoMunicipio(this.empresa.getCodigoMuncipio())));
		infEvento.setId(String.format("ID%s%s%02d", infEvento.getTpEvento(), infEvento.getChNFe(),
				Integer.parseInt(infEvento.getNSeqEvento())));
		infEvento.setDetEvento(detEvento);

		
		//SignatureType assinatura = new SignatureType();
		//assinatura.setSignedInfo(new SignedInfoType());
		
		TEvento tEvento = new TEvento();
		tEvento.setInfEvento(infEvento);
		tEvento.setVersao(VERSAO_LEIAUTE_EVENTO);
		//tEvento.setSignature(assinatura);

		TEnvEvento envEvento = new TEnvEvento();
		envEvento.setIdLote("1");
		envEvento.setVersao(VERSAO_LEIAUTE);
		envEvento.getEvento().add(tEvento);

		NfeDadosMsg nfeDadosMensagem = new NfeDadosMsg();
		//nfeDadosMensagem.getContent().add(tEnvEvento);

		String xml = null; 
		TEnvEvento teste; 		
		SefazSigner signer = new SefazSigner(empresa.getCertificado().getFilePath(),
				empresa.getCertificado().getAlias(), empresa.getCertificado().getPassword());
		try {

			//XmlUtils.setContextPath("br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4:br.inf.portalfiscal.nfe.manifestacao");
			//nfeDadosMensagem.getContent().add(envEvento);
			xml = XmlUtils.removeAccents(XmlUtils.objectToXml(envEvento));
			xml = signer.signEvento(xml, this.empresa.getCertificado().getAlias(),
					this.empresa.getCertificado().getPassword());
			
			
			
			nfeDadosMensagem.getContent().add(xml);
//			//xml = "<tEnvEvento xmlns=\"http://www.portalfiscal.inf.br/nfe\" versao=\"1.00\"><idLote>1</idLote><evento versao=\"1.00\"><infEvento Id=\"ID2102003521022930234800038755005000223560113007472501\"><cOrgao>91</cOrgao><tpAmb>1</tpAmb><CNPJ>08876402000166</CNPJ><chNFe>35210229302348000387550050002235601130074725</chNFe><dhEvento>2021-03-19T08:44:36-03:00</dhEvento><tpEvento>210200</tpEvento><nSeqEvento>1</nSeqEvento><verEvento>1.00</verEvento><detEvento versao=\"1.00\"><descEvento>Confirmacao da Operacao</descEvento></detEvento></infEvento><Signature ><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"#ID2102003521022930234800038755005000223560113007472501\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>UgWo8VwSCpLLR4Y6QgC9DFmOZWU=</DigestValue></Reference></SignedInfo><SignatureValue>XLBaQ6ks+zgbRaXQNHKPHCPKmqYI+Vyaq16EnncCpqYWy5Uy8bnO3LWVAuBHD+NfNwb2qLNMIHYT&#13;MdCB5n96xkqw26TsgNbg2h51BX0ltssyipVM26NnYoGKIupN6k0/sW3IX3yvNyoiXyUcGb2Vx0z3&#13;9eMpiB/3YRQ9irOTGzOEWFYA4xbDeM0qe6CFr4LTGGXsKclYWDlotABfH0CiM6Ob73JcRyFIsvCe&#13;iQGr2NipkxWkvVS1d4VtVZ0AbOaFMX6h1R1fT0BC9X0VPVty/PrXnILj5G1BNogyXv28Vw1gFPeF&#13;N5sHg4zn2zsLy5hY7JWffhb81MMo2PdwUgDT4Q==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIHOTCCBSGgAwIBAgIIcUUgESM/Q+owDQYJKoZIhvcNAQELBQAwWTELMAkGA1UEBhMCQlIxEzAR&#13;BgNVBAoTCklDUC1CcmFzaWwxFTATBgNVBAsTDEFDIFNPTFVUSSB2NTEeMBwGA1UEAxMVQUMgU09M&#13;VVRJIE11bHRpcGxhIHY1MB4XDTIwMTEyMzEzNDQwMFoXDTIxMTEyMzEzNDQwMFowgeMxCzAJBgNV&#13;BAYTAkJSMRMwEQYDVQQKEwpJQ1AtQnJhc2lsMQswCQYDVQQIEwJSUzEaMBgGA1UEBxMRU2FudGEg&#13;Q3J1eiBkbyBTdWwxHjAcBgNVBAsTFUFDIFNPTFVUSSBNdWx0aXBsYSB2NTEXMBUGA1UECxMOMDA5&#13;OTE1ODkwMDAxMzcxEzARBgNVBAsTClByZXNlbmNpYWwxGjAYBgNVBAsTEUNlcnRpZmljYWRvIFBK&#13;IEExMSwwKgYDVQQDEyNNRUNBTklDQSBIRU5OIEVJUkVMSTowODg3NjQwMjAwMDE2NjCCASIwDQYJ&#13;KoZIhvcNAQEBBQADggEPADCCAQoCggEBALONtkRGYKGlKkRNHFQ9d2Psu/Q2/7VILuUQHGUKWri6&#13;4a9nxGtZ+emSnxe1BpdxpQvIGUfQslQ7vQZLwQcLu61AktyMv0Hw72bq/H0X/e4X1epHjJXboSvz&#13;VFM2NEdmvm50F9lWUE2DzLeNkFVQnkJK2oNgJorA9yEzsFl+59xzCNkyxVKtoB19mN3kod3IrtFs&#13;dtDVR4ouqJQS95z9RPUFSnf7Mp+ppLBvYUFAkBdsGJE0nnaBfDC00to5mYdc2pseCryL9dRBHAv6&#13;fFD9YP4sMStQxs78B03zknqijEWapvbF02oDtU+tC5ulhLq3aHmVnixalvOwLpiG2sEqFZECAwEA&#13;AaOCAngwggJ0MAkGA1UdEwQCMAAwHwYDVR0jBBgwFoAUxVLtJYAJ35yCyJ9Hxt20XzHdubEwVAYI&#13;KwYBBQUHAQEESDBGMEQGCCsGAQUFBzAChjhodHRwOi8vY2NkLmFjc29sdXRpLmNvbS5ici9sY3Iv&#13;YWMtc29sdXRpLW11bHRpcGxhLXY1LnA3YjCBswYDVR0RBIGrMIGogSFvbmxpbmVAb25saW5lY29u&#13;dGFiaWxpZGFkZS5uZXQuYnKgFQYFYEwBAwKgDBMKUEFVTE8gSEVOTqAZBgVgTAEDA6AQEw4wODg3&#13;NjQwMjAwMDE2NqA4BgVgTAEDBKAvEy0wMjA4MTk1ODI2ODcwNjY1MDg3MDAwMDAwMDAwMDAwMDAw&#13;MDAwMDAwMDAwMDCgFwYFYEwBAwegDhMMMDAwMDAwMDAwMDAwMF0GA1UdIARWMFQwUgYGYEwBAgEm&#13;MEgwRgYIKwYBBQUHAgEWOmh0dHA6Ly9jY2QuYWNzb2x1dGkuY29tLmJyL2RvY3MvZHBjLWFjLXNv&#13;bHV0aS1tdWx0aXBsYS5wZGYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMIGMBgNVHR8E&#13;gYQwgYEwPqA8oDqGOGh0dHA6Ly9jY2QuYWNzb2x1dGkuY29tLmJyL2xjci9hYy1zb2x1dGktbXVs&#13;dGlwbGEtdjUuY3JsMD+gPaA7hjlodHRwOi8vY2NkMi5hY3NvbHV0aS5jb20uYnIvbGNyL2FjLXNv&#13;bHV0aS1tdWx0aXBsYS12NS5jcmwwHQYDVR0OBBYEFI7O0IF47z5KwnEezzb3QqnkaFyMMA4GA1Ud&#13;DwEB/wQEAwIF4DANBgkqhkiG9w0BAQsFAAOCAgEAeQfVZ94scK1lPZhD2Sf8dTunpd4Lif0RM82c&#13;F8KKF+v5wBczEl/Fhrvsx+qgMLCTmJb098jrceOJCbv/ywXwHIizCPi4MKq6zrOhOzinHDaRwyJf&#13;ZD0u8lB7AoYfvaFk3zf8eJxzHIZfek8PWAgMNSO6KE17kdO427Kmq/F6vsqcoQoVcyJPT06cYU9Z&#13;LVB+SEJhLpJYj5aJo7VDnWqpAtxBPpBQmAM3xrD7wlQSOsgjm62x+cZ94TSt6vx5Vt8tbGX7Frl1&#13;LwnNcbDP4eF4WJVY7vnneWzSNQKabZz3HvP+5ODnU5Cvb88SugpI4GY3AY7epqLvqwFjzj8TKW3U&#13;Am8qwbn19RPq0BQvMqWsGHzXdqOiP/ZT2kmP9tn9TpT51LtYidaZUjwOHUvBDi0RVBll6VW+4QIP&#13;6ePz+P7abh3Hz96u52CY4bNNCOeNxnzP5w1yX7zFv5iMXTnBzyQVFDlIrus+mYv1MnvD8nim1m9X&#13;aPaZFrz3hZecOxQdm7ZsjgADDSkwDJm+0JJyFFS3KUUSHcCt7pzAeTeq/vsAEHiMnXfm7eQ50Qjv&#13;6uIRuTJag2CVTTflLdglRuPm/S/f2Qtfilupf/FiKh8DSTC/lqTCAEMgsG8vNSvuNqeMC8Bm2Ovz&#13;gXE6X5M3JZ1YhtQ6BL9VtGplauFlyG9cbwZjLQs=</X509Certificate></X509Data></KeyInfo></Signature></evento></tEnvEvento>";
//
//			JAXBContext jaxbContext = JAXBContext.newInstance("br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4:br.inf.portalfiscal.nfe.manifestacao");
//			
//			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//			
//
//			Reader reader = new StringReader(xml);
//			teste = (TEnvEvento) unmarshaller.unmarshal(reader);
//				
//		nfeDadosMensagem.getContent().add(teste);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		//nfeDadosMensagem.getContent().remove(0);
		//nfeDadosMensagem.getContent().add(xml);
		return nfeDadosMensagem;
	}
	
	
	private void validarEvento(EventoManifestacaoDestinatario evento) throws ValidacaoEventoException{
		TipoEvento tipoevento = TipoEvento.valueOfCodigo(evento.getTipoEvento());
		
		if (tipoevento == null) {
			throw new ValidacaoEventoException(String.format("O tipo de evento '%s' não corrsponde a um tipo de evento de manifestação váido.", evento.getTipoEvento()));
		}
		
		if (tipoevento == OPERACAO_NAO_REALIZADA){
			if (evento.getJustificativa() == null || evento.getJustificativa().isEmpty()) {
				throw new ValidacaoEventoException(String.format("O Evento '%s' requer uma justificativa.",tipoevento.name()));	
			}
		}
	}
	
	private Wss4jSecurityInterceptor securityInterceptor() throws Exception {
		Wss4jSecurityInterceptor securityInterceptor = new Wss4jSecurityInterceptor();
		
	
//		// set security actions on request
//		securityInterceptor.setSecurementActions(ConfigurationConstants.SIGNATURE);
//		//securityInterceptor.setSecurementUsername(getEmpresa().getCertificado().getAlias());
//		//securityInterceptor.setSecurementEncryptionUser(getEmpresa().getCertificado().getAlias());
//		securityInterceptor.setSecurementSignatureUser(getEmpresa().getCertificado().getAlias());
//		//securityInterceptor.setValidationCallbackHandler(callBackHandler());
//		securityInterceptor.setSecurementSignatureParts("{Content}{http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4}nfeDadosMsg");
//		securityInterceptor. setSecurementSignatureCrypto(getCryptoFactoryBean().getObject());
//
//		securityInterceptor.setSecurementSignatureKeyIdentifier("EncryptedKeySHA1");
//
//        securityInterceptor.setSecurementPassword(getEmpresa().getCertificado().getPassword());
//
//        securityInterceptor.setSecureRequest(true);
//               
//
//        securityInterceptor.setValidateResponse(false);
//        
//        securityInterceptor.afterPropertiesSet();
		return securityInterceptor;
       
	}
	
	public CallbackHandler callBackHandler() {
		
		return null;
	}

	public CryptoFactoryBean getCryptoFactoryBean() throws Exception {
		Resource clientKeyStoreFile = resourceLoader.getResource("file:" + getEmpresa().getCertificado().getFilePath());

		CryptoFactoryBean cryptoFactoryBean = new CryptoFactoryBean();
		cryptoFactoryBean.setCryptoProvider(CertificateStore.class);
		cryptoFactoryBean.setKeyStoreType("JKS");
		cryptoFactoryBean.setKeyStorePassword(getEmpresa().getCertificado().getPassword());
		cryptoFactoryBean.setDefaultX509Alias(getEmpresa().getCertificado().getAlias());
		
		Properties cryptoFactoryBeanConfig = new Properties();
		cryptoFactoryBeanConfig.setProperty("org.apache.wss4j.crypto.provider","org.apache.wss4j.common.crypto.Merlin");
		cryptoFactoryBeanConfig.setProperty("org.apache.wss4j.crypto.merlin.CertificateStore",getEmpresa().getCertificado().getFilePath());
		cryptoFactoryBean.setKeyStoreLocation(clientKeyStoreFile);
		cryptoFactoryBeanConfig.setProperty("org.apache.wss4j.crypto.merlin.keystore.type","jks");
		cryptoFactoryBeanConfig.setProperty("org.apache.wss4j.crypto.merlin.keystore.password",getEmpresa().getCertificado().getPassword());
		cryptoFactoryBeanConfig.setProperty("org.apache.wss4j.crypto.merlin.keystore.alias",getEmpresa().getCertificado().getAlias());
		cryptoFactoryBeanConfig.setProperty("org.apache.wss4j.crypto.merlin.keystore.file",getEmpresa().getCertificado().getFilePath());

		cryptoFactoryBean.setConfiguration(cryptoFactoryBeanConfig);
		cryptoFactoryBean.afterPropertiesSet();
 	   return cryptoFactoryBean;
	   
	}
	
	public Jaxb2Marshaller createMarshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setCheckForXmlRootElement(true);
        marshaller.setSupportDtd(true);
        marshaller.setSupportJaxbElementClass(false);
		marshaller.setContextPaths("br.inf.portalfiscal.nfe.wsdl.nferecepcaoevento4:br.inf.portalfiscal.nfe.manifestacao");
        marshaller.setMarshallerProperties(
				Map.of(
						Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE,
						"com.sun.xml.bind.marshaller.CharacterEscapeHandler", JaxbCharacterEscapeHandler.theInstance));
//            marshaller.setUnmarshallerProperties(
//                Map.of(
//                    Marshaller.JAXB_ENCODING, "UTF-8"
//                )
//            );
        
        //						"com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper(), 
		return marshaller;
	}
}



