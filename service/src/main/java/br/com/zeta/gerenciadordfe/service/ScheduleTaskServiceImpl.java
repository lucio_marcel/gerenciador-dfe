package br.com.zeta.gerenciadordfe.service;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.service.ScheduleTaskService;

@Service
public class ScheduleTaskServiceImpl implements ScheduleTaskService {

	TaskScheduler scheduler;

	Map<Long, ScheduledFuture<?>> jobsMap = new HashMap<>();

	public ScheduleTaskServiceImpl(TaskScheduler scheduler) {
		this.scheduler = scheduler;
	}

	// Schedule Task to be executed every night at 00 or 12 am
	@Override
	public void addTaskToScheduler(Long taskid, Runnable task) {
		ScheduledFuture<?> scheduledTask = scheduler.schedule(task, new CronTrigger("0 0 0 * * ?", TimeZone.getTimeZone(TimeZone.getDefault().getID())));
		jobsMap.put(taskid, scheduledTask);
	}
	
	@Override
	public void addTaskToScheduler(Long taskId, Runnable task, OffsetDateTime startTime) {
		ScheduledFuture<?> scheduledTask = scheduler.schedule(task, startTime.toInstant());
		jobsMap.put(taskId, scheduledTask);
	}
	
	@Override
	public void removeTaskFromScheduler(Long taskId) {
		ScheduledFuture<?> scheduledTask = jobsMap.get(taskId);
		if (scheduledTask != null) {
			scheduledTask.cancel(true);
			jobsMap.put(taskId, null);
		}
	}

	// A context refresh event listener
	@EventListener({ ContextRefreshedEvent.class })
	public void contextRefreshedEvent() {
		// Get all tasks from DB and reschedule them in case of context restarted
	}

}