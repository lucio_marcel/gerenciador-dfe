package br.com.zeta.gerenciadordfe.service.core.http;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;

import org.apache.commons.httpclient.ConnectTimeoutException;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HSProtocolSocketFactory implements ProtocolSocketFactory {
		
	private SSLContext ssl = null;
	private String cacerts;
	
	private X509Certificate certificate;
	private PrivateKey privateKey;

	public HSProtocolSocketFactory(X509Certificate certificate, PrivateKey privateKey, String cacerts) {
		this.certificate = certificate;
		this.privateKey = privateKey;
		this.cacerts = cacerts;
	}

	public SSLContext createSSLContext() {
		try {
			KeyManager[] keyManagers = createKeyManagers();
			TrustManager[] trustManagers = createTrustManagers();
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(keyManagers, trustManagers, null);
			return sslContext;
		} catch (Exception e) {
			log.error(e.getMessage());
			e.getStackTrace();
		}
		return null;
	}

	public SSLContext getSSLContext() {

		if (ssl == null) {
			ssl = createSSLContext();
		}
		return ssl;
	}

	public Socket createSocket(String host, int port, InetAddress localAddress, int localPort,
			HttpConnectionParams params) throws IOException, UnknownHostException, ConnectTimeoutException {

		if (params == null) {
			throw new IllegalArgumentException("Parameters may not be null");
		}

		int timeout = params.getConnectionTimeout();
		SocketFactory socketfactory = getSSLContext().getSocketFactory();
		if (timeout == 0) {
			return socketfactory.createSocket(host, port, localAddress, localPort);
		}

		Socket socket = socketfactory.createSocket();
		SocketAddress localaddr = new InetSocketAddress(localAddress, localPort);
		SocketAddress remoteaddr = new InetSocketAddress(host, port);
		socket.bind(localaddr);

		try {
			socket.connect(remoteaddr, timeout);
		} catch (Throwable t) {
			log.error(t.getMessage());
			throw new ConnectTimeoutException("Erro na conexao", t);
		}

		return socket;
	}

	public Socket createSocket(String host, int port, InetAddress clientHost, int clientPort)
			throws IOException, UnknownHostException {
		return getSSLContext().getSocketFactory().createSocket(host, port, clientHost, clientPort);
	}

	public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
		return getSSLContext().getSocketFactory().createSocket(host, port);
	}

	public Socket createSocket(Socket socket, String host, int port, boolean autoClose)
			throws IOException, UnknownHostException {
		return getSSLContext().getSocketFactory().createSocket(socket, host, port, autoClose);
	}

	public KeyManager[] createKeyManagers() {
		HSKeyManager keyManager = new HSKeyManager(certificate, privateKey);

		return new KeyManager[] { keyManager };
	}

	public TrustManager[] createTrustManagers()
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, Exception {

		TrustManager[] trustManagers = null;
		try {
			KeyStore trustStore = KeyStore.getInstance("JKS");
			FileInputStream fi = new FileInputStream(this.cacerts);
			trustStore.load(fi, "changeit".toCharArray());
			fi.close();
			TrustManagerFactory trustManagerFactory = TrustManagerFactory
					.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(trustStore);
			
			trustManagers = trustManagerFactory.getTrustManagers();
			
		} catch (Exception e) {
			log.error("Problemas ao realizar a criacao dos TrustManagers. Verifique se o path do arquivo cacerts.keystore esta correto.\n" + e.getMessage());
			throw new Exception("Problemas ao realizar a criacao dos TrustManagers. Verifique se o path do arquivo cacerts.keystore esta correto.\n" + e.getMessage());
		}
		return trustManagers;
	}

	class HSKeyManager implements X509KeyManager {

		private X509Certificate certificate;
		private PrivateKey privateKey;

		public HSKeyManager(X509Certificate certificate, PrivateKey privateKey) {
			this.certificate = certificate;
			this.privateKey = privateKey;
		}

		public String chooseClientAlias(String[] arg0, Principal[] arg1, Socket arg2) {
			return certificate.getIssuerDN().getName();
		}

		public String chooseServerAlias(String arg0, Principal[] arg1, Socket arg2) {
			return null;
		}

		public X509Certificate[] getCertificateChain(String arg0) {
			return new X509Certificate[] { certificate };
		}

		public String[] getClientAliases(String arg0, Principal[] arg1) {
			return new String[] { certificate.getIssuerDN().getName() };
		}

		public PrivateKey getPrivateKey(String arg0) {
			return privateKey;
		}

		public String[] getServerAliases(String arg0, Principal[] arg1) {
			return null;
		}
	}

}