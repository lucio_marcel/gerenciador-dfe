package br.com.zeta.gerenciadordfe.service;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Base64;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.SituacaoNFe;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.TipoOperacao;
import br.com.zeta.gerenciadordfe.domain.exception.EntityDataIntegrityViolationException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.NFeCanceladaException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.NFeNaoManifestadaException;
import br.com.zeta.gerenciadordfe.domain.exception.entity.ResumoNFeNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.exception.entity.XmlNFeNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.filter.ResumoNFeFilter;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.com.zeta.gerenciadordfe.domain.model.entity.Emitente;
import br.com.zeta.gerenciadordfe.domain.repository.ResumoNFeRepository;
import br.com.zeta.gerenciadordfe.domain.repository.specification.ResumoNFeSpecifications;
import br.com.zeta.gerenciadordfe.domain.service.ResumoNFeService;
import br.inf.portalfiscal.nfe.distdfe.ResNFe;
import br.inf.portalfiscal.nfe.nfe.TNfeProc;

@Service
public class ResumoNfeServiceImpl implements ResumoNFeService {

	@Autowired
	private ResumoNFeRepository resumoNFeRepository;

	@Override
	public ResumoNFe salvar(ResumoNFe resumoNfe) {
		try {
			return resumoNFeRepository.save(resumoNfe);
		} catch (DataIntegrityViolationException e) {
			throw new EntityDataIntegrityViolationException(e);
		}
	}

	@Override
	@Transactional
	public void adicionar(ResumoNFe resumoNfe) {
		try {
			if (!resumoNFeRepository.existsByChaveAcesso(resumoNfe.getChaveAcesso())) {
				resumoNfe.setNumeroDownloads(0);
				salvar(resumoNfe);
			}
		} catch (DataIntegrityViolationException e) {
			throw new EntityDataIntegrityViolationException(e);
		}
	}

	@Override
	@Transactional
	public void adicionar(Empresa empresa, ResNFe resNFe) {
		adicionar(toResumoNFe(empresa, resNFe));
		
	}
	
	@Override
	@Transactional
	public void adicionar(Empresa empresa, TNfeProc tNfeProc) {
		adicionar(nfeToResumoNFe(empresa, tNfeProc));
	}

	private ResumoNFe toResumoNFe(Empresa empresa, ResNFe resNFe) {
		Emitente emitente = Emitente.builder()
				.nome(resNFe.getXNome())
				.CNPJ(resNFe.getCNPJ())
				.CPF(resNFe.getCPF())
				.inscricaoEstadual(resNFe.getIE())
				.build();
		
		ResumoNFe resumoNFe = ResumoNFe.builder()
				.empresa(empresa)
				.chaveAcesso(resNFe.getChNFe())
				.dataEmissao(OffsetDateTime.parse(resNFe.getDhEmi()))
				.dataAutorizacao(OffsetDateTime.parse(resNFe.getDhRecbto()))
				.digestValue(Base64.getEncoder().encodeToString(resNFe.getDigVal()))
				.emitente(emitente)
				.nfeManifestada(false).numeroProtocolo(resNFe.getNProt())
				.situacaoNfe(SituacaoNFe.valueOfCodigo(resNFe.getCSitNFe()))
				.numeroProtocolo(resNFe.getNProt())
				.valor(new BigDecimal(resNFe.getVNF()))
				.nfeManifestada(false)
				.nfeImportada(false)
				.numeroDownloads(0)
				.tipoOperacao(TipoOperacao.valueOfCodigo(resNFe.getTpNF()))
				.build();
		
		return resumoNFe;
	}
	
	
	private ResumoNFe nfeToResumoNFe(Empresa empresa, TNfeProc nfeProc) {
		Emitente emitente = Emitente.builder()
				.nome(nfeProc.getNFe().getInfNFe().getEmit().getXNome())
				.CNPJ(nfeProc.getNFe().getInfNFe().getEmit().getCNPJ())
				.CPF(nfeProc.getNFe().getInfNFe().getEmit().getCPF())
				.inscricaoEstadual(nfeProc.getNFe().getInfNFe().getEmit().getIE())
				.build();
		
		ResumoNFe resumoNFe = ResumoNFe.builder()
				.empresa(empresa)
				.chaveAcesso(nfeProc.getProtNFe().getInfProt().getChNFe())
				.dataEmissao(OffsetDateTime.parse(nfeProc.getNFe().getInfNFe().getIde().getDhEmi()))
				.dataAutorizacao(OffsetDateTime.parse(nfeProc.getProtNFe().getInfProt().getDhRecbto()))
				.digestValue(Base64.getEncoder().encodeToString(nfeProc.getProtNFe().getInfProt().getDigVal()))
				.emitente(emitente)
				.nfeManifestada(true)
				.numeroProtocolo(nfeProc.getProtNFe().getInfProt().getNProt())
				.situacaoNfe(SituacaoNFe.AUTORIZADA)
				.numeroProtocolo(nfeProc.getProtNFe().getInfProt().getNProt())
				.valor(new BigDecimal(nfeProc.getNFe().getInfNFe().getTotal().getICMSTot().getVNF()))
				.nfeManifestada(true)
				.nfeImportada(false)
				.numeroDownloads(0)
				.tipoOperacao(TipoOperacao.valueOfCodigo(nfeProc.getNFe().getInfNFe().getIde().getTpNF()))
				.build();
		
		return resumoNFe;
	}
	
	
	@Override
	@Transactional
	public void salvarXmlNFe(Long empresaCodigo, String chaveAcesso, String xmlNFe) {

		if (chaveAcesso != null && resumoNFeRepository.existsByChaveAcesso(chaveAcesso)) {
			ResumoNFe resumo = buscarPorChaveAcesso(empresaCodigo, chaveAcesso);

			resumo.setXml(xmlNFe);

			if (!resumo.getSituacaoNfe().equals(SituacaoNFe.CANCELADA)) {
				resumo.setNfeManifestada(true);
			}

			salvar(resumo);			
		}

	}

	@Override
	public Page<ResumoNFe> buscarTodos(ResumoNFeFilter filtro, Pageable pageable) {
		return resumoNFeRepository.findAll(ResumoNFeSpecifications.usandoFiltro(filtro), pageable);
	}

	@Override
	public List<ResumoNFe> buscarTodosComManifestacaoPendente(Long empresaCodigo) {
		return resumoNFeRepository.findAllManifestacaoPendente(empresaCodigo);
	}

	@Override
	public ResumoNFe buscarPorChaveAcesso(Long empresaCodigo, String chaveAcesso) {
		return resumoNFeRepository.findByEmpresaCodigoAndChaveAcesso(empresaCodigo, chaveAcesso)
				.orElseThrow(() -> new ResumoNFeNaoEncontradoException(empresaCodigo, chaveAcesso));
	}

	@Override
	public String buscarXml(Long empresaCodigo, String chaveAcesso) {
		ResumoNFe resumoNFe = buscarPorChaveAcesso(empresaCodigo, chaveAcesso);

		if (resumoNFe.getXml() == null) {
			throw new XmlNFeNaoEncontradoException(empresaCodigo, chaveAcesso);
		} else if (resumoNFe.getSituacaoNfe().equals(SituacaoNFe.CANCELADA)) {
			throw new NFeCanceladaException(chaveAcesso);
		} else if (!resumoNFe.isNfeManifestada()) {
			throw new NFeNaoManifestadaException(chaveAcesso);
		}

		return resumoNFe.getXml();
	}



}