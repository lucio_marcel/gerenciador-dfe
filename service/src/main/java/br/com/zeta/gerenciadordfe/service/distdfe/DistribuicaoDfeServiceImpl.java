package br.com.zeta.gerenciadordfe.service.distdfe;

import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoConsulta.CONSULTA_POR_CHAVE_ACESSO;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoConsulta.CONSULTA_POR_ULTIMO_NSU;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.RESUMO_NFE;
import static br.com.zeta.gerenciadordfe.domain.core.enums.TipoDocumento.RETORNO_DOWNLOAD_NFE;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta.DOCUMENTOS_NAO_LOCALIZADOS;
import static br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta.ERRO;
import static br.com.zeta.gerenciadordfe.service.core.utils.GZip.unZipAndUnMarshal;
import static br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils.objectToXml;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import br.com.zeta.gerenciadordfe.domain.core.enums.TipoConsulta;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.StatusResposta;
import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.UF;
import br.com.zeta.gerenciadordfe.domain.exception.ApplicationException;
import br.com.zeta.gerenciadordfe.domain.exception.DomainException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.NFeNaoManifestadaException;
import br.com.zeta.gerenciadordfe.domain.exception.dfe.sefaz.ConsultaNFeException;
import br.com.zeta.gerenciadordfe.domain.exception.entity.ResumoNFeNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.exception.entity.XmlNFeNaoEncontradoException;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Empresa;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.Historico;
import br.com.zeta.gerenciadordfe.domain.service.EmpresaService;
import br.com.zeta.gerenciadordfe.domain.service.HistoricoService;
import br.com.zeta.gerenciadordfe.domain.service.ResumoNFeService;
import br.com.zeta.gerenciadordfe.domain.service.distdfe.DistribuicaoDfeService;
import br.com.zeta.gerenciadordfe.domain.service.storage.XmlStorageService;
import br.com.zeta.gerenciadordfe.service.core.utils.WebServiceMessageFacory;
import br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils;
import br.inf.portalfiscal.nfe.distdfe.DistDFeInt;
import br.inf.portalfiscal.nfe.distdfe.ResNFe;
import br.inf.portalfiscal.nfe.distdfe.RetDistDFeInt;
import br.inf.portalfiscal.nfe.nfe.TNfeProc;
import br.inf.portalfiscal.nfe.wsdl.nfedistribuicaodfe.NfeDistDFeInteresse;
import br.inf.portalfiscal.nfe.wsdl.nfedistribuicaodfe.NfeDistDFeInteresseResponse;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DistribuicaoDfeServiceImpl extends WebServiceGatewaySupport implements DistribuicaoDfeService {

	@Autowired private WebServiceMessageFacory webServiceMessageFacory;
	@Autowired private ResumoNFeService resumoNFeService;
	@Autowired private EmpresaService empresaService;
	@Autowired private HistoricoService historicoService;
	@Autowired private XmlStorageService xmlStorageService;

	@Getter	private Empresa empresaConsulta;
	@Getter private String documentoConsulta;
	
	private static String CONTEXT_PATH = "br.inf.portalfiscal.nfe.distdfe:"
			+ "br.inf.portalfiscal.nfe.evento:"
			+ "br.inf.portalfiscal.nfe.nfe:"
			+ "org.w3._2000._09.xmldsig_";

	public String consultarPorChaveAcesso(Long empresaCodigo, String chaveAcesso) {
		
		String xml = new String();
		
		try {
			xml = resumoNFeService.buscarXml(empresaCodigo, chaveAcesso);
			
		} catch (ResumoNFeNaoEncontradoException | XmlNFeNaoEncontradoException | NFeNaoManifestadaException ex) {
			this.empresaConsulta = empresaService.buscarPorCodigo(empresaCodigo);
			this.documentoConsulta = chaveAcesso;

			NfeDistDFeInteresse nfeDistDFeInteresse = getNfeDistDFeInteresse(CONSULTA_POR_CHAVE_ACESSO);
			
			preverConsumoIndevido(CONSULTA_POR_CHAVE_ACESSO, this.empresaConsulta);

			List<Object> objects = getWebServiceResponse(nfeDistDFeInteresse);
			
			gravarHistorico(CONSULTA_POR_CHAVE_ACESSO);

			System.out.println(XmlUtils.getContextPath());
			XmlUtils.setContextPath(null);
			System.out.println(XmlUtils.getContextPath());
			
			if (!objects.isEmpty() && (objects.get(0) instanceof TNfeProc)) {
				try {
					xml = objectToXml(objects.get(0));
					resumoNFeService.salvarXmlNFe(empresaCodigo, chaveAcesso, xml);
					
				} catch (Exception e) {
					e.printStackTrace();
					throw new ApplicationException(e.getMessage());
				}
			} else {
				throw new XmlNFeNaoEncontradoException("XML da NFe ainda não disponível!");
			}
			

		}
		
		return xml;
	}
	
	public void consultarPorUltimoNSU(Empresa empresa) {
		
		try {
			this.empresaConsulta = empresa;
			this.documentoConsulta = this.empresaConsulta.getUltimoNsuConsultado();
			
			OffsetDateTime DataAtual = OffsetDateTime.now();
	
			log.info("Empresa : " + empresa.getCodigo() + " - CNPJ: " + empresa.getCnpj() + 
					" - Último NSU Consultado: " + empresa.getUltimoNsuConsultado() + 
					" - Maior NSU Disponível : " + empresa.getMaiorNsuExistente() + 
					" - Data Última Consulta : " + empresa.getDataUltimaConsulta() + 
					" - Data Atual: " + DataAtual);
			NfeDistDFeInteresse nfeDistDFeInteresse = getNfeDistDFeInteresse(CONSULTA_POR_ULTIMO_NSU);
			
			
			preverConsumoIndevido(CONSULTA_POR_ULTIMO_NSU, empresa);
	
			List<Object> objects = getWebServiceResponse(nfeDistDFeInteresse);
			
			gravarHistorico(CONSULTA_POR_ULTIMO_NSU);
			
			
			objects.stream().forEach(object -> {
				if (object instanceof ResNFe) {
					ResNFe resNFe =  (ResNFe) object;
					resumoNFeService.adicionar(empresa, resNFe);		

					try {
						xmlStorageService.salvar(RESUMO_NFE, resNFe.getChNFe(), objectToXml(resNFe), empresa.getCnpj());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				} else if (object instanceof TNfeProc) {
					TNfeProc tNfeProc = (TNfeProc) object;
					
					String chaveAcesso = tNfeProc.getProtNFe().getInfProt().getChNFe(); 

					String xmlNFe = null;
					
					try {
						xmlNFe = objectToXml(tNfeProc);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					resumoNFeService.adicionar(empresa, tNfeProc);
					
					resumoNFeService.salvarXmlNFe(empresa.getCodigo(), chaveAcesso, xmlNFe);

					xmlStorageService.salvar(RETORNO_DOWNLOAD_NFE, chaveAcesso, xmlNFe, empresa.getCnpj());
				}
			});
			
		}catch(ConsultaNFeException e)		{
			throw new ConsultaNFeException(e.getMessage());
	
		}catch(ApplicationException e)		{
			throw new ApplicationException(e.getMessage(), e);
	
		}finally		{
			Empresa empresaAtualizada = empresaService.buscarPorCodigo(empresa.getCodigo());
			empresaAtualizada.setStatusUltimaConsulta(empresa.getStatusUltimaConsulta());
			empresaAtualizada.setUltimoNsuConsultado(empresa.getUltimoNsuConsultado());
			empresaAtualizada.setMaiorNsuExistente(empresa.getMaiorNsuExistente());
			empresaAtualizada.setDataUltimaConsulta(empresa.getDataUltimaConsulta());
			empresaAtualizada.finalizarConsuultaDFe();
			empresaService.atualizar(empresaAtualizada);
		}
	}
	
	private void gravarHistorico(TipoConsulta tipoConsulta) {
		Historico historico = new Historico();
		historico.setEmpresa(empresaConsulta);
		historico.setDataConsulta(empresaConsulta.getDataUltimaConsulta());
		historico.setStatusConsulta(empresaConsulta.getStatusUltimaConsulta());
		historico.setTipoConsulta(tipoConsulta);
		historicoService.adicionar(historico);
	}
	
	private void preverConsumoIndevido(TipoConsulta tipoConsulta, Empresa empresa) {
		Long numeroDeConsultas = historicoService.contarConsultasUltimaHoraPorTipo(tipoConsulta, empresa.getId());
		switch (tipoConsulta) {
		case CONSULTA_POR_CHAVE_ACESSO:
			if (numeroDeConsultas >= 20) {
				throw new ConsultaNFeException("Número de consultas por chave de acesso excedido. São permitidas 20 consultas por Hora.");
			}
			break;
		case CONSULTA_POR_ULTIMO_NSU:
			if (numeroDeConsultas >= 20) {
				throw new ConsultaNFeException("Número de consultas por NSU excedido. São permitidas 20 consultas por Hora.");
			}
			break;
		default:
			throw new DomainException(String.format("Tipo de consulta inválida: %s", tipoConsulta.name()));
		}
	}
	
	private List<Object> getWebServiceResponse(NfeDistDFeInteresse nfeDistDFeInteresse) {

		List<Object> objects = new ArrayList<Object>();
		
	
		// Configuração
		try {
			setMessageSender(webServiceMessageFacory.getWebServiceMessageSender(empresaConsulta.getCertificado()));
		} catch (Exception e) {
			empresaConsulta.setStatusUltimaConsulta(ERRO);
			empresaConsulta.setDataUltimaConsulta(OffsetDateTime.now());
			e.printStackTrace();
			throw new ConsultaNFeException("Erro ao iniciar o processamento da consulta.");
		}

		NfeDistDFeInteresseResponse response;

		response = (NfeDistDFeInteresseResponse) getWebServiceTemplate().marshalSendAndReceive(nfeDistDFeInteresse,
				new SoapActionCallback(
						"http://www.portalfiscal.inf.br/nfe/wsdl/NFeDistribuicaoDFe/nfeDistDFeInteresse"));

		RetDistDFeInt retDistDFeInt = (RetDistDFeInt) response.getNfeDistDFeInteresseResult().getContent().get(0);

		StatusResposta statusResposta = StatusResposta.valueOfCodigo(retDistDFeInt.getCStat());

		empresaConsulta.setStatusUltimaConsulta(statusResposta);
		empresaConsulta.setDataUltimaConsulta(OffsetDateTime.now());
		
		log.info("Empresa : " + empresaConsulta.getCodigo() + " - CNPJ: " + empresaConsulta.getCnpj() + " RESPOSTA: " + statusResposta);
		switch (statusResposta) {
		case DOCUMENTOS_LOCALIZADOS:
			retDistDFeInt.getLoteDistDFeInt().getDocZip().forEach(docZip -> {
				Object object = null;

				try {
					//jaxBElement = unZipAndUnMarshal(docZip.getValue(), ResEvento.class, ResNFe.class, TNfeProc.class,TProcEvento.class);
					object = unZipAndUnMarshal(docZip.getValue(),CONTEXT_PATH);

				} catch (Exception e) {
					empresaConsulta.setStatusUltimaConsulta(ERRO);
					empresaConsulta.setDataUltimaConsulta(OffsetDateTime.now());
					e.printStackTrace();
					throw new ConsultaNFeException("Erro ao processar o retorno da consulta.");

				}
				
				if (object instanceof JAXBElement<?>) {
					objects.add(((JAXBElement<?>) object).getValue());
				} else {
					objects.add(object);
				}

				empresaConsulta.setUltimoNsuConsultado(docZip.getNSU());

			});

			empresaConsulta.setMaiorNsuExistente(retDistDFeInt.getMaxNSU());

			if (empresaConsulta.getUltimoNsuConsultado().equals(empresaConsulta.getMaiorNsuExistente())) {
				empresaConsulta.setStatusUltimaConsulta(DOCUMENTOS_NAO_LOCALIZADOS);
			}

			break;

		case DOCUMENTOS_NAO_LOCALIZADOS:
		case CONSUMO_INDEVIDO:
			break;

		default:
			empresaConsulta.setStatusUltimaConsulta(ERRO);
			empresaConsulta.setDataUltimaConsulta(OffsetDateTime.now());
			throw new ConsultaNFeException(retDistDFeInt.getCStat(), retDistDFeInt.getXMotivo());
		}

		return objects;
	}
	
	private NfeDistDFeInteresse getNfeDistDFeInteresse(TipoConsulta tipoConsulta) {
		
		DistDFeInt distDFeInt = new DistDFeInt();
		distDFeInt.setVersao("1.01");
		distDFeInt.setTpAmb(empresaConsulta.getTipoAmbiente().getCodigo());
		distDFeInt.setCUFAutor(UF.parseByCodigoMunicipio(empresaConsulta.getCodigoMuncipio()).getCodigo());
		distDFeInt.setCNPJ(empresaConsulta.getCnpj());

		switch (tipoConsulta) {
		case CONSULTA_POR_CHAVE_ACESSO:
			DistDFeInt.ConsChNFe consChNFe = new DistDFeInt.ConsChNFe();
			consChNFe.setChNFe(documentoConsulta);
			distDFeInt.setConsChNFe(consChNFe);
			break;

		case CONSULTA_POR_ULTIMO_NSU:
			DistDFeInt.DistNSU distNSU = new DistDFeInt.DistNSU();
			distNSU.setUltNSU(documentoConsulta);
			distDFeInt.setDistNSU(distNSU);
			break;

		default:
			throw new DomainException(String.format("Tipo de consulta inválida: %s", tipoConsulta.name()));
		}

		NfeDistDFeInteresse.NfeDadosMsg dadosMensagem = new NfeDistDFeInteresse.NfeDadosMsg();
		dadosMensagem.getContent().add(distDFeInt);
		
		NfeDistDFeInteresse nfeDistDFeInteresse = new NfeDistDFeInteresse();
		nfeDistDFeInteresse.setNfeDadosMsg(dadosMensagem);

		return nfeDistDFeInteresse;
	}

	
//	private String elementToXml(JAXBElement<?> element) {
//		
//		StringWriter writer = new StringWriter();
//
//		try {
//			JAXBContext jaxbContext = JAXBContext.newInstance(CONTEXT_PATH);
//
//			Marshaller marshaller = jaxbContext.createMarshaller();
//			marshaller.marshal(element, writer);
//			
//		} catch (JAXBException e) {
//			e.printStackTrace();
//			
//		}
//
//		return writer.toString();
//
//	}

}


