package br.com.zeta.gerenciadordfe.service.core.certificate;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoException;
import br.com.zeta.gerenciadordfe.domain.exception.certificate.CertificadoExpiradoException;
import br.com.zeta.gerenciadordfe.service.core.utils.XmlUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class DefaultSigner {
	protected static final String TRANSFORM_METHOD_C14N = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
	protected static final String SIGNATURE_METHOD_RSA_SHA1 = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
	protected static final String DIGEST_METHOD_SHA1 = "http://www.w3.org/2000/09/xmldsig#sha1";

	private char[] password;

	private String certificateFilePath;
	private String alias;
	private KeyStore keyStore;

	private X509Certificate signingCertificate;

	public DefaultSigner(String certificateFilePath, String alias, String password)  {

		try {
		this.certificateFilePath = certificateFilePath;
		this.alias = alias;
		this.password = password.toCharArray();

		loadKeyStore();
		checkCertificateValidity();
		} catch(CertificadoException e) {
			throw new CertificadoException(e.getMessage(), e);
		}

	}

	private void loadKeyStore(){
		try {
			this.keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			FileInputStream fileInputStream = null;
			fileInputStream = new FileInputStream(new File(certificateFilePath));
			this.keyStore.load(fileInputStream, this.password);
			fileInputStream.close();
		} catch (CertificateException | KeyStoreException | NoSuchAlgorithmException | IOException e) {
			throw new CertificadoException(e.getMessage(), e);
		}

	}

	protected PrivateKey getPrivateKey() {

		try {
			String alias = keyStore.getCertificateAlias(getX509Certificate());

			Key privateKey = this.keyStore.getKey(alias, this.password);

			if (privateKey instanceof PrivateKey) {
				return (PrivateKey) privateKey;
			} else {
				return null;
			}
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			throw new CertificadoException(e.getMessage(), e);
		}

	}

	protected static Document documentFactory(String xml)
			throws SAXException, IOException, ParserConfigurationException {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		return factory.newDocumentBuilder().parse(new ByteArrayInputStream(xml.getBytes()));

	}

	protected static Document documentFactoryNFSe(String xml) throws ParserConfigurationException, SAXException, IOException {

		xml = XmlUtils.removeAccents(xml);
		Document document = null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		document = builder.parse(new InputSource(new StringReader(xml)));

		return document;
		
	}

	protected KeyInfo getKeyInfo()  {

		XMLSignatureFactory signatureFactory = XMLSignatureFactory.getInstance("DOM");
		KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
		List<X509Certificate> x509Content = new ArrayList<X509Certificate>();
		x509Content.add(this.getX509Certificate());
		X509Data x509Data = keyInfoFactory.newX509Data(x509Content);
		return keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));

	}

	protected Document signDocument(Document document, String projeto)  {

		try {
			KeyInfo keyInfo;
			SignedInfo signedInfo;
			XMLSignatureFactory xmlSignatureFactory;
			xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
			ArrayList<Transform> transformList = new ArrayList<Transform>();
			Transform enveloped = xmlSignatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec) null);
			Transform c14n = xmlSignatureFactory.newTransform(TRANSFORM_METHOD_C14N, (TransformParameterSpec) null);
			transformList.add(enveloped);
			if (projeto != null && (projeto.equalsIgnoreCase("infisc") || projeto.equalsIgnoreCase("ginfes")))
				transformList.add(c14n);

			Reference reference = xmlSignatureFactory.newReference("",
					xmlSignatureFactory.newDigestMethod(DigestMethod.SHA1, null), transformList, null, null);

			signedInfo = xmlSignatureFactory.newSignedInfo(
					xmlSignatureFactory.newCanonicalizationMethod(CanonicalizationMethod.INCLUSIVE,
							(C14NMethodParameterSpec) null),
					xmlSignatureFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
					Collections.singletonList(reference));

			KeyInfoFactory kif = xmlSignatureFactory.getKeyInfoFactory();

			List<Object> x509Content = new ArrayList<Object>();

			if (projeto != null && projeto.equalsIgnoreCase("infisc"))
				x509Content.add(this.getX509Certificate().getSubjectX500Principal().getName());

			x509Content.add(this.getX509Certificate());
			X509Data xd = kif.newX509Data(x509Content);
			keyInfo = kif.newKeyInfo(Collections.singletonList(xd));
			DOMSignContext dsc = new DOMSignContext(getPrivateKey(), document.getDocumentElement());
			XMLSignature signature = xmlSignatureFactory.newXMLSignature(signedInfo, keyInfo);
			signature.sign(dsc);

			return document;

		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | MarshalException | XMLSignatureException e) {
			throw new CertificadoException(e.getMessage(), e.getCause());

		}
		
	}

	protected String toOutputXML(Document doc) throws TransformerException {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));
		return os.toString();

	}

	protected ArrayList<Transform> createSignatureFactory(XMLSignatureFactory signatureFactory){

		try {
			ArrayList<Transform> transformList = new ArrayList<Transform>();
			Transform envelopedTransform;
			envelopedTransform = signatureFactory.newTransform(Transform.ENVELOPED,(TransformParameterSpec) null);
			transformList.add(envelopedTransform);
			return transformList;
		} catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
			throw new CertificadoException(e.getMessage(), e.getCause());
		
		}

	}

	private void checkCertificateValidity() throws CertificadoExpiradoException {

		X509Certificate certificate = getX509Certificate();

		try {
			certificate.checkValidity();

		} catch (CertificateExpiredException e) {
			throw new CertificadoExpiradoException(this.alias, certificate.getNotAfter());

		} catch (CertificateNotYetValidException e) {
			throw new CertificadoException(e.getMessage(), e.getCause());
			
		}

	}

	public X509Certificate getX509Certificate() {

		try {
			Enumeration<String> aliases = keyStore.aliases();

			while (aliases.hasMoreElements()) {
				String certAlias = aliases.nextElement();

				Certificate cert = keyStore.getCertificate(certAlias);

				if (cert instanceof X509Certificate) {
					X509Certificate certificate = (X509Certificate) cert;

					return certificate;
				}
			}

		} catch (KeyStoreException e) {
			throw new CertificadoException(e.getMessage(), e.getCause());

		}

		return null;

	}
}