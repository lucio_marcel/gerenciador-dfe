package br.com.zeta.gerenciadordfe.service.core.utils;

import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpsUrlConnectionMessageSender;

import br.com.zeta.gerenciadordfe.domain.core.configuration.ConfigProperties;
import br.com.zeta.gerenciadordfe.domain.model.entity.Certificado;

@Component
public class WebServiceMessageFacory {
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Autowired
	private ConfigProperties configProperties;
	
	public WebServiceMessageSender getWebServiceMessageSender(Certificado certificado) throws Exception {
		
		Resource clientKeyStoreFile = resourceLoader.getResource("file:" + certificado.getFilePath());
		Resource trustStoreFile = resourceLoader.getResource("file:" + configProperties.getCacertsFileLocation());
		
		String clientKeyStorePassword = certificado.getPassword();
				
		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(trustStoreFile.getInputStream(), null);

		KeyStore keyStore = KeyStore.getInstance("JKS");
		keyStore.load(clientKeyStoreFile.getInputStream(), clientKeyStorePassword.toCharArray());

		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(trustStore);

		KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		keyManagerFactory.init(keyStore, clientKeyStorePassword.toCharArray());

		HttpsUrlConnectionMessageSender messageSender = new HttpsUrlConnectionMessageSender();
		
		messageSender.setTrustManagers(trustManagerFactory.getTrustManagers());
		messageSender.setKeyManagers(keyManagerFactory.getKeyManagers());
		
		return messageSender;
		
	}

}