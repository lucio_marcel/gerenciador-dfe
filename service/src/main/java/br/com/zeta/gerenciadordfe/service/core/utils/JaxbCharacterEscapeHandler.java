package br.com.zeta.gerenciadordfe.service.core.utils;

import java.io.IOException;
import java.io.Writer;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

public class JaxbCharacterEscapeHandler implements CharacterEscapeHandler {

	
	public static final JaxbCharacterEscapeHandler theInstance = new JaxbCharacterEscapeHandler();
	
		public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
			
	        int limit = start + length;
	        for (int i = start; i < limit; i++) {
	            char c = ch[i];
	            if (c == '&' || c == '<' || c == '>' || (c == '\"' && isAttVal)
	                    || (c == '\'' && isAttVal)) {
	                if (i != start) {
	                    out.write(ch, start, i - start);
	                }
	                start = i + 1;
	                switch (ch[i]) {
	                    case '&':
	                        out.write("&");
	                        break;
	 
	                    case '<':
	                        out.write("<");
	                        break;
	 
	                    case '>':
	                        out.write(">");
	                        break;
	 
	                    case '\"':
	                        out.write("\"");
	                        break;
	 
	                    case '\'':
	                        out.write("\'");
	                        break;
	                }
	            }
	        }
	        if (start != limit) {
	            out.write(ch, start, limit - start);
	        }
	    }
}