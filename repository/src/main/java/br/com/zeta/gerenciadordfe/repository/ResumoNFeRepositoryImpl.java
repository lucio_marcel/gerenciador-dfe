package br.com.zeta.gerenciadordfe.repository;

import static br.com.zeta.gerenciadordfe.domain.repository.specification.ResumoNFeSpecifications.usandoFiltro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import br.com.zeta.gerenciadordfe.domain.core.enums.sefaz.SituacaoNFe;
import br.com.zeta.gerenciadordfe.domain.filter.ResumoNFeFilter;
import br.com.zeta.gerenciadordfe.domain.model.aggregate.ResumoNFe;
import br.com.zeta.gerenciadordfe.domain.repository.ResumoNFeRepository;
import br.com.zeta.gerenciadordfe.domain.repository.ResumoNFeRepositoryQueries;

@Repository
public class ResumoNFeRepositoryImpl implements ResumoNFeRepositoryQueries{
	
	@Autowired @Lazy
	private ResumoNFeRepository resumoNFeRepository;
	
	@Override
	public List<ResumoNFe> findAllManifestacaoPendente(Long empresaCodigo){
		
		ResumoNFeFilter filtro = ResumoNFeFilter.builder()
				.empresaCodigo(empresaCodigo)
				.nfeManifestada(false)
				.situacaoNfe(SituacaoNFe.AUTORIZADA)
				.build();

		return resumoNFeRepository.findAll(usandoFiltro(filtro));
	}
	
}
	